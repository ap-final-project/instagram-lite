<img src = "readmeFiles/name.jpg" alt="Instagram" style="width:100%;">


# Instagram Lite

Simpler version of Instagram witch is built with [**maven framwork**](https://maven.apache.org/) (software project management)

## Maintainers

- [**Mahdieh Sajedipoor**](https://gitlab.com/Mahdieh_sjp)
- [**Nima Taheri**](https://gitlab.com/nimanta)
- [**Maryam Jahromi**](https://gitlab.com/m.ji78)

## Motivation
Instagram lite is written by our team and it is simplified version of Instagram which is one of the top photos and video sharing social networking service and we have chosen this for our advanced programming final project at Shahid Beheshti University.

## Features

- **Clean & beautiful user interface design**
\
This social media software developed using [**JavaFX**](https://openjfx.io/) programming language.

  <p align="center">
    <img src = "readmeFiles/login.jpg" alt="Instagram">
  </p>
  <br\>
  <p align="center">
    <img src = "readmeFiles/main user.jpg" alt="Instagram">
  </p>
  <br\>
  <p align="center">
    <img src = "readmeFiles/timeline.jpg" alt="Instagram">
  </p>
  <br\>
  <p align="center">
    <img src = "readmeFiles/direct.jpg" alt="Instagram">
  </p>
  <br\>


- **Fast and safe connection**
\
We handle each user's requests with [**multiple threads**](https://www.javatpoint.com/multithreading-in-java) so they don't have to waite for response and can continue their journey.
  
- **Storing data are reliable and nothing gets lost**
\
Your data are stored in [**MongoDB**](https://www.mongodb.com/) database.


## Libraries 
- **[**JavaFX Base**](https://mvnrepository.com/artifact/org.openjfx/javafx-base)**
- **[**JavaFX FXML**](https://mvnrepository.com/artifact/org.openjfx/javafx-fxml)**
- **[**JavaFX Controls**](https://mvnrepository.com/artifact/org.openjfx/javafx-controls)**
- **[**JavaFX Graphics**](https://mvnrepository.com/artifact/org.openjfx/javafx-graphics)**
- **[**Gson**](https://mvnrepository.com/artifact/com.google.code.gson/gson)**
- **[**Bson**](https://mvnrepository.com/artifact/org.mongodb/bson)**
- **[**Java Dotenv**](https://mvnrepository.com/artifact/io.github.cdimascio/java-dotenv)**
- **[**JFoeniX**](https://mvnrepository.com/artifact/com.jfoenix/jfoenix)**
- **[**ControlsFX**](https://mvnrepository.com/artifact/org.controlsfx/controlsfx)**
- **[**JavaMail API**](https://mvnrepository.com/artifact/com.sun.mail/javax.mail)**
- **[**Jackson Annotations**](https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-annotations)**


## What we learnt
We learned how to connect the client to the server and we worked with threads and stored our data in database.


## Mentors
A special thanks to
- [**Ali Alibadi**](https://gitlab.com/ali-aliabadi)
- [**Sepeher Kashanchi**](https://gitlab.com/Spoorlord)
  

<p align="center">
  <img src = "readmeFiles/img.png" weight = "100%" alt="Instagram">
</p>
