package API;

import java.util.ArrayList;

/**
 *
 * The value is used for notification storage.
 * Json String, coming from server, will be converted to one of these objects.
 */
public class Notifications {
    public class Like {

        public class data {
            public String media;
            public String postID;
            public String userID;
            public int filesNumber;
            public String profilePic;
            public String otherUserID;
        }
    }

    public class Comment {

        public class data {
            public String userID;
            public int filesNumber;
            public String profilePic;
            public String otherUserID;
        }

    }

    public class Follow {

        public class data {
            public String message;
            public int filesNumber;
            public ArrayList<String> posts;
        }
    }
}
