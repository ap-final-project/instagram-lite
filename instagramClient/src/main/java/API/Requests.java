package API;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Includes methods to create Json String request with a unique requestID
 * Saves the request into a HashMap with requestID as its key
 */
public class Requests {
    private final String OLD_PASSWORD = "oldPassword";
    private final String NEW_PASSWORD = "newPassword";
    private final String OTHER_USERID = "otherUserID";
    private final String REQUEST_ID = "requestID";
    private final String REPLIED_TO = "repliedTo";
    private final String PASSWORD = "password";
    private final String RECEIVER = "receiver";
    private final String FILENAME = "fileName";
    private final String CONTENT = "content";
    private final String CAPTION = "caption";
    private final String COMMENT = "comment";
    private final String CHAT_ID = "chatID";
    private final String USER_ID = "userID";
    private final String POST_ID = "postID";
    private final String LENGTH = "length";
    private final String RESULT = "result";
    private final String EMAIL = "email";
    private final String DATA = "data";
    private final String LAST = "last";
    private final String BIO = "bio";
    private JsonObject jsonObject;

    private JsonObject getJsonObject(String path) {
        JsonObject jsonObject = new JsonObject();
        try (FileReader reader = new FileReader(path)) {
            Gson gson = new Gson();
            JsonReader jsonReader = new JsonReader(reader);
            jsonObject = gson.fromJson(jsonReader, JsonObject.class);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    public String creatLoginRequest(long reqID, String ID, String pass) {
        this.jsonObject = getJsonObject(Paths.LOGIN_REQUEST_FILE_PATH);

        jsonObject.addProperty(REQUEST_ID, reqID);

        JsonObject data = new JsonObject();
        data.addProperty(USER_ID, ID);
        data.addProperty(PASSWORD, pass);
        jsonObject.add(DATA, data);

        return jsonObject.toString();
    }

    public String creatSignUpRequest(long reqID, String ID, String name, String email, String pass) {
        this.jsonObject = getJsonObject(Paths.SIGNUP_REQUEST_FILE_PATH);

        jsonObject.addProperty(REQUEST_ID, reqID);

        JsonObject data = new JsonObject();
        data.addProperty(USER_ID, ID);
        data.addProperty(CommonValues.USERNAME, name);
        data.addProperty(EMAIL, email);
        data.addProperty(PASSWORD, pass);
        jsonObject.add(DATA, data);

        return jsonObject.toString();
    }

    public String createVerifyRequest(long reqID, String email, boolean result) {
        this.jsonObject = getJsonObject(Paths.VERIFY_REQUEST_FILE_PATH);
        jsonObject.addProperty(REQUEST_ID, reqID);

        JsonObject data = new JsonObject();
        data.addProperty(EMAIL, email);
        data.addProperty(RESULT, result);
        jsonObject.add(DATA, data);

        return jsonObject.toString();
    }

    public String createForgotPassRequest(long reqID, String email) {
        this.jsonObject = getJsonObject(Paths.FORGOT_PASS_REQUEST_FILE_PATH);
        jsonObject.addProperty(REQUEST_ID, reqID);

        JsonObject data = new JsonObject();
        data.addProperty(EMAIL, email);
        jsonObject.add(DATA, data);

        return jsonObject.toString();
    }

    public String createChangeEmailRequest(long reqID, String email) {
        this.jsonObject = getJsonObject(Paths.CHANGE_EMAIL_REQUEST_FILE_PATH);
        jsonObject.addProperty(REQUEST_ID, reqID);

        JsonObject data = new JsonObject();
        data.addProperty(EMAIL, email);
        jsonObject.add(DATA, data);

        return jsonObject.toString();
    }

    public String createChangePassRequest(long reqID, String oldPassword, String newPassword) {
        this.jsonObject = getJsonObject(Paths.CHANGE_PASSWORD_REQUEST_FILE_PATH);
        jsonObject.addProperty(REQUEST_ID, reqID);

        JsonObject data = new JsonObject();
        data.addProperty(OLD_PASSWORD, oldPassword);
        data.addProperty(NEW_PASSWORD, newPassword);
        jsonObject.add(DATA, data);

        return jsonObject.toString();
    }

    public String createEditBioRequest(long reqID, String bio) {
        this.jsonObject = getJsonObject(Paths.EDIT_BIO_REQUEST_FILE_PATH);
        jsonObject.addProperty(REQUEST_ID, reqID);

        JsonObject data = new JsonObject();
        data.addProperty(BIO, bio);
        jsonObject.add(DATA, data);

        return jsonObject.toString();
    }

    public String createEditUserNameRequest(long reqID, String userName) {
        this.jsonObject = getJsonObject(Paths.EDIT_USERNAME_REQUEST_FILE_PATH);
        jsonObject.addProperty(REQUEST_ID, reqID);

        JsonObject data = new JsonObject();
        data.addProperty(CommonValues.USERNAME, userName);
        jsonObject.add(DATA, data);

        return jsonObject.toString();
    }

    public String createEditProfilePicRequest(long reqID, String fileName) {
        this.jsonObject = getJsonObject(Paths.EDIT_PROFILE_PIC_REQUEST_FILE_PATH);
        jsonObject.addProperty(REQUEST_ID, reqID);

        JsonObject data = new JsonObject();
        data.addProperty(FILENAME, fileName);
        jsonObject.add(DATA, data);

        return jsonObject.toString();
    }

    public String createRemoveProfilePicRequest(long reqID) {
        this.jsonObject = getJsonObject(Paths.REMOVE_PROFILE_PIC_REQUEST_FILE_PATH);
        jsonObject.addProperty(REQUEST_ID, reqID);

        JsonObject data = new JsonObject();
        jsonObject.add(DATA, data);

        return jsonObject.toString();
    }

    public String createAddPostRequest(long reqID, String fileName, String caption) {
        this.jsonObject = getJsonObject(Paths.POST_REQUEST_FILE_PATH);
        jsonObject.addProperty(REQUEST_ID, reqID);

        JsonObject data = new JsonObject();
        data.addProperty(FILENAME, fileName);
        data.addProperty(CAPTION, caption);
        jsonObject.add(DATA, data);

        return jsonObject.toString();
    }

    public String createGetTimeLineRequest(long reqID, String last) {
        this.jsonObject = getJsonObject(Paths.GET_TIMELINE_REQUEST_FILE_PATH);
        jsonObject.addProperty(REQUEST_ID, reqID);

        JsonObject data = new JsonObject();
        data.addProperty(LAST, last);
        jsonObject.add(DATA, data);

        return jsonObject.toString();
    }

    public String createGetActivitiesRequest(long reqID, String last) {
        this.jsonObject = getJsonObject(Paths.GET_ACTIVITIES_REQUEST_FILE_PATH);
        jsonObject.addProperty(REQUEST_ID, reqID);

        JsonObject data = new JsonObject();
        data.addProperty(LAST, last);
        jsonObject.add(DATA, data);

        return jsonObject.toString();
    }


    public String createGetMainUserProfileRequest(long reqID) {
        this.jsonObject = getJsonObject(Paths.GET_MAIN_USER_PROFILE_REQUEST_FILE_PATH);
        jsonObject.addProperty(REQUEST_ID, reqID);

        JsonObject data = new JsonObject();
        jsonObject.add(DATA, data);

        return jsonObject.toString();
    }


    public String createLikeRequest(long reqID, String otherUser, String postID) {
        this.jsonObject = getJsonObject(Paths.LIKE_REQUEST_FILE_PATH);
        jsonObject.addProperty(REQUEST_ID, reqID);

        JsonObject data = new JsonObject();
        data.addProperty(POST_ID, postID);
        data.addProperty(OTHER_USERID, otherUser);
        jsonObject.add(DATA, data);

        return jsonObject.toString();
    }

    public String createUnLikeRequest(long reqID, String otherUser, String postID) {
        this.jsonObject = getJsonObject(Paths.UNLIKE_REQUEST_FILE_PATH);
        jsonObject.addProperty(REQUEST_ID, reqID);

        JsonObject data = new JsonObject();
        data.addProperty(POST_ID, postID);
        data.addProperty(OTHER_USERID, otherUser);
        jsonObject.add(DATA, data);

        return jsonObject.toString();
    }

    public String createAddCommentRequest(long reqID,
                                          String repliedTo, String postID,
                                          String comment, String otherUserID) {
        this.jsonObject = getJsonObject(Paths.COMMENT_REQUEST_FILE_PATH);
        jsonObject.addProperty(REQUEST_ID, reqID);

        JsonObject data = new JsonObject();
        data.addProperty(POST_ID, postID);
        data.addProperty(REPLIED_TO, repliedTo);
        data.addProperty(COMMENT, comment);
        data.addProperty(OTHER_USERID, otherUserID);
        jsonObject.add(DATA, data);

        return jsonObject.toString();
    }

    public String createGetCommentsListRequest(long reqID, String postID, String last) {
        this.jsonObject = getJsonObject(Paths.GET_COMMENT_LIST_REQUEST_FILE_PATH);
        jsonObject.addProperty(REQUEST_ID, reqID);

        JsonObject data = new JsonObject();
        data.addProperty(POST_ID, postID);
        data.addProperty(LAST, last);
        jsonObject.add(DATA, data);

        return jsonObject.toString();
    }

    public String createGetLikesListRequest(long reqID, String postID, String last) {
        this.jsonObject = getJsonObject(Paths.GET_LIKE_LIST_REQUEST_FILE_PATH);
        jsonObject.addProperty(REQUEST_ID, reqID);

        JsonObject data = new JsonObject();
        data.addProperty(POST_ID, postID);
        data.addProperty(LAST, last);
        jsonObject.add(DATA, data);

        return jsonObject.toString();
    }

    public String createGetFollowersListRequest(long reqID, String userID, String last) {
        this.jsonObject = getJsonObject(Paths.GET_FOLLOWERS_LIST_REQUEST_FILE_PATH);
        jsonObject.addProperty(REQUEST_ID, reqID);

        JsonObject data = new JsonObject();
        data.addProperty(USER_ID, userID);
        data.addProperty(LAST, last);
        jsonObject.add(DATA, data);

        return jsonObject.toString();
    }

    public String createGetFollowingListRequest(long reqID, String userID, String last) {
        this.jsonObject = getJsonObject(Paths.GET_FOLLOWING_LIST_REQUEST_FILE_PATH);
        jsonObject.addProperty(REQUEST_ID, reqID);

        JsonObject data = new JsonObject();
        data.addProperty(USER_ID, userID);
        data.addProperty(LAST, last);
        jsonObject.add(DATA, data);

        return jsonObject.toString();
    }

    public String createFollowRequest(long reqID, String otherUser) {
        this.jsonObject = getJsonObject(Paths.FOLLOW_REQUEST_FILE_PATH);
        jsonObject.addProperty(REQUEST_ID, reqID);

        JsonObject data = new JsonObject();
        data.addProperty(USER_ID, otherUser);
        jsonObject.add(DATA, data);

        return jsonObject.toString();
    }

    public String createUnfollowRequest(long reqID, String otherUser) {
        this.jsonObject = getJsonObject(Paths.UNFOLLOW_REQUEST_FILE_PATH);
        jsonObject.addProperty(REQUEST_ID, reqID);

        JsonObject data = new JsonObject();
        data.addProperty(USER_ID, otherUser);
        jsonObject.add(DATA, data);

        return jsonObject.toString();
    }

    public String createGetUserProfileRequest(long reqID, String otherUser) {
        this.jsonObject = getJsonObject(Paths.GET_USER_PROFILE_REQUEST_FILE_PATH);
        jsonObject.addProperty(REQUEST_ID, reqID);

        JsonObject data = new JsonObject();
        data.addProperty(USER_ID, otherUser);
        jsonObject.add(DATA, data);

        return jsonObject.toString();
    }

    public String createDeletePostRequest(long reqID, String postID) {
        this.jsonObject = getJsonObject(Paths.DELETE_POST_REQUEST_FILE_PATH);
        jsonObject.addProperty(REQUEST_ID, reqID);

        JsonObject data = new JsonObject();
        data.addProperty(POST_ID, postID);
        jsonObject.add(DATA, data);

        return jsonObject.toString();
    }

    public String createGerMorePostRequest(long reqID, String userID, String last) {
        this.jsonObject = getJsonObject(Paths.GET_MORE_POST_REQUEST_FILE_PATH);
        jsonObject.addProperty(REQUEST_ID, reqID);

        JsonObject data = new JsonObject();
        data.addProperty(USER_ID, userID);
        data.addProperty(LAST, last);
        jsonObject.add(DATA, data);

        return jsonObject.toString();
    }

    public String createSendDirectMessageRequest(long reqID, String receiver, String content) {
        this.jsonObject = getJsonObject(Paths.SEND_DIRECT_MESSAGE_REQUEST_FILE_PATH);
        jsonObject.addProperty(REQUEST_ID, reqID);

        JsonObject data = new JsonObject();
        data.addProperty(RECEIVER, receiver);
        data.addProperty(CONTENT, content);
        jsonObject.add(DATA, data);

        return jsonObject.toString();
    }

    public String createGetDirectMessagesRequest(long reqID, String chatID, String length) {
        this.jsonObject = getJsonObject(Paths.GET_DIRECT_MESSAGES_REQUEST_FILE_PATH);
        jsonObject.addProperty(REQUEST_ID, reqID);

        JsonObject data = new JsonObject();
        data.addProperty(CHAT_ID, chatID);
        data.addProperty(LENGTH, length);
        jsonObject.add(DATA, data);

        return jsonObject.toString();
    }

    public String createLogoutRequest(long reqID) {
        this.jsonObject = getJsonObject(Paths.LOGOUT_REQUEST_FILE_PATH);
        jsonObject.addProperty(REQUEST_ID, reqID);

        return jsonObject.toString();
    }

    public String createDeleteAccountRequest(long reqID, String password) {
        this.jsonObject = getJsonObject(Paths.DELETE_ACCOUNT_REQUEST_FILE_PATH);
        jsonObject.addProperty(REQUEST_ID, reqID);

        JsonObject data = new JsonObject();
        data.addProperty(PASSWORD, password);
        jsonObject.add(DATA, data);
        return jsonObject.toString();
    }

    public String createGetChatListRequest(long reqID) {
        this.jsonObject = getJsonObject(Paths.GET_CHAT_LIST_REQUEST_FILE_PATH);
        jsonObject.addProperty(REQUEST_ID, reqID);

        return jsonObject.toString();
    }


}