package API;

import models.DirectMessage;

import java.util.ArrayList;

/**
 * The value is used for responses storage.
 * Json String, coming from server, will be converted to one of these objects.
 */
public class Responses {

    public class Login {
        public boolean result;

        public class data {
            public String bio;
            public String email;
            public String userID;
            public int postNumber;
            public String message;
            public String userName;
            public String password;
            public int filesNumber;
            public String profilePic;
            public int followersNumber;
            public int followingNumber;
            public ArrayList<String> posts;
        }
    }

    public class SignUp {
        public boolean result;

        public class data {
            public String code;

            public String message;
        }

    }

    public class Verify {
        public boolean result;

        public class data {
            public String message;
        }

    }

    public class ForgotPassword {
        public boolean result;

        public class data {
            public String message;
        }
    }

    public class EditUserName {
        public boolean result;

        public class data {
            public String message;
            public String userName;
        }
    }

    public class EditBio {
        public boolean result;

        public class data {
            public String message;
            public String bio;
        }
    }

    public class ChangeEmail {
        public boolean result;

        public class data {
            public String message;
            public String email;
        }
    }

    public class EditProfilePic {
        public boolean result;

        public class data {
            public String message;
        }
    }

    public class RemoveProfilePic {
        public boolean result;

        public class data {
            public String message;
        }
    }

    public class GetTimeline {
        public boolean result;

        public class data {
            public String message;
            public int filesNumber;
            public ArrayList<String> posts;
        }
    }

    public class AddPost {
        public boolean result;

        public class data {
            public String postID;
            public String message;
        }
    }

    public class GetMainUserProfile {
        public boolean result;

        public class data {
            public String bio;
            public String email;
            public String userID;
            public int postNumber;
            public String message;
            public String userName;
            public String password;
            public int filesNumber;
            public String profilePic;
            public int followersNumber;
            public int followingNumber;
            public ArrayList<String> posts;
        }
    }


    public class Like {
        public boolean result;

        public class data {
            public String message;
        }
    }

    public class Unlike {
        public boolean result;

        public class data {
            public String message;
        }
    }


    public class AddComment {
        public boolean result;

        public class data {
            public String message;
            public String commentID;
        }
    }


    public class GetCommentList {
        public boolean result;

        public class data {
            public String message;
            public int filesNumber;
            public ArrayList<String> comments;
        }
    }


    public class GetLikeList {
        public boolean result;

        public class data {
            public String message;
            public int filesNumber;
            public ArrayList<String> likes;
        }
    }

    public class GetFollowersList {
        public boolean result;

        public class data {
            public String message;
            public int filesNumber;
            public ArrayList<String> followers;
        }
    }

    public class GetFollowingList {
        public boolean result;

        public class data {
            public String message;
            public int filesNumber;
            public ArrayList<String> following;
        }
    }

    public class Follow {
        public boolean result;

        public class data {
            public String message;
        }
    }

    public class Unfollow {
        public boolean result;

        public class data {
            public String message;
        }
    }

    public class GetUserProfile {
        public boolean result;

        public class data {
            public String bio;
            public String userID;
            public int postNumber;
            public String message;
            public String userName;
            public String password;
            public int filesNumber;
            public boolean followed;
            public String profilePic;
            public int followersNumber;
            public int followingNumber;
            public ArrayList<String> posts;
        }
    }

    public class DeletePost {
        public boolean result;

        public class data {
            public String message;
        }
    }

    public class GetChatList {
        public boolean result;

        public class data {
            public String message;
            public int filesNumber;
            public ArrayList<String> chats;
        }
    }

    public class GetDirectMessages {
        public boolean result;

        public class data {
            public String message;
            public ArrayList<String> directMessages;
        }

    }


    public class DeleteAccount {
        public boolean result;

        public class data {
            public String message;
        }
    }

    public class Logout {
        public boolean result;

        public class data {
            public String message;
        }
    }

    public class GetActivities {
        public boolean result;

        public class data {
            public String message;
            public int filesNumber;
            public ArrayList<String> activities;
        }
    }

    public class ChangePassword {
        public boolean result;

        public class data {
            public String message;
            public String password;
        }
    }


    public class SendDirectMessage {
        public boolean result;

        public class data {
            public String message;
            public DirectMessage directMessage;
        }
    }


}
