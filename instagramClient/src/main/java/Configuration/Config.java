package Configuration;

import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import java.util.ResourceBundle;

public class Config {
    private final ResourceBundle resourceBundle;
    private static volatile Config configSingleton;


    public static Config getInstance() {
        if (configSingleton == null) {
            synchronized (Config.class) {
                if (configSingleton == null) {
                    configSingleton = new Config();
                }
            }
        }

        return configSingleton;
    }

    private Config() {
        this.resourceBundle = ResourceBundle.getBundle("config");
        if (configSingleton != null){
            throw new RuntimeException("Use getInstance() method to get the single instance of this class.");
        }
    }

    public int getIntValue(String str) {
        return Integer.parseInt(resourceBundle.getString(str));
    }

    public String getStringValue(String str) {
        return resourceBundle.getString(str);
    }

    public Boolean getBooleanValue(String str) {
        return resourceBundle.getString(str).equalsIgnoreCase("true");
    }
}
