package Controller;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import models.Activity;

import java.util.ArrayList;

public class ActivityBoxCtrl {
    private Text id;
    private Text msg;
    private Text text;
    private Text date;
    private Activity activity;
    public static ArrayList<Activity> directNotifications;

    @FXML
    private Circle circleImage;

    @FXML
    private TextFlow textFlow;

    @FXML
    private ImageView postImageView;

    @FXML
    private Pane pane;

    @FXML
    private void paneAction(MouseEvent event) {
        //todo:
    }

    public void setData(Activity ac) {
        this.activity = ac;
        this.id = new Text();
        this.msg = new Text();
        this.date = new Text();
        this.text = new Text();

        setProfImage();
        System.out.println("id: " + ac.getUserID());

        switch (activity.getType()) {
            case LIKE -> likeNotification();

            case DIRECT -> directNotifications.add(activity);

            case FOLLOW -> followNotification();

            case COMMENT -> commentNotification();

            case MENTION -> mentionNotification();
        }
    }

    private void likeNotification() {
        setPostImage();
        id.setText(activity.getUserID());
        id.getStyleClass().add("ID-text");

        text.setText(" liked your photo.");
        date.setText("\n" +activity.getDate());

        textFlow.getChildren().addAll(id, text, date);
    }

    private void mentionNotification() {
        setPostImage();
        id.setText(activity.getUserID());
        id.getStyleClass().add("ID-text");

        text.setText(" mentioned you in a comment:\n");
        msg.setText(activity.getComment());
        date.setText("\n" +activity.getDate());

        textFlow.getChildren().addAll(id, text, msg, date);
    }

    private void followNotification() {
        id.setText(activity.getUserID());
        id.getStyleClass().add("ID-text");

        text.setText(" started following you.");
        date.setText("\n" +activity.getDate());

        textFlow.getChildren().addAll(id, text, date);
    }

    private void commentNotification() {
        setPostImage();
        id.setText(activity.getUserID());
        id.getStyleClass().add("ID-text");

        text.setText(" commented:\n");
        System.out.println("comment: " + activity.getComment());
        msg.setText(activity.getComment());
        date.setText("\n" + activity.getDate());

        textFlow.getChildren().addAll(id, text, msg, date);
    }

    private void setProfImage() {
        Image image = activity.getProfImage();
        if (image == null) {
            circleImage.setVisible(false);
            FontAwesomeIconView avatar = new FontAwesomeIconView();
            avatar.setGlyphName("USER_CIRCLE");
            avatar.setSize("66");
            avatar.setLayoutX(14);
            avatar.setLayoutY(65);
            avatar.setFill(Color.web("#a8a5a5"));
            pane.getChildren().add(avatar);
        } else {
            circleImage.setFill(new ImagePattern(image));
        }
    }

    private void setPostImage() {
        postImageView.setImage(activity.getPostImage());
    }


}
