package Controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.VBox;
import models.Activity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.BrokenBarrierException;

import static Controller.LoginCtrl.*;

public class ActivityPageCtrl {
    private String lastActivityID;
    private ArrayList<Activity> activities;

    @FXML
    private VBox vBox;

    public void sendRequest() throws BrokenBarrierException, InterruptedException, IOException {
        long reqId = mainConnection.getActivitiesReq(null);
        cyclicBarrier.await();
        activities = responseHandler.handleGetActivitiesResponse(reqId);

        if (activities.size() > 0) {
            lastActivityID = activities.get(activities.size() - 1).getPostID();
        } else {
            lastActivityID = null;
        }

        /*if (lastActivityID != null) {
            Collections.reverse(activities);
        }*/

        setActivities();
    }

    private void setActivities() throws IOException {
        for (Activity activity : activities) {
            FXMLLoader loader = StartLoader.loadFxml("/activityBox.fxml");
            Parent root = loader.load();
            ActivityBoxCtrl activityBoxCtrl = loader.getController();
            activityBoxCtrl.setData(activity);
            vBox.getChildren().addAll(root);
        }
    }
}
