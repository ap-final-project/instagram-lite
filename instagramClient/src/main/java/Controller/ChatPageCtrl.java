package Controller;

import com.jfoenix.controls.JFXButton;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import models.DirectMessage;
import models.Message;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.concurrent.BrokenBarrierException;

import static Controller.LoginCtrl.*;


public class ChatPageCtrl implements Initializable {
    private DirectMessage directMessage;
    private ArrayList<Message> messages;

    @FXML
    private Circle circleImg;

    @FXML
    private Label usernameLbl;

    @FXML
    private VBox vBox;

    @FXML
    private TextField messageTextField;

    @FXML
    private TextFlow emojiList;

    @FXML
    private JFXButton btnEmoji;

    @FXML
    private Pane upperPane;

    public void setData(DirectMessage directMessage) {
        this.directMessage = directMessage;

        usernameLbl.setText(directMessage.getUserID());
        Image image = directMessage.getProfileImage();
        if (image == null) {
            circleImg.setVisible(false);
            FontAwesomeIconView avatar = new FontAwesomeIconView();
            avatar.setGlyphName("USER_CIRCLE");
            avatar.setSize("44");
            avatar.setLayoutX(24);
            avatar.setLayoutY(40);
            avatar.setFill(Color.web("#a8a5a5"));
            upperPane.getChildren().add(avatar);
        } else {
            circleImg.setFill(new ImagePattern(image));
        }

        messages = directMessage.getMessages();


        for (Message message : messages) {
            createChatBox(message);
        }

    }

    @FXML
    private void emojiAction(ActionEvent event) {
        if (emojiList.isVisible()) {
            emojiList.setVisible(false);
        } else {
            emojiList.setVisible(true);
        }
    }

    private void createChatBox(Message message) {
        Text text = new Text(message.getContent());

        text.setFill(Color.WHITE);
        text.getStyleClass().add("message");
        TextFlow tempFlow = new TextFlow();
        tempFlow.getChildren().add(text);
        tempFlow.setMaxWidth(200);

        TextFlow flow = new TextFlow(tempFlow);

        HBox hbox = new HBox(12);

        if (!mainUser.getUserID().equals(message.getSender())) {
            tempFlow.getStyleClass().add("tempFlowFlipped");
            flow.getStyleClass().add("textFlowFlipped");
            hbox.setAlignment(Pos.CENTER_LEFT);
            hbox.getChildren().add(flow);

        } else {
            text.setFill(Color.WHITE);
            tempFlow.getStyleClass().add("tempFlow");
            flow.getStyleClass().add("textFlow");
            hbox.setAlignment(Pos.BOTTOM_RIGHT);
            hbox.getChildren().add(flow);
        }

        hbox.getStyleClass().add("hbox");
        Platform.runLater( () -> vBox.getChildren().addAll(hbox));
    }

    @FXML
    private void sendMsgAction(MouseEvent event) throws BrokenBarrierException, InterruptedException {
        if (!messageTextField.getText().trim().equals("")) {
            Message m = new Message(directMessage.getUserID(), mainUser.getUserID(), messageTextField.getText().trim());
            long id = mainConnection.sendDirectMessageReq(m.getReceiver(), messageTextField.getText().trim());
            cyclicBarrier.await();
            boolean result = responseHandler.handleSendMessageResponse(id);

            if (result) {
                createChatBox(m);
            }

        }

        messageTextField.setText("");
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        for (Node text : emojiList.getChildren()) {
            text.setOnMouseClicked(event -> {
                messageTextField.setText(messageTextField.getText() + " " + ((Text) text).getText());
                emojiList.setVisible(false);
            });
        }


    }
}
