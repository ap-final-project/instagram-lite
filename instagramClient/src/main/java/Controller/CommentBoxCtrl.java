package Controller;

import com.jfoenix.controls.JFXTextArea;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import models.Comment;

import static Controller.LoginCtrl.mainUser;

public class CommentBoxCtrl {

    @FXML
    private Circle imgCircle;

    @FXML
    public TextArea commentTxt;

    @FXML
    public Text replyUsr;

    @FXML
    public Text usrIDLabel;

    @FXML
    public Text date;

    public void setComments(Comment comment) {
        replyUsr.setText(comment.getRepliedTo());
        usrIDLabel.setText(comment.getUserID());

        imgCircle.setFill(new ImagePattern(comment.getImage()));
        Image image = mainUser.getAx();
        if (image != null) {
            imgCircle.setVisible(true);
            imgCircle.setFill(new ImagePattern(image));
        } else {
            imgCircle.setVisible(false);
            FontAwesomeIconView avatar = new FontAwesomeIconView();
            avatar.setVisible(true);
            avatar.setGlyphName("USER_CIRCLE");
            avatar.setSize("60");
            avatar.setLayoutX(5);
            avatar.setLayoutY(80);
            avatar.setFill(Color.web("#a8a5a5"));
        }

        date.setText(comment.getDate());

        commentTxt.setText(comment.getComment());
    }


}
