package Controller;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import models.Comment;
import models.Post;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.BrokenBarrierException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static Controller.LoadingFXMLs.loadCommentBoxPageFXML;
import static Controller.LoginCtrl.*;

public class CommentPageCtrl {
    private Post post;
    private String lastCommentID;

    @FXML
    private VBox commentsVBox;

    @FXML
    private TextField commentField;

    public void setPostForPage(Post post) {
        this.post = post;
    }


    private Pane normalComment(Comment comment) throws IOException {
        Pane pane = new Pane();
        pane.setPrefWidth(380);
        pane.setPrefHeight(80);
        pane.minWidth(Region.USE_PREF_SIZE);
        pane.minHeight(Region.USE_PREF_SIZE);
        pane.maxWidth(Region.USE_PREF_SIZE);
        pane.maxHeight(Region.USE_PREF_SIZE);
        pane.setStyle("-fx-background-color: white");
        pane.setStyle("-fx-border-color:  #cdcdcd");
        pane.setStyle("-fx-border-width: 0px 0px 2px 0px");

        Circle circle = new Circle(30);
        if (comment.getImage() == null) {
            circle.setVisible(false);
            FontAwesomeIconView avatar = new FontAwesomeIconView();
            avatar.setGlyphName("USER_CIRCLE");
            avatar.setSize("50");
            avatar.setX(15);
            avatar.setY(50);
            avatar.setFill(Color.web("#a8a5a5"));
            pane.getChildren().add(avatar);
        } else {
            circle.setFill(new ImagePattern(comment.getImage()));
            circle.setLayoutX(35);
            circle.setLayoutY(40);
        }

        Text idText = new Text(comment.getUserID());
        idText.setStyle("-fx-font-weight: Bold");
        idText.setStyle("-fx-font-family: Loma");
        idText.setStyle("-fx-font-size: 15");
        idText.setLayoutX(80);
        idText.setLayoutY(25);

        Text dateText = new Text();
        dateText.setStyle("-fx-font-size: 11");
        dateText.setLayoutX(220);
        dateText.setLayoutY(25);
        dateText.setText(comment.getDate());

        Label commentLabel = new Label(comment.getComment());
        commentLabel.setWrapText(true);
        commentLabel.setStyle("-fx-font-size: 12");
        commentLabel.setPrefWidth(290);
        commentLabel.setPrefHeight(55);
        commentLabel.minWidth(Region.USE_PREF_SIZE);
        commentLabel.minHeight(Region.USE_PREF_SIZE);
        commentLabel.maxWidth(Region.USE_PREF_SIZE);
        commentLabel.maxHeight(Region.USE_PREF_SIZE);
        commentLabel.setLayoutX(80);
        commentLabel.setLayoutY(35);
        commentLabel.setAlignment(Pos.TOP_LEFT);

        pane.getChildren().addAll(commentLabel, dateText, idText, circle);
        return pane;
    }

    private String checkRegexForReply() {
        String regex = "@[a-z0-9-_]+\\.*[a-z0-9-_]+";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(commentField.getText());

        if (matcher.find()) {
            int startIndex = matcher.start();
            int endIndex = matcher.end();
            return commentField.getText().substring(startIndex, endIndex);
        }
        return null;
    }

    private Parent replyComment(Comment comment) throws IOException, BrokenBarrierException, InterruptedException {
        return loadCommentBoxPageFXML(comment, commentField);
    }

    @FXML
    private void sendCommentAction(ActionEvent event) {
        String repliedID = checkRegexForReply();

        Comment comment = new Comment(mainUser.getUserID(), repliedID,
                post.getPostID(), commentField.getText(), post.getUserID(), mainUser.getProfilePic());

        try {
            long id = mainConnection.addCommentReq(comment.getRepliedTo(),
                    comment.getPostID(), comment.getComment(), comment.getOtherUserID());
            cyclicBarrier.await();
            Object result = responseHandler.handleAddCommentResponse(id);

            Parent root = null;
            if (result instanceof String) {
                comment.setCommentID(String.valueOf(result));

                if (comment.getRepliedTo() == null) {
                    root = normalComment(comment);

                    root.setOnMouseClicked(mouseEvent -> {
                        commentField.setText("@" + comment.getUserID());

                    });
                } else {
                    root = replyComment(comment);

                    root.setOnMouseClicked(mouseEvent -> {
                        commentField.setText("@" + new CommentBoxCtrl().usrIDLabel.getText());
                    });
                }
            }
            commentsVBox.getChildren().add(root);

        } catch (InterruptedException | BrokenBarrierException | IOException interruptedException) {
            interruptedException.printStackTrace();
        }


        commentField.setText("");
    }

    public void sendRequest() throws BrokenBarrierException, InterruptedException, IOException {
        long id = mainConnection.getCommentsListReq(post.getPostID(), lastCommentID);
        cyclicBarrier.await();
        ArrayList<Comment> comments = responseHandler.handleGetCommentsListResponse(id);

        if (comments.size() > 0) {
            lastCommentID = comments.get(comments.size() - 1).getPostID();
        } else {
            lastCommentID = null;
        }

        Parent root;
        for (Comment c : comments) {
            if (c.getRepliedTo() == null) {
                root = normalComment(c);
            } else {
                root = replyComment(c);
            }
            commentsVBox.getChildren().add(root);
        }

    }

}
