package Controller;

import API.Messages;
import com.jfoenix.controls.JFXButton;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.print.Collation;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import models.*;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.ResourceBundle;
import java.util.concurrent.BrokenBarrierException;

import static Controller.ActivityBoxCtrl.directNotifications;
import static Controller.LoadingFXMLs.*;
import static Controller.LoginCtrl.*;
import static Controller.MainPageCtrl.mainStackPane;
import static Controller.MainPageCtrl.showDialog;
import static Controller.ProfilePageCtrl.mainPageCtrl;
import static Controller.ProfilePageCtrl.sendRequest;

public class EmptyPageCtrl implements Initializable {
    private String ID;
    private String state;
    private final String LIKE_PAGE = "LikePage";
    private final String DIRECT_PAGE = "DirectPage";
    private final String EXPLORE_PAGE = "ExplorePage";
    private final String FOLLOWER_FOLLOWING_PAGE = "FwrFwgPage";

    @FXML
    private TextField searchField;

    @FXML
    private VBox vBox;

    @FXML
    private Pane emptyPane;

    @FXML
    private void search(ActionEvent event) throws BrokenBarrierException, InterruptedException, IOException {
        if (state.equals(EXPLORE_PAGE)) {
            long reqId = mainConnection.getUserProfileReq(searchField.getText().trim());
            cyclicBarrier.await();
            User user = responseHandler.handleGetUserProfileResponse(reqId);

            if (user != null) {
                vBox.getChildren().add(loadUserBoxPageFXML(emptyPane, user));
            } else {
                showDialog(mainStackPane, Messages.GET_USER_PROFILE_UNSUCCESSFUL);
            }
        }
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setID(String id) {
        this.ID = id;
    }

    private void setUndo() {
        /*mainPageCtrl.backLbl.setOnMouseClicked(event -> {
            if (state.equals(FOLLOWER_FOLLOWING_PAGE) || state.equals(LIKE_PAGE)) {
                try {
                    User user = null;
                    if (ID.equals(mainUser.getUserID())) {
                        sendRequest();
                        emptyPane.getChildren().setAll(loadProfilePageFXML(mainPageCtrl));
                        mainPageCtrl.backLbl.setVisible(false);
                        System.out.println("set shod1");

                    } else {
                        long reqID = mainConnection.getUserProfileReq(ID);
                        cyclicBarrier.await();
                        user = responseHandler.handleGetUserProfileResponse(reqID);

                        emptyPane.getChildren().setAll(loadOtherProfilePageFXML(user));
                        mainPageCtrl.backLbl.setVisible(false);
                        System.out.println("set shod2");

                    }

                } catch (InterruptedException | BrokenBarrierException | IOException e) {
                    e.printStackTrace();
                }

            } else if (state.equals(DIRECT_PAGE)) {
                try {
                    emptyPane.getChildren().setAll(loadHomePageFXML());
                    mainPageCtrl.backLbl.setVisible(false);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });*/
    }


    public void setDataForLikers(ArrayList<Like> list) throws IOException {
        state = LIKE_PAGE;
        for (Like like : list) {
            vBox.getChildren().add(loadUserBoxPageFXML(emptyPane, like));
        }
    }

    public void setDataForFwrFwg(ArrayList<User> list) throws IOException {
        state = FOLLOWER_FOLLOWING_PAGE;
        for (User user : list) {
            vBox.getChildren().add(loadUserBoxPageFXML(emptyPane, user));
        }
    }

    public void setDataForDirects(ArrayList<DirectMessage> directs) {
        state = DIRECT_PAGE;
        Collections.reverse(directs);
        for (DirectMessage DM : directs) {
            vBox.getChildren().add(createPaneForUser(DM));
        }
    }

    public Pane createPaneForUser(DirectMessage DM) {
        Pane pane = new Pane();
        pane.setPrefWidth(500);
        pane.setPrefHeight(70);
        pane.setStyle("-fx-background-color: white");


        Pane notifPane = new Pane();
        Label numberOfUnReadMsg = new Label();
        numberOfUnReadMsg.setPrefWidth(35);
        numberOfUnReadMsg.setPrefHeight(35);
        notifPane.setPrefWidth(35);
        notifPane.setPrefHeight(35);
        notifPane.setLayoutX(460);
        notifPane.setLayoutY(20);
        notifPane.setStyle("-fx-background-color: #aecdeb");
        notifPane.getStyleClass().add("notif-icon");
        notifPane.getChildren().add(numberOfUnReadMsg);

        /*if (!directNotifications.isEmpty()) {
            //todo: check kon
            for (Activity a : directNotifications) {
                if (a.getOtherUserID().equals(DM.getUserID())) {
                    pane.getChildren().add(notifPane);
                }
            }
        }*/


        Label IDLabel = new Label();
        Label msgLabel = new Label();
        JFXButton button = new JFXButton();
        Circle circleImg = new Circle(30);
        FontAwesomeIconView avatar = new FontAwesomeIconView();

        circleImg.setLayoutX(45);
        circleImg.setLayoutY(35);
        circleImg.setStrokeWidth(0);
        Image image = DM.getProfileImage();
        if (image != null) {
            circleImg.setFill(new ImagePattern(image));
        } else {
            circleImg.setVisible(false);
            avatar.setGlyphName("USER_CIRCLE");
            avatar.setSize("60");
            avatar.setLayoutX(15);
            avatar.setLayoutY(55);
            avatar.setFill(Color.web("#a8a5a5"));
            pane.getChildren().add(avatar);
        }

        IDLabel.setLayoutX(100);
        IDLabel.setLayoutY(6);
        IDLabel.setPrefWidth(250);
        IDLabel.setPrefHeight(25);
        IDLabel.setStyle("-fx-font-family: Loma");
        IDLabel.setStyle("-fx-font-size: 20");
        IDLabel.setStyle("-fx-font-weight: Bold");
        IDLabel.setText(DM.getUserID());

        msgLabel.setLayoutX(100);
        msgLabel.setLayoutY(40);
        msgLabel.setPrefWidth(380);
        msgLabel.setPrefHeight(20);
        msgLabel.setStyle("-fx-font-family: Loma");
        msgLabel.setStyle("-fx-font-size: 12");
        msgLabel.setText(DM.getLastMessage());

        pane.setOnMouseClicked(e -> {
            paneAction(DM);
        });

        pane.getChildren().addAll(button, IDLabel, circleImg, msgLabel);
        return pane;
    }

    private void paneAction(DirectMessage DM) {
        try {
            long id = mainConnection.getDirectMessagesReq(DM.getChatID(), null);
            cyclicBarrier.await();
            ArrayList<Message> chats = responseHandler.handleGetDirectMessagesResponse(id);
            DM.setMessages(chats);

            emptyPane.getChildren().addAll(loadChatPageFXML(DM));

        } catch (InterruptedException | BrokenBarrierException | IOException interruptedException) {
            interruptedException.printStackTrace();
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        mainPageCtrl.backLbl.setVisible(true);
        mainPageCtrl.backLbl.getStyleClass().add("back-btn");


    }
}
