package Controller;

import API.Messages;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.io.IOException;
import java.util.concurrent.BrokenBarrierException;

import static Controller.LoadingFXMLs.loadLoginPageFXML;
import static Controller.LoginCtrl.*;


public class ForgotPassCtrl {

    @FXML
    private Pane pane;

    @FXML
    private Label errorLbl;

    @FXML
    private TextField emailField;

    @FXML
    void backBtnHandler(MouseEvent event) throws IOException {
        pane.getChildren().setAll(loadLoginPageFXML());
    }

    @FXML
    void resetPassHandler(ActionEvent event) throws InterruptedException, IOException, BrokenBarrierException {
        errorLbl.setText("");
        if (isValidEmailAddress(emailField.getText())) {
            makeInstance();

            long reqId = mainConnection.forgotPassReq(emailField.getText());
            cyclicBarrier.await();
            boolean result = responseHandler.handleForgotPassResponse(reqId);

            if (result) {
                emailField.setText("");
                errorLbl.setTextFill(Color.rgb(0, 120, 0));
                errorLbl.setText(Messages.FORGOT_PASS_SUCCESSFUL);
            } else {
                errorLbl.setText(Messages.FORGOT_PASS_ERROR);
            }
        } else {
            errorLbl.setText(Messages.CHANGE_EMAIL_INCORRECT);
        }
    }

    public static boolean isValidEmailAddress(String email) {
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            result = false;
        }
        return result;
    }

    @FXML
    void closeBtnHandler(MouseEvent event) {
        StartLoader.stage.close();
    }

    @FXML
    void minimizeBtnHandler(MouseEvent event) {
        StartLoader.stage.setIconified(true);
    }

}
