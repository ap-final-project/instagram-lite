package Controller;

import API.Messages;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Skin;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import models.DirectMessage;
import models.Post;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.ResourceBundle;
import java.util.concurrent.BrokenBarrierException;

import static Controller.LoadingFXMLs.loadEmptyPageFXML;
import static Controller.LoadingFXMLs.loadPostPageFXML;
import static Controller.LoginCtrl.*;
import static Controller.MainPageCtrl.mainStackPane;
import static Controller.MainPageCtrl.showDialog;


public class HomePageCtrl implements Initializable {
    private String lastPostId;

    @FXML
    private Pane mainPane;

    @FXML
    private Pane paneHomePage;

    @FXML
    private VBox vBoxMainPage;

    @FXML
    private ScrollPane timeLineScrollPane;

    @FXML
    private void directMessageHandler(MouseEvent event) throws IOException, BrokenBarrierException, InterruptedException {
        long id = mainConnection.getChatListReq();
        cyclicBarrier.await();
        ArrayList<DirectMessage> directs = responseHandler.handleGetChatListResponse(id);

        paneHomePage.getChildren().setAll(loadEmptyPageFXML(directs, mainUser.getUserID()));
    }

    @FXML
    private void timeLineSPFinishedAction(ScrollEvent event) throws IOException, BrokenBarrierException, InterruptedException {
        sendRequestToGetPosts();
    }


    private void sendRequestToGetPosts() throws IOException, BrokenBarrierException, InterruptedException {
        long id = mainConnection.getTimeLineReq(lastPostId);
        cyclicBarrier.await();
        ArrayList<Post> postArrayList = responseHandler.handleGetTimeLineResponse(id);


        if ((mainUser.getNumberOfPost() != 0) && (postArrayList.size() == 0)) {
            //showDialog(mainStackPane, Messages.PROBLEM);
        } else {
            if (postArrayList.size() > 0) {
                lastPostId = postArrayList.get(postArrayList.size() - 1).getPostID();
            } else {
                lastPostId = null;
            }

            /*if (lastPostId != null) {
                Collections.reverse(postArrayList);
            }*/

            for (Post p : postArrayList) {
                vBoxMainPage.getChildren().add(loadPostPageFXML(p));
            }
        }
    }


    private void accessScrollBar(ScrollPane scrollPane) {
        for (Node node : scrollPane.lookupAll(".scroll-bar:vertical")) {
            if (node instanceof ScrollBar scrollBar) {
                scrollBar.valueProperty().addListener((observable, oldValue, newValue) -> {
                    if ((Double) newValue == 1.0) {
                        System.out.println("more post");
                        try {
                            sendRequestToGetPosts();
                        } catch (IOException | BrokenBarrierException | InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            lastPostId = null;
            sendRequestToGetPosts();
        } catch (IOException | InterruptedException | BrokenBarrierException e) {
            e.printStackTrace();
        }


        if (timeLineScrollPane.getSkin() == null) {
            ChangeListener<Skin<?>> skinChangeListener = new ChangeListener<Skin<?>>() {
                @Override
                public void changed(ObservableValue<? extends Skin<?>> observable, Skin<?> oldValue, Skin<?> newValue) {
                    timeLineScrollPane.skinProperty().removeListener(this);
                    accessScrollBar(timeLineScrollPane);
                }
            };
            timeLineScrollPane.skinProperty().addListener(skinChangeListener);
        } else {
            accessScrollBar(timeLineScrollPane);
        }


        timeLineScrollPane.vbarPolicyProperty().setValue(ScrollPane.ScrollBarPolicy.NEVER);
        mainPane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                timeLineScrollPane.vbarPolicyProperty().setValue(ScrollPane.ScrollBarPolicy.AS_NEEDED);
            }
        });

        mainPane.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                timeLineScrollPane.vbarPolicyProperty().setValue(ScrollPane.ScrollBarPolicy.NEVER);
            }
        });
    }
}
