package Controller;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import models.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.BrokenBarrierException;

import static Controller.LoginCtrl.mainUser;

public class LoadingFXMLs {
    private static Parent root;
    private static FXMLLoader loader;

    public static Parent loadHomePageFXML() throws IOException {
        root = StartLoader.loadFxml("/homePage.fxml").load();

        return root;
    }

    public static Parent loadLoginPageFXML() throws IOException {
        root = StartLoader.loadFxml("/login.fxml").load();

        return root;
    }

    public static Parent loadSignUpPageFXML() throws IOException {
        root = StartLoader.loadFxml("/signUp.fxml").load();

        return root;
    }


    public static Parent loadProfilePageFXML(MainPageCtrl mainPageCtrl) throws IOException {
        loader = StartLoader.loadFxml("/profilePage.fxml");
        root = loader.load();
        ProfilePageCtrl profilePageCtrl = loader.getController();
        profilePageCtrl.setClass(mainPageCtrl);
        return root;
    }

    public static Parent loadOtherProfilePageFXML(User user) throws IOException {
        loader = StartLoader.loadFxml("/otherUserProfile.fxml");
        root = loader.load();
        OtherUserProfileCtrl otherUserProfileCtrl = loader.getController();
        otherUserProfileCtrl.setDataForPage(user);
        return root;
    }

    public static Parent loadCommentPageFXML(Post post) throws IOException, BrokenBarrierException, InterruptedException {
        loader = StartLoader.loadFxml("/commentPage.fxml");
        root = loader.load();
        CommentPageCtrl commentPageCtrl = loader.getController();
        commentPageCtrl.setPostForPage(post);
        commentPageCtrl.sendRequest();
        return root;
    }

    public static Parent loadCommentBoxPageFXML(Comment comment, TextField textField) throws IOException {
        loader = StartLoader.loadFxml("/commentBox.fxml");
        root = loader.load();
        CommentBoxCtrl commentBoxCtrl = loader.getController();
        commentBoxCtrl.setComments(comment);

        return root;
    }

    public static Parent loadPostPageFXML(Post post) throws IOException {
        loader = StartLoader.loadFxml("/postPage.fxml");
        root = loader.load();
        PostPageCtrl postPageCtrl = loader.getController();
        postPageCtrl.setDataForOtherUser(post);
        return root;
    }

    public static Parent loadEmptyPageFXML(ArrayList arrayList, String userID) throws IOException {
        loader = StartLoader.loadFxml("/emptyPage.fxml");
        root = loader.load();
        EmptyPageCtrl emptyPageCtrl = loader.getController();
        emptyPageCtrl.setID(userID);

        if (arrayList.isEmpty()) {
            return root;
        }


        if (arrayList.get(0) == null) {
            return root;
        } else if (arrayList.get(0) instanceof Like) {
            System.out.println("instanceof Like");
            emptyPageCtrl.setDataForLikers(arrayList);
        } else if (arrayList.get(0) instanceof User) {
            System.out.println("instanceof user");
            emptyPageCtrl.setDataForFwrFwg(arrayList);
        } else if (arrayList.get(0) instanceof DirectMessage) {
            System.out.println("instanceof directMessage");
            emptyPageCtrl.setDataForDirects(arrayList);
        }

        return root;
    }


    public static Parent loadUserBoxPageFXML(Pane pane, Object obj) throws IOException {
        loader = StartLoader.loadFxml("/userBox.fxml");
        root = loader.load();
        UserBoxCtrl userBoxCtrl = loader.getController();
        userBoxCtrl.setPaneForClass(pane);

        if (obj instanceof Like) {
            Like like = ((Like) obj);
            if (like.getUserID().equals(mainUser.getUserID())) {
                userBoxCtrl.setDataForBox(mainUser.getUserID(), mainUser.getUserID(), null, mainUser.getAx());
            } else {
                userBoxCtrl.setDataForBox(like.getUserID(), like.getName(), like.isFollow(), like.getProfileImage());
            }
        } else if (obj instanceof User) {
            User user = ((User) obj);
            userBoxCtrl.setDataForBox(user.getUserID(), user.getUserName(), user.isFollowed(), user.getAx());
        }
        return root;
    }

    public static Parent loadActivityPageFXML() throws IOException, BrokenBarrierException, InterruptedException {
        loader = StartLoader.loadFxml("/activity.fxml");
        root = loader.load();
        ActivityPageCtrl activityPageCtrl = loader.getController();
        activityPageCtrl.sendRequest();

        return root;
    }

    public static Parent loadActivityBoxPageFXML(Activity activity) throws IOException {
        loader = StartLoader.loadFxml("/activityBox.fxml");
        root = loader.load();
        ActivityBoxCtrl activityBoxCtrl = loader.getController();
        activityBoxCtrl.setData(activity);

        return root;
    }

    public static Parent loadChatPageFXML(DirectMessage directMessage) throws IOException {
        loader = StartLoader.loadFxml("/chatPage.fxml");
        root = loader.load();
        ChatPageCtrl chatPageCtrl = loader.getController();
        chatPageCtrl.setData(directMessage);

        return root;
    }

        /*public static Parent loadSendDirectPageFXML() throws IOException, BrokenBarrierException, InterruptedException {
        loader = StartLoader.loadFxml("/sendDirectPage.fxml");
        root = loader.load();
        SendDirectPageCtrl sendDirectPageCtrl = loader.getController();
        sendDirectPageCtrl.sendRequest();
        return root;
    }*/
}
