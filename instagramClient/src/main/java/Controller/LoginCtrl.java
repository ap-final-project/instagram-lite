package Controller;

import API.Messages;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import models.User;
import org.apache.commons.io.FileUtils;
import socket.Connection;
import socket.ResponseHandler;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

import static Controller.StartLoader.loadFxml;

public class LoginCtrl {
    public static ResponseHandler responseHandler;
    public static CyclicBarrier cyclicBarrier;
    public static Connection mainConnection;
    public static User mainUser;

    public static void makeInstance() throws IOException {
        cyclicBarrier = new CyclicBarrier(2);
        responseHandler = new ResponseHandler();
        mainConnection = new Connection();
    }

    @FXML
    public Pane pane;

    @FXML
    private Label errorLbl;

    @FXML
    private TextField IDField;

    @FXML
    private PasswordField passwordField;


    @FXML
    private void loginBtnHandler(ActionEvent event) throws IOException, InterruptedException, BrokenBarrierException {
        errorLbl.setText("");
        makeInstance();

        if (checkFields()) {
            long reqId = mainConnection.loginReq(IDField.getText(), passwordField.getText());
            cyclicBarrier.await();
            Object obj = responseHandler.handleLoginResponse(reqId);

            if (obj instanceof User) {
                mainUser = (User) obj;
                Parent root = loadFxml("/instagram.fxml").load();
                pane.getChildren().setAll(root);
            } else if (obj instanceof String) {
                errorLbl.setText(String.valueOf(obj));
            }
        }
        passwordField.setText("");
    }


    private boolean checkFields() {
        if (IDField.getText().trim().equals("") || passwordField.getText().trim().equals("")) {
            errorLbl.setText(Messages.FILL_FIELDS);
            return false;
        }
        return true;
    }

    @FXML
    private void forgotPassHandler(MouseEvent event) throws IOException {
        Parent root = loadFxml("/forgotPass.fxml").load();
        pane.getChildren().setAll(root);
    }

    @FXML
    private void signUpHandler(MouseEvent event) throws IOException {
        Parent root = loadFxml("/signUp.fxml").load();
        pane.getChildren().setAll(root);
    }

    @FXML
    void closeBtnHandler(MouseEvent event) throws IOException {
        LoginCtrl.makeInstance();
        FileUtils.cleanDirectory(new File(mainConnection.dataPath));
        StartLoader.stage.close();
    }

    @FXML
    void minimizeBtnHandler(MouseEvent event) {
        StartLoader.stage.setIconified(true);
    }


}
