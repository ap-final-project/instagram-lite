package Controller;

import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.BrokenBarrierException;

import static Controller.LoadingFXMLs.*;
import static Controller.LoginCtrl.mainConnection;


public class MainPageCtrl implements Initializable {
    public MainPageCtrl mainPageCtrl = this;
    public static StackPane mainStackPane;

    @FXML
    public Pane middlePane;

    @FXML
    public Pane paneBar;

    @FXML
    public Label backLbl;

    @FXML
    public Pane mainPaneInInstagram;

    @FXML
    private StackPane mainStackP;

    @FXML
    private FontAwesomeIconView timeLinePageIcon;

    @FXML
    private FontAwesomeIconView activityPageIcon;

    @FXML
    private FontAwesomeIconView profilePageIcon;

    @FXML
    private FontAwesomeIconView explorePageIcon;


    @FXML
    private void activityBtnHandler(MouseEvent event) throws BrokenBarrierException, InterruptedException, IOException {
        timeLinePageIcon.setStrokeWidth(1);
        activityPageIcon.setStrokeWidth(2);
        profilePageIcon.setStrokeWidth(1);
        explorePageIcon.setStrokeWidth(5);
        middlePane.getChildren().setAll(loadActivityPageFXML());
    }

    @FXML
    private void searchBtnHandler(MouseEvent event) throws IOException {
        FXMLLoader loader = StartLoader.loadFxml("/emptyPage.fxml");
        Parent root = loader.load();
        EmptyPageCtrl emptyPageCtrl = loader.getController();
        emptyPageCtrl.setState("ExplorePage");
        middlePane.getChildren().setAll(root);

        timeLinePageIcon.setStrokeWidth(1);
        activityPageIcon.setStrokeWidth(1);
        profilePageIcon.setStrokeWidth(1);
        explorePageIcon.setStrokeWidth(4);
    }

    @FXML
    private void homeBtnHandler(MouseEvent event) throws IOException {
        timeLinePageIcon.setStrokeWidth(2);
        activityPageIcon.setStrokeWidth(1);
        profilePageIcon.setStrokeWidth(1);
        explorePageIcon.setStrokeWidth(5);
        middlePane.getChildren().setAll(loadHomePageFXML());
    }


    @FXML
    public void profileBtnHandler(MouseEvent event) throws IOException {
        timeLinePageIcon.setStrokeWidth(1);
        activityPageIcon.setStrokeWidth(1);
        profilePageIcon.setStrokeWidth(2);
        explorePageIcon.setStrokeWidth(5);
        middlePane.getChildren().setAll(loadProfilePageFXML(mainPageCtrl));
    }

    @FXML
    private void newPostBtnHandler(MouseEvent event) throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.JPEG"));
        File selectedFile = fileChooser.showOpenDialog(StartLoader.stage);


        if (selectedFile != null) {
            FXMLLoader fxmlLoader = StartLoader.loadFxml("/newPost.fxml");
            Parent root = fxmlLoader.load();
            NewPostCtrl newPostCtrl = fxmlLoader.getController();
            newPostCtrl.setDataForPage(selectedFile);
            newPostCtrl.setFileName(selectedFile.getName());
            try {
                FileUtils.copyFileToDirectory(new File(selectedFile.getPath())
                        , new File(LoginCtrl.mainConnection.dataPath + "Temp"));

            } catch (IOException ioException) {
                ioException.printStackTrace();
            }

            middlePane.getChildren().setAll(root);
        }
    }

    public static void showDialog(StackPane pane, String msg) {
        JFXDialogLayout layout = new JFXDialogLayout();
        Text text = new Text(msg);
        text.setFont(Font.font(14));
        layout.setBody(text);
        layout.setStyle("-fx-background-color: #94ACD9");

        JFXDialog dialog = new JFXDialog(pane, layout, JFXDialog.DialogTransition.CENTER);

        dialog.show();
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        backLbl.setVisible(false);
        try {
            middlePane.getChildren().setAll(loadProfilePageFXML(mainPageCtrl));
        } catch (IOException e) {
            e.printStackTrace();
        }

        mainStackPane = mainStackP;

    }

    @FXML
    private void minimizeBtnHandler(MouseEvent event) {
        StartLoader.stage.setIconified(true);
    }

    @FXML
    public void closeBtnHandler(MouseEvent event) throws IOException {
        FileUtils.cleanDirectory(new File(mainConnection.dataPath));
        StartLoader.stage.close();
    }


}
