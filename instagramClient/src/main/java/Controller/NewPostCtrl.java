package Controller;

import API.Messages;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import models.Post;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.BrokenBarrierException;

import static Controller.LoginCtrl.cyclicBarrier;
import static Controller.MainPageCtrl.mainStackPane;
import static Controller.MainPageCtrl.showDialog;
import static Controller.ProfilePageCtrl.sendRequest;


public class NewPostCtrl implements Initializable {
    private String fileName;

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @FXML
    private ImageView imageViewPost;

    @FXML
    private TextArea captionTxt;

    @FXML
    private Pane newPostPane;

    @FXML
    private StackPane stackPane;

    @FXML
    private void addPostAction(MouseEvent event) throws IOException, BrokenBarrierException, InterruptedException {
        /*String regex = "@[a-z0-9-_]+\\.*[a-z0-9-_]+";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(captionTxt.getText());
        //todo: check this, using find()
        int startIndex = matcher.start();
        int endIndex = matcher.end();
        if (matcher.find()) {
            //todo:send req for mention
        }*/

        Post post = new Post(captionTxt.getText(), LoginCtrl.mainUser.getUserID());
        post.setMedia(fileName);

        long id = LoginCtrl.mainConnection.addPostReq(post.getMedia(), post.getCaption());
        cyclicBarrier.await();
        Object result = LoginCtrl.responseHandler.handleAddPostResponse(id);

        if (result instanceof String) {
            post.setPostID(String.valueOf(result));
            Parent root = StartLoader.loadFxml("/homePage.fxml").load();
            newPostPane.getChildren().setAll(root);
            sendRequest();
        } else {
            showDialog(mainStackPane, Messages.PROBLEM);
        }

    }

    @FXML
    private void undoAction(MouseEvent event) throws IOException {
        FXMLLoader loader = StartLoader.loadFxml("/homePage.fxml");
        Parent root = loader.load();
        newPostPane.getChildren().setAll(root);
    }

    public void setDataForPage(File file) {
        imageViewPost.setImage(new Image(file.toURI().toString()));
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        captionTxt.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable,
                                String oldValue, String newValue) {

                if (newValue == null) {
                    return;
                }
                if (newValue.length() > 200) {
                    captionTxt.setText(oldValue);
                } else {
                    captionTxt.setText(newValue);
                }
            }
        });

    }
}
