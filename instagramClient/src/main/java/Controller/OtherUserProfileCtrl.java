package Controller;

import API.Messages;
import com.jfoenix.controls.JFXButton;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import models.DirectMessage;
import models.Post;
import models.User;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.BrokenBarrierException;

import static Controller.LoadingFXMLs.loadChatPageFXML;
import static Controller.LoginCtrl.*;
import static Controller.MainPageCtrl.mainStackPane;
import static Controller.MainPageCtrl.showDialog;
import static Controller.ProfilePageCtrl.mainPageCtrl;

public class OtherUserProfileCtrl implements Initializable {
    private User userPage;
    private String lastFollowerID;
    private String lastFollowingID;
    private PostPageCtrl postPageCtrl;
    private FontAwesomeIconView avatar;
    private final String FOLLOW = "Follow";
    private final String UNFOLLOW = "UnFollow";

    @FXML
    private Label idLabel;

    @FXML
    private Pane paneOtherProfPage;

    @FXML
    private Circle profCircle;

    @FXML
    private Label username;

    @FXML
    private Label bio;

    @FXML
    private JFXButton followUnfollowBtn;

    @FXML
    private Label numberOfFollowing;

    @FXML
    private Label numberOfFollowers;

    @FXML
    private Label numberOfPosts;

    @FXML
    private GridPane gridPane;

    public void setDataForPage(User user) throws IOException {
        //set info
        this.userPage = user;
        bio.setText(userPage.getBio());
        username.setText(userPage.getUserName());
        idLabel.setText(userPage.getUserID());
        numberOfPosts.setText(String.valueOf(userPage.getNumberOfPost()));
        numberOfFollowers.setText(String.valueOf(userPage.getNumberOfFollower()));
        numberOfFollowing.setText(String.valueOf(userPage.getNumberOfFollowing()));

        if (!user.isFollowed()) {
            followStyle();
        } else {
            unfollowStyle();
        }

        //set image
        Image image = userPage.getAx();
        if (image != null) {
            profCircle.setVisible(true);
            avatar.setVisible(false);
            profCircle.setFill(new ImagePattern(image));
        } else {
            profCircle.setVisible(false);
            avatar.setVisible(true);
            avatar.setGlyphName("USER_CIRCLE");
            avatar.setSize("120");
            avatar.setX(20);
            avatar.setY(160);
            avatar.setFill(Color.web("#a8a5a5"));
        }

        //set posts
        if (mainUser.getNumberOfPost() > 0) {
            setPosts();
        }
    }

    private void setPosts() throws IOException {
        List<Post> arrayList = userPage.getPosts();

        gridPane.getChildren().removeAll(gridPane.getChildren());

        int row = 1;
        int column = 0;
        double size = 166.667;

        for (Post post : arrayList) {
            ImageView imageView = new ImageView(post.getPostImage());
            imageView.setFitHeight(size);
            imageView.setFitWidth(size);

            imageView.setCursor(Cursor.HAND);

            if (column == 3) {
                column = 0;
                ++row;
            }

            gridPane.add(imageView, column++, row);

            if (mainUser.getNumberOfPost() < 3) {
                gridPane.setPrefWidth(mainUser.getNumberOfPost() * size);
                gridPane.setMaxWidth(Region.USE_PREF_SIZE);
                gridPane.setMinWidth(Region.USE_PREF_SIZE);
            } else {
                gridPane.setMinWidth(Region.USE_PREF_SIZE);
                gridPane.setPrefWidth(500);
                gridPane.setMaxWidth(Region.USE_PREF_SIZE);

            }

            gridPane.setMinHeight(Region.USE_COMPUTED_SIZE);
            gridPane.setPrefHeight(Region.USE_COMPUTED_SIZE);
            gridPane.setMaxHeight(Region.USE_PREF_SIZE);


            imageView.setOnMouseClicked(e -> {
                try {
                    FXMLLoader loader = StartLoader.loadFxml("/postPage.fxml");
                    Parent root = loader.load();
                    postPageCtrl = loader.getController();

                    setUndoInPostPage();

                    postPageCtrl.setDataForMainUser(post);
                    paneOtherProfPage.getChildren().setAll(root);
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            });
        }
    }

    private void setUndoInPostPage() throws IOException {
        mainPageCtrl.backLbl.setVisible(true);
        mainPageCtrl.backLbl.getStyleClass().add("back-btn");

        mainPageCtrl.backLbl.setOnMouseClicked(event -> {
            postPageCtrl.undoAction();
            mainPageCtrl.backLbl.setVisible(false);
        });
    }

    @FXML
    private void followAction(ActionEvent event) throws BrokenBarrierException, InterruptedException {
        if (followUnfollowBtn.getText().equals(FOLLOW)) {
            long id = mainConnection.followReq(idLabel.getText());
            cyclicBarrier.await();
            boolean result = responseHandler.handleFollowResponse(id);

            if (result)
                followStyle();

        } else {

            long id = mainConnection.unFollowReq(idLabel.getText());
            cyclicBarrier.await();
            boolean result = responseHandler.handleUnfollowResponse(id);

            if (result)
                unfollowStyle();

        }
    }

    private void followStyle() {
        followUnfollowBtn.setText(FOLLOW);
        followUnfollowBtn.setStyle("-fx-background-color: #0095F6");
    }

    private void unfollowStyle() {
        followUnfollowBtn.setText(UNFOLLOW);
        followUnfollowBtn.setStyle("-fx-background-color: #A4C9EBFF");
    }

    @FXML
    private void followersBtnHandler(MouseEvent event) throws IOException, BrokenBarrierException, InterruptedException {
        long id = mainConnection.getFollowersListReq(userPage.getUserID(), lastFollowerID);
        cyclicBarrier.await();
        ArrayList<User> arrayList = responseHandler.handleGetFollowersListResponse(id);

        if (lastFollowerID != null) {
            Collections.reverse(arrayList);
        }

        if ((userPage.getNumberOfFollower() != 0) && (arrayList.size() == 0)) {
            showDialog(mainStackPane, Messages.PROBLEM);
        } else {
            if (arrayList.size() > 0) {
                lastFollowerID = arrayList.get(arrayList.size() - 1).getUserID();
            } else {
                lastFollowerID = null;
            }

            FXMLLoader loader = StartLoader.loadFxml("/emptyPage.fxml");
            Parent parent = loader.load();
            EmptyPageCtrl emptyPageCtrl = loader.getController();
            emptyPageCtrl.setDataForFwrFwg(arrayList);

            paneOtherProfPage.getChildren().setAll(parent);
        }
    }

    @FXML
    private void followingBtnHandler(MouseEvent event) throws BrokenBarrierException, InterruptedException, IOException {
        long id = mainConnection.getFollowingListReq(userPage.getUserID(), lastFollowingID);
        cyclicBarrier.await();
        ArrayList<User> arrayList = responseHandler.handleGetFollowingListResponse(id);

        if (lastFollowingID != null) {
            Collections.reverse(arrayList);
        }

        if ((userPage.getNumberOfFollowing() != 0) && (arrayList.size() == 0)) {
            showDialog(mainStackPane, Messages.PROBLEM);
        } else {
            if (arrayList.size() > 0) {
                lastFollowingID = arrayList.get(arrayList.size() - 1).getUserID();
            } else {
                lastFollowingID = null;
            }

            FXMLLoader loader = StartLoader.loadFxml("/emptyPage.fxml");
            Parent parent = loader.load();
            EmptyPageCtrl emptyPageCtrl = loader.getController();
            emptyPageCtrl.setDataForFwrFwg(arrayList);

            paneOtherProfPage.getChildren().setAll(parent);
        }
    }

    @FXML
    private void message(ActionEvent event) throws IOException {
        DirectMessage directMessage = new DirectMessage(idLabel.getText());
        //todo: ax
        paneOtherProfPage.getChildren().setAll(loadChatPageFXML(directMessage));

    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        avatar = new FontAwesomeIconView();
        paneOtherProfPage.getChildren().add(avatar);
    }
}
