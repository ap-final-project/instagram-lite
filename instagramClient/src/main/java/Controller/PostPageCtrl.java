package Controller;

import API.Messages;
import API.Values;
import Configuration.Config;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.effect.BlendMode;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import models.Like;
import models.Post;
import models.User;

import java.io.IOException;
import java.net.URL;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.concurrent.BrokenBarrierException;

import static Controller.LoadingFXMLs.*;
import static Controller.LoginCtrl.*;
import static Controller.MainPageCtrl.mainStackPane;
import static Controller.MainPageCtrl.showDialog;
import static Controller.ProfilePageCtrl.mainPageCtrl;
import static Controller.ProfilePageCtrl.sendRequest;


public class PostPageCtrl implements Initializable {
    private Post post;
    private String state;
    private String lastUserID;
    private final String PROFILE_PAGE = "ProfilePage";
    private final String TIMELINE_PAGE = "TimeLinePage";

    @FXML
    private Pane panePostPage;

    @FXML
    private Circle imgCircle;

    @FXML
    private Label IDHead;

    @FXML
    private MenuBar menuBar;

    @FXML
    private Menu menu;

    @FXML
    private MenuItem menuItem;

    @FXML
    private Label likesInPost;

    @FXML
    private Label IDLblInPost;

    @FXML
    private Label caption;

    @FXML
    private Label dateOfPost;

    @FXML
    private VBox vBoxStyle;

    @FXML
    private ImageView postImageView;

    @FXML
    private FontAwesomeIconView FALike;

    @FXML
    private FontAwesomeIconView moreIcon;

    @FXML
    private FontAwesomeIconView FABookMark;


    @FXML
    private void savedMsgAction(MouseEvent event) {

    }

    @FXML
    private void sendToDirect(MouseEvent event) {
        /*Stage cmStage = new Stage();
        cmStage.setResizable(Config.getInstance().getBooleanValue(Values.RESIZE_CONFIG));

        Scene scene = new Scene(loadSendDirectPageFXML());
        cmStage.setScene(scene);
        cmStage.show();*/
    }

    @FXML
    private void commentAction(MouseEvent event) throws IOException, BrokenBarrierException, InterruptedException {
        Stage cmStage = new Stage();
        cmStage.setResizable(Config.getInstance().getBooleanValue(Values.RESIZE_CONFIG));

        FXMLLoader loader = StartLoader.loadFxml("/commentPage.fxml");
        Parent root = loader.load();
        CommentPageCtrl commentPageCtrl = loader.getController();
        commentPageCtrl.setPostForPage(post);
        commentPageCtrl.sendRequest();

        cmStage.setScene(new Scene(root));
        cmStage.show();
    }


    public void setDataForOtherUser(Post p) throws IOException {
        this.post = p;
        state = TIMELINE_PAGE;
        setStyleForMainPane(state);

        IDLblInPost.setText(post.getUserID());
        IDHead.setText(post.getUserID());

        likesInPost.setText(NumberFormat
                .getNumberInstance(Locale.US)
                .format(post.getLikes())
                + "  Likes");

        if (post.getProfilePic() == null) {
            imgCircle.setVisible(false);
            FontAwesomeIconView icon = new FontAwesomeIconView();
            icon.setGlyphName("USER_CIRCLE");
            icon.setSize("42");
            icon.setX(20);
            icon.setY(40);
            icon.setFill(Color.web("#a8a5a5"));
            panePostPage.getChildren().add(icon);
        } else {
            imgCircle.setFill(new ImagePattern(post.getProfileImage()));
        }

        postImageView.setImage(post.getPostImage());

        dateOfPost.setText(post.getDate());

        caption.setText(post.getCaption());

        moreIcon.setVisible(false);
        menuBar.setVisible(false);

        if (post.isLiked()) {
            likeStyle();
        } else {
            unLikeStyle();
        }
    }

    public void setDataForMainUser(Post p) throws IOException {
        this.post = p;
        state = PROFILE_PAGE;
        setStyleForMainPane(state);

        IDLblInPost.setText(mainUser.getUserID());
        IDHead.setText(mainUser.getUserID());

        likesInPost.setText(NumberFormat
                .getNumberInstance(Locale.US)
                .format(post.getLikes())
                + "  Likes");

        if (mainUser.getAx() == null) {
            imgCircle.setVisible(false);
            FontAwesomeIconView icon = new FontAwesomeIconView();
            icon.setGlyphName("USER_CIRCLE");
            icon.setSize("42");
            icon.setX(20);
            icon.setY(40);
            icon.setFill(Color.web("#a8a5a5"));
            panePostPage.getChildren().add(icon);
        } else {
            imgCircle.setFill(new ImagePattern(mainUser.getAx()));
        }

        postImageView.setImage(post.getPostImage());

        dateOfPost.setText(post.getDate());

        caption.setText(post.getCaption());

        if (post.isLiked()) {
            likeStyle();
        } else {
            unLikeStyle();
        }
    }

    public void undoAction() {
        if (state.equals(PROFILE_PAGE)) {
            state = "";
            try {
                FXMLLoader l = StartLoader.loadFxml("/profilePage.fxml");
                Parent root = l.load();
                panePostPage.getChildren().setAll(root);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (state.equals(TIMELINE_PAGE)) {
            state = "";
            try {
                FXMLLoader l = StartLoader.loadFxml("/homePage.fxml");
                Parent root = l.load();
                panePostPage.getChildren().setAll(root);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private void updateLikes() {
        likesInPost.setText(NumberFormat
                .getNumberInstance(Locale.US)
                .format(post.getLikes())
                + "  Likes");
    }

    private void likeStyle() {
        FALike.setFill(Color.RED);
        FALike.setStroke(Color.RED);
        FALike.setStrokeWidth(1);
    }

    private void unLikeStyle() {
        FALike.setFill(Color.WHITE);
        FALike.setStroke(Color.BLACK);
        FALike.setStrokeWidth(1);
    }

    private void setStyleForMainPane(String state) {
        if (state.equals(TIMELINE_PAGE)) {
            panePostPage.setPrefHeight(Region.USE_COMPUTED_SIZE);
            panePostPage.minHeight(Region.USE_COMPUTED_SIZE);
            panePostPage.maxHeight(Region.USE_COMPUTED_SIZE);

            vBoxStyle.setPrefHeight(Region.USE_COMPUTED_SIZE);
            vBoxStyle.minHeight(Region.USE_COMPUTED_SIZE);
            vBoxStyle.maxHeight(Region.USE_COMPUTED_SIZE);
        } else if (state.equals(PROFILE_PAGE)) {
            panePostPage.setPrefHeight(620);
            panePostPage.minHeight(Region.USE_PREF_SIZE);
            panePostPage.maxHeight(Region.USE_PREF_SIZE);

            vBoxStyle.setPrefHeight(620);
            vBoxStyle.minHeight(Region.USE_PREF_SIZE);
            vBoxStyle.maxHeight(Region.USE_PREF_SIZE);
        }
    }

    @FXML
    private void deletePostAction(ActionEvent event) {
        try {
            long id = mainConnection.deletePostReq(post.getPostID());
            cyclicBarrier.await();
            boolean result = responseHandler.handleDeletePostResponse(id);

            if (result) {
                sendRequest();
                Parent parent = StartLoader.loadFxml("/profilePage.fxml").load();
                panePostPage.getChildren().setAll(parent);
            } else {
                showDialog(mainStackPane, Messages.PROBLEM);
            }
        } catch (InterruptedException | BrokenBarrierException | IOException interruptedException) {
            interruptedException.printStackTrace();
        }
    }

    @FXML
    private void likeAction(MouseEvent event) throws BrokenBarrierException, InterruptedException {
        if (FALike.getFill().equals(Color.WHITE)) {
            long id = mainConnection.likePostReq(post.getUserID(), post.getPostID());
            cyclicBarrier.await();
            boolean result = responseHandler.handleLikeResponse(id);

            if (result) {
                likeStyle();
                post.setLikes(post.getLikes() + 1);
                sendRequest();
                updateLikes();
            } else {
                showDialog(mainStackPane, Messages.PROBLEM);
            }

        } else {
            long id = mainConnection.unLikePostReq(post.getUserID(), post.getPostID());
            cyclicBarrier.await();
            boolean result = responseHandler.handleUnLikeResponse(id);

            if (result) {
                unLikeStyle();
                post.setLikes(post.getLikes() - 1);
                sendRequest();
                updateLikes();
            } else {
                showDialog(mainStackPane, Messages.PROBLEM);
            }
        }

    }

    @FXML
    private void showLikers(MouseEvent event) throws BrokenBarrierException, InterruptedException, IOException {
        long id = mainConnection.getLikesListReq(post.getPostID(), lastUserID);
        cyclicBarrier.await();
        ArrayList<Like> arrayList = responseHandler.handleGetLikesListResponse(id);
        post.setLikesArray(arrayList);

        Parent parent = null;
        if ((post.getLikes() != 0) && (arrayList.size() == 0)) {
            showDialog(mainStackPane, Messages.PROBLEM);
        } else {
            if (arrayList.size() > 0) {
                lastUserID = arrayList.get(arrayList.size() - 1).getUserID();
            } else {
                lastUserID = null;
            }

            if (lastUserID != null) {
                Collections.reverse(arrayList);
            }

            parent = loadEmptyPageFXML(arrayList, post.getUserID());
        }


        if (state.equals(TIMELINE_PAGE)) {
            mainPageCtrl.middlePane.getChildren().setAll(parent);
            mainPageCtrl.backLbl.setVisible(true);
            mainPageCtrl.backLbl.setOnMouseClicked(e -> {
                try {
                    mainPageCtrl.middlePane.getChildren().setAll(loadHomePageFXML());
                    mainPageCtrl.backLbl.setVisible(false);
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            });
        } else if (state.equals(PROFILE_PAGE)) {
            panePostPage.getChildren().setAll(parent);
        }
    }

    @FXML
    private void goToProfilePage(MouseEvent event) throws IOException, BrokenBarrierException, InterruptedException {
        Parent parent = null;
        if (IDLblInPost.getText().equals(mainUser.getUserID())) {
            parent = StartLoader.loadFxml("/profilePage.fxml").load();
        } else {
            long id = mainConnection.getUserProfileReq(IDLblInPost.getText());
            cyclicBarrier.await();
            User user = responseHandler.handleGetUserProfileResponse(id);

            if (user != null) {

                parent = loadOtherProfilePageFXML(user);
            }
        }

        if (state.equals(TIMELINE_PAGE)) {
            mainPageCtrl.middlePane.getChildren().setAll(parent);
            mainPageCtrl.backLbl.setVisible(true);
            mainPageCtrl.backLbl.getStyleClass().add("back-btn");
            mainPageCtrl.backLbl.setOnMouseClicked(e -> {
                try {
                    mainPageCtrl.middlePane.getChildren().setAll(loadHomePageFXML());
                    mainPageCtrl.backLbl.setVisible(false);
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            });
        } else {
            panePostPage.getChildren().setAll(parent);
            mainPageCtrl.backLbl.setVisible(false);
        }

    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        menuBar.setBlendMode(BlendMode.DARKEN);
        menu.setStyle("-fx-background-color: transparent");
        menu.setGraphic(moreIcon);
    }
}
