package Controller;

import API.Messages;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.stage.FileChooser;
import models.Post;
import models.User;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.BrokenBarrierException;

import static Controller.LoadingFXMLs.loadLoginPageFXML;
import static Controller.LoginCtrl.*;
import static Controller.MainPageCtrl.mainStackPane;
import static Controller.MainPageCtrl.showDialog;


public class ProfilePageCtrl implements Initializable {
    private String lastFollowerID;
    private String lastFollowingID;
    private PostPageCtrl postPageCtrl;
    private FontAwesomeIconView avatar;
    public static MainPageCtrl mainPageCtrl;

    @FXML
    private Pane mainPane;

    @FXML
    private StackPane stackPane;

    @FXML
    private Pane paneProfPage;

    @FXML
    private Label IDLabel;

    @FXML
    private Circle profCircle;

    @FXML
    private Label username;

    @FXML
    private Label bio;

    @FXML
    private Label numberOfFollowing;

    @FXML
    private Label numberOfFollowers;

    @FXML
    private Label numberOfPosts;

    @FXML
    private GridPane gridPane;

    @FXML
    private Pane paneEditPage;

    @FXML
    private TextField changeID;

    @FXML
    private TextField changeName;

    @FXML
    private TextField changeEmail;

    @FXML
    private TextArea changeBio;

    @FXML
    private Label errorLblInEditPage;

    @FXML
    private ImageView settingIcon;

    @FXML
    private MenuBar menuBar;

    @FXML
    private Menu menu;

    @FXML
    private MenuItem removeProfileItem;

    @FXML
    private MenuItem newProfileItem;

    @FXML
    private MenuBar menuBarInProfile;

    @FXML
    private Menu menuInProfile;

    @FXML
    private MenuItem logOutItem;

    @FXML
    private MenuItem deleteAccountItem;

    @FXML
    private MenuItem ChangePassItem;

    @FXML
    private Pane paneChangePass;

    @FXML
    private Label errorLabelInChangePass;

    @FXML
    private TextField currentPassField;

    @FXML
    private TextField newPassField;

    @FXML
    private TextField confirmPassField;

    @FXML
    private TextField passFieldForDelete;

    @FXML
    private Pane paneDeleteAccount;

    @FXML
    private Label errorLabelInDeletePage;

    @FXML
    private FontAwesomeIconView changePhotoIcon;


    @FXML
    private void deleteAccountItem(ActionEvent event) throws IOException {
        setUndoInDeletePage();
        errorLabelInDeletePage.setText("");
        paneChangePass.setVisible(false);
        paneProfPage.setVisible(false);
        paneEditPage.setVisible(false);
        paneDeleteAccount.setVisible(true);
    }

    @FXML
    private void ChangePassItemAction(ActionEvent event) throws IOException {
        setUndoInChangePassPage();
        errorLabelInChangePass.setText("");
        paneChangePass.setVisible(true);
        paneProfPage.setVisible(false);
        paneEditPage.setVisible(false);
        paneDeleteAccount.setVisible(false);
    }

    @FXML
    private void deleteAccountAction(ActionEvent event) throws BrokenBarrierException, InterruptedException, IOException {
        if (passFieldForDelete.getText().length() == 0) {
            errorLabelInChangePass.setText(Messages.FILL_FIELDS);
        } else {
            long reqId = mainConnection.deleteAccountReq(passFieldForDelete.getText());
            cyclicBarrier.await();
            boolean result = responseHandler.handleDeleteAccountResponse(reqId);
            if (result) {
                System.out.println(Messages.DELETE_ACCOUNT_SUCCESSFUL);
                showDialog(mainStackPane, Messages.DELETE_ACCOUNT_SUCCESSFUL);
                mainPageCtrl.mainPaneInInstagram.getChildren().setAll(loadLoginPageFXML());
            }
        }
    }

    @FXML
    private void logoutAction(ActionEvent event) throws BrokenBarrierException, InterruptedException, IOException {
        long reqId = mainConnection.logoutReq();
        cyclicBarrier.await();
        boolean result = responseHandler.handleLogoutResponse(reqId);
        if (result) {
            mainPageCtrl.mainPaneInInstagram.getChildren().add(loadLoginPageFXML());
        }
    }


    @FXML
    private void changePassHandler(ActionEvent event) throws BrokenBarrierException, InterruptedException {
        if (newPassField.getText().length() != 5) {
            errorLabelInChangePass.setText(Messages.SIGNUP_PASS_5_CHAR);
        } else {
            if (confirmPassField.getText().equals(newPassField.getText())) {
                long reqId = mainConnection.changePassReq(currentPassField.getText(), confirmPassField.getText());
                cyclicBarrier.await();
                boolean result = responseHandler.handleChangePassResponse(reqId);

                if (result) {
                    errorLabelInChangePass.setText(Messages.CHANGE_PASS_SUCCESSFUL);
                } else {
                    errorLabelInChangePass.setText(Messages.CHANGE_PASS_INCORRECT);
                }

            } else {
                errorLabelInChangePass.setText(Messages.CHANGE_PASS_MISMATCH);
            }
        }
    }


    public void setClass(MainPageCtrl pageCtrl) {
        mainPageCtrl = pageCtrl;
    }

    private void setDataForProfilePage() throws IOException {
        //set image
        Image image = mainUser.getAx();
        if (image != null) {
            profCircle.setVisible(true);
            avatar.setVisible(false);
            profCircle.setFill(new ImagePattern(image));
        } else {
            profCircle.setVisible(false);
            avatar.setVisible(true);
            avatar.setGlyphName("USER_CIRCLE");
            avatar.setSize("120");
            avatar.setX(20);
            avatar.setY(160);
            avatar.setFill(Color.web("#a8a5a5"));
            removeProfileItem.setVisible(false);
        }

        //set info
        bio.setText(mainUser.getBio());
        username.setText(mainUser.getUserName());
        IDLabel.setText(mainUser.getUserID());
        numberOfPosts.setText(String.valueOf(mainUser.getNumberOfPost()));
        numberOfFollowers.setText(String.valueOf(mainUser.getNumberOfFollower()));
        numberOfFollowing.setText(String.valueOf(mainUser.getNumberOfFollowing()));


        //set posts
        if (mainUser.getNumberOfPost() > 0) {
            setPosts();
        }

    }

    private void setInfoForEditPage() {
        changeBio.setText(mainUser.getBio());
        changeName.setText(mainUser.getUserName());
        changeID.setText(mainUser.getUserID());
        changeEmail.setText(mainUser.getEmail());
        removeProfileItem.setVisible(profCircle.isVisible());
    }

    private void setPosts() throws IOException {
        List<Post> arrayList = mainUser.getPosts();

        //todo: age srcoll kone bere paein ino byd brdarm
        gridPane.getChildren().removeAll(gridPane.getChildren());

        int row = 1;
        int column = 0;
        double size = 166.667;

        for (Post post : arrayList) {
            ImageView imageView = new ImageView(post.getPostImage());
            imageView.setFitHeight(size);
            imageView.setFitWidth(size);

            imageView.setCursor(Cursor.HAND);

            if (column == 3) {
                column = 0;
                ++row;
            }

            gridPane.add(imageView, column++, row);

            if (mainUser.getNumberOfPost() < 3) {
                gridPane.setPrefWidth(mainUser.getNumberOfPost() * size);
                gridPane.setMaxWidth(Region.USE_PREF_SIZE);
                gridPane.setMinWidth(Region.USE_PREF_SIZE);
            } else {
                gridPane.setMinWidth(Region.USE_PREF_SIZE);
                gridPane.setPrefWidth(500);
                gridPane.setMaxWidth(Region.USE_PREF_SIZE);

            }

            gridPane.setMinHeight(Region.USE_COMPUTED_SIZE);
            gridPane.setPrefHeight(Region.USE_COMPUTED_SIZE);
            gridPane.setMaxHeight(Region.USE_PREF_SIZE);


            imageView.setOnMouseClicked(e -> {
                try {
                    FXMLLoader loader = StartLoader.loadFxml("/postPage.fxml");
                    Parent root = loader.load();
                    postPageCtrl = loader.getController();

                    /*if (!inProfilePage) {
                    }*/

                    setUndoInPostPage();

                    postPageCtrl.setDataForMainUser(post);
                    mainPane.getChildren().setAll(root);
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            });
        }
    }

    private void removeImage() {
        profCircle.setVisible(false);
        avatar = new FontAwesomeIconView();
        avatar.setGlyphName("USER_CIRCLE");
        avatar.setSize("120");
        avatar.setX(20);
        avatar.setY(170);
        avatar.setFill(Color.web("#a8a5a5"));
        avatar.setCursor(Cursor.HAND);
        paneProfPage.getChildren().add(avatar);
    }

    @FXML
    private void changeProfileAction(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.JPEG"));
        File selectedFile = fileChooser.showOpenDialog(StartLoader.stage);

        if (selectedFile != null) {
            try {
                FileUtils.copyFileToDirectory(new File(selectedFile.getPath())
                        , new File(mainConnection.dataPath + "Temp"));

                long reqId = mainConnection.editProfilePicReq(selectedFile.getName());
                cyclicBarrier.await();
                boolean result = responseHandler.handleEditProfilePicResponse(reqId);

                if (result) {
                    avatar.setVisible(false);
                    removeProfileItem.setVisible(true);
                    errorLblInEditPage.setText(Messages.EDIT_PROFILE_PIC_SUCCESSFUL);
                    errorLblInEditPage.getStyleClass().add("successful-label");

                    sendRequest();
                } else {
                    showDialog(mainStackPane, Messages.PROBLEM);
                }

            } catch (IOException | InterruptedException | BrokenBarrierException ioException) {
                ioException.printStackTrace();
            }
        }
    }

    @FXML
    private void removeProfileAction(ActionEvent event) {
        try {
            long reqId = mainConnection.removeProfilePicReq();
            cyclicBarrier.await();
            boolean result = responseHandler.handleRemoveProfilePicResponse(reqId);
            if (result) {
                mainUser.setProfilePic(null);
                removeProfileItem.setVisible(false);
                errorLblInEditPage.setText(Messages.REMOVE_PROFILE_PIC_SUCCESSFUL);
                errorLblInEditPage.getStyleClass().add("successful-label");
                removeImage();
                sendRequest();
            }
        } catch (InterruptedException | BrokenBarrierException interruptedException) {
            interruptedException.printStackTrace();
        }
    }

    private void setUndoInEditPage() {
        mainPageCtrl.backLbl.setVisible(true);
        mainPageCtrl.backLbl.getStyleClass().add("back-btn");

        mainPageCtrl.backLbl.setOnMouseClicked(e -> {
            try {
                setDataForProfilePage();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
            paneEditPage.setVisible(false);
            paneProfPage.setVisible(true);
            mainPageCtrl.backLbl.setVisible(false);
        });
    }

    private void setUndoInPostPage() throws IOException {
        mainPageCtrl.backLbl.setVisible(true);
        mainPageCtrl.backLbl.getStyleClass().add("back-btn");

        mainPageCtrl.backLbl.setOnMouseClicked(event -> {
            postPageCtrl.undoAction();
            mainPageCtrl.backLbl.setVisible(false);
            //errorLblInEditPage.setText("");
        });
    }

    private void setUndoInDeletePage() throws IOException {
        mainPageCtrl.backLbl.setVisible(true);
        mainPageCtrl.backLbl.getStyleClass().add("back-btn");

        mainPageCtrl.backLbl.setOnMouseClicked(event -> {
            errorLabelInDeletePage.setText("");
            paneChangePass.setVisible(false);
            paneProfPage.setVisible(true);
            paneEditPage.setVisible(false);
            paneDeleteAccount.setVisible(false);
            mainPageCtrl.backLbl.setVisible(false);
        });
    }

    private void setUndoInChangePassPage() throws IOException {
        mainPageCtrl.backLbl.setVisible(true);
        mainPageCtrl.backLbl.getStyleClass().add("back-btn");

        mainPageCtrl.backLbl.setOnMouseClicked(event -> {
            errorLabelInChangePass.setText("");
            paneChangePass.setVisible(false);
            paneProfPage.setVisible(true);
            paneEditPage.setVisible(false);
            paneDeleteAccount.setVisible(false);
            mainPageCtrl.backLbl.setVisible(false);
        });
    }

    @FXML
    private void followersBtnHandler(MouseEvent event) throws BrokenBarrierException, InterruptedException, IOException {
        long id = mainConnection.getFollowersListReq(mainUser.getUserID(), lastFollowerID);
        cyclicBarrier.await();
        ArrayList<User> arrayList = responseHandler.handleGetFollowersListResponse(id);


        if ((mainUser.getNumberOfFollower() != 0) && (arrayList.size() == 0)) {
            showDialog(mainStackPane, Messages.PROBLEM);
        } else {
            if (arrayList.size() > 0) {
                lastFollowerID = arrayList.get(arrayList.size() - 1).getUserID();
            } else {
                lastFollowerID = null;
            }

            if (lastFollowerID != null) {
                Collections.reverse(arrayList);
            }


            FXMLLoader loader = StartLoader.loadFxml("/emptyPage.fxml");
            Parent parent = loader.load();
            EmptyPageCtrl emptyPageCtrl = loader.getController();
            emptyPageCtrl.setDataForFwrFwg(arrayList);

            paneProfPage.getChildren().setAll(parent);
        }
    }

    @FXML
    private void followingBtnHandler(MouseEvent event) throws BrokenBarrierException, InterruptedException, IOException {
        long id = mainConnection.getFollowingListReq(mainUser.getUserID(), lastFollowingID);
        cyclicBarrier.await();
        ArrayList<User> arrayList = responseHandler.handleGetFollowingListResponse(id);

        if ((mainUser.getNumberOfFollowing() != 0) && (arrayList.size() == 0)) {
            showDialog(mainStackPane, Messages.PROBLEM);
        } else {
            if (arrayList.size() > 0) {
                lastFollowingID = arrayList.get(arrayList.size() - 1).getUserID();
            } else {
                lastFollowingID = null;
            }

            /*if (lastFollowingID != null) {
                Collections.reverse(arrayList);
            }*/

            FXMLLoader loader = StartLoader.loadFxml("/emptyPage.fxml");
            Parent parent = loader.load();
            EmptyPageCtrl emptyPageCtrl = loader.getController();
            emptyPageCtrl.setDataForFwrFwg(arrayList);

            paneProfPage.getChildren().setAll(parent);
        }
    }


    @FXML
    private void editProfileBtn(ActionEvent event) throws IOException {
        errorLblInEditPage.setText("");
        setInfoForEditPage();
        setUndoInEditPage();
        paneEditPage.setVisible(true);
        paneProfPage.setVisible(false);
    }

    private void checkBioField() throws BrokenBarrierException, InterruptedException {
        String oldBio;
        if (mainUser.getBio() == null) {
            oldBio = "";
        } else {
            oldBio = mainUser.getBio();
        }

        if (changeBio.getText() == null) {
            changeBio.setText("");
        }

        if (!oldBio.equals(changeBio.getText()) && !changeBio.getText().trim().equals("")) {
            long reqId = mainConnection.editBioReq(changeBio.getText());
            cyclicBarrier.await();
            boolean result = responseHandler.handleEditBioResponse(reqId);

            if (result) {
                errorLblInEditPage.setText(Messages.EDIT_BIO_SUCCESSFUL);
                errorLblInEditPage.getStyleClass().add("successful-label");
                mainUser.setBio(changeBio.getText());
                sendRequest();
            }
        }

    }

    private void checkEmailField() throws BrokenBarrierException, InterruptedException {
        String oldEmail = mainUser.getEmail();

        if (!oldEmail.equals(changeEmail.getText()) && !changeEmail.getText().trim().equals("")) {
            if (ForgotPassCtrl.isValidEmailAddress(changeEmail.getText())) {
                long reqId = mainConnection.changeEmailReq(changeEmail.getText());

                cyclicBarrier.await();

                boolean result = responseHandler.handleChangeEmailResponse(reqId);

                if (result) {
                    errorLblInEditPage.setText(Messages.CHANGE_EMAIL_SUCCESSFUL);
                    mainUser.setEmail(changeEmail.getText());
                    sendRequest();
                } else {
                    errorLblInEditPage.setText(Messages.CHANGE_EMAIL_UNSUCCESSFUL);
                }

            } else {
                errorLblInEditPage.setText(Messages.CHANGE_EMAIL_INCORRECT);
            }

        }

    }

    private void checkUserNameField() throws BrokenBarrierException, InterruptedException {
        String oldName = mainUser.getUserName();

        if (!oldName.equals(changeName.getText()) && !changeName.getText().equals("\s+")) {
            long reqId = mainConnection.editUserNameReq(changeName.getText());

            cyclicBarrier.await();

            boolean result = responseHandler.handleEditUserNameResponse(reqId);

            if (result) {
                errorLblInEditPage.setText(Messages.EDIT_USERNAME_SUCCESSFUL);
                errorLblInEditPage.getStyleClass().add("successful-label");
                mainUser.setUserName(changeName.getText());
                sendRequest();
            } else {
                errorLblInEditPage.setText(Messages.EDIT_USERNAME_UNSUCCESSFUL);
                errorLblInEditPage.getStyleClass().add("error-label");
            }
        }

    }

    @FXML
    private void saveEditAction(ActionEvent event) throws BrokenBarrierException, InterruptedException {
        errorLblInEditPage.setText("");
        checkUserNameField();
        checkEmailField();
        checkBioField();
    }


    public static void sendRequest() throws BrokenBarrierException, InterruptedException {
        long reqId = mainConnection.getMainUserProfile();

        cyclicBarrier.await();

        User user = responseHandler.handleGetMainUserProfileResponse(reqId);

        if (user != null) {
            mainUser = user;
        } else {
            //mainPageCtrl.showDialog();
        }
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        avatar = new FontAwesomeIconView();
        paneProfPage.getChildren().add(avatar);

        try {
            sendRequest();
            setDataForProfilePage();
        } catch (IOException | InterruptedException | BrokenBarrierException e) {
            e.printStackTrace();
        }


        paneEditPage.setVisible(false);
        paneProfPage.setVisible(true);
        paneChangePass.setVisible(false);
        paneDeleteAccount.setVisible(false);

        menu.setStyle("-fx-background-color: transparent");
        menu.setGraphic(changePhotoIcon);
        menuInProfile.setStyle("-fx-background-color: transparent");
        //menuInProfile.setGraphic(settingIcon);

        changeBio.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(final ObservableValue<? extends String> observable,
                                final String oldValue, final String newValue) {

                if (newValue == null) {
                    return;
                }
                if (newValue.length() > 180) {
                    changeBio.setText(oldValue);
                } else {
                    changeBio.setText(newValue);
                }
            }
        });
    }

}
