package Controller;

import com.jfoenix.controls.JFXButton;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import models.Comment;
import models.DirectMessage;
import models.Like;
import models.Post;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.BrokenBarrierException;

import static Controller.LoginCtrl.*;
import static Controller.LoginCtrl.responseHandler;

public class SendDirectPageCtrl {
    /*private Post post;

    @FXML
    private Pane paneSendDirect;

    @FXML
    private TextField searchField;

    @FXML
    private VBox showUsersVBox;

    public void sendRequest() throws BrokenBarrierException, InterruptedException {
        long id = mainConnection.getChatListReq();
        cyclicBarrier.await();
        ArrayList<DirectMessage> directs = responseHandler.handleGetChatListResponse(id);

        for (DirectMessage directMsg : directs) {
            showUsersVBox.getChildren().add(createPaneForUser(directMsg));
        }

    }

    public Pane createPaneForUser(DirectMessage DM) {
        Pane pane = new Pane();
        pane.setPrefWidth(300);
        pane.setPrefHeight(50);
        pane.setStyle("-fx-background-color: white");

        Label IDLabel = new Label();
        JFXButton button = new JFXButton();
        Circle circleImg = new Circle(22);
        FontAwesomeIconView avatar = new FontAwesomeIconView();

        circleImg.setLayoutX(35);
        circleImg.setLayoutY(25);
        circleImg.setStrokeWidth(0);
        Image image = DM.getProfileImage();
        if (image != null) {
            circleImg.setFill(new ImagePattern(image));
        } else {
            avatar.setGlyphName("USER_CIRCLE");
            avatar.setSize("44");
            avatar.setLayoutX(55);
            avatar.setLayoutY(45);
            avatar.setFill(Color.web("#a8a5a5"));
            pane.getChildren().add(avatar);
        }


        IDLabel.setLayoutX(65);
        IDLabel.setLayoutY(13);
        IDLabel.setPrefWidth(150);
        IDLabel.setPrefHeight(25);
        IDLabel.setStyle("-fx-font-family: Loma");
        IDLabel.setStyle("-fx-font-size: 15");
        IDLabel.setText(DM.getUserID());

        button.setLayoutX(225);
        button.setLayoutY(13);
        button.setPrefWidth(70);
        button.setPrefHeight(25);
        button.setStyle("-fx-border-width:  1px");
        button.setButtonType(JFXButton.ButtonType.RAISED);


        button.setStyle("-fx-background-color: #0095F6");
        button.setText("Send");

        button.setOnAction(e -> {
            button.setStyle("-fx-background-color: #443490");
            button.setDisable(true);

        });


        pane.getChildren().addAll(button, IDLabel, circleImg);
        return pane;
    }


    @FXML
    private void searchAction(ActionEvent event) {
        //todo: badaaaaaan
    }*/
}
