package Controller;

import API.Messages;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

import java.io.IOException;
import java.util.concurrent.BrokenBarrierException;

import static Controller.LoadingFXMLs.loadLoginPageFXML;
import static Controller.LoginCtrl.*;
import static Controller.StartLoader.loadFxml;
import static Controller.StartLoader.stage;


public class SignUpCtrl {
    public static String code;

    @FXML
    public Pane pane;

    @FXML
    private Label errorLbl;

    @FXML
    private TextField IDField;

    @FXML
    private TextField emailField;

    @FXML
    private TextField usernameField;

    @FXML
    private PasswordField passwordField;

    @FXML
    private void backToLogin(MouseEvent event) throws IOException {
        pane.getChildren().setAll(loadLoginPageFXML());
    }

    @FXML
    private void signUpBtnHandler(ActionEvent event) throws IOException, InterruptedException, BrokenBarrierException {
        errorLbl.setText("");
        boolean isCheck = checkFields();
        if (isCheck) {
            makeInstance();

            long reqId = mainConnection.signUpReq(emailField.getText(), usernameField.getText(), IDField.getText(), passwordField.getText());
            cyclicBarrier.await();
            Object result = responseHandler.handleSignupResponse(reqId);

            if (result instanceof String) {
                errorLbl.setText(String.valueOf(result));
            } else if (result instanceof Boolean) {
                FXMLLoader loader = loadFxml("/verifyCode.fxml");
                Parent root = loader.load();
                VerifyCodeCtrl codeCtrl = loader.getController();
                emailField.setStyle("-fx-underline: true");
                codeCtrl.description.setText(codeCtrl.description.getText() + emailField.getText());
                codeCtrl.setEmail(emailField.getText());
                pane.getChildren().setAll(root);
            }
        }
    }

    private boolean checkFields() {
        if (emailField.getText().trim().equals("") || usernameField.getText().trim().equals("") || passwordField.getText().trim().equals("")) {
            errorLbl.setText(Messages.FILL_FIELDS);
            return false;
        } else if (!ForgotPassCtrl.isValidEmailAddress(emailField.getText())) {
            errorLbl.setText(Messages.CHANGE_EMAIL_INCORRECT);
            return false;
        }

        passwordField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable,
                                String oldValue, String newValue) {

                if (newValue == null) {
                    return;
                }
                if (newValue.length() > 5) {
                    passwordField.setText(oldValue);
                } else {
                    passwordField.setText(newValue);
                }
            }
        });

        return true;
    }

    @FXML
    void closeBtnHandler(MouseEvent event) {
        stage.close();
    }

    @FXML
    void minimizeBtnHandler(MouseEvent event) {
        stage.setIconified(true);
    }

}
