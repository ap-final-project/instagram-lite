package Controller;

import API.Values;
import Configuration.Config;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import models.Like;
import models.Post;
import models.User;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class StartLoader extends Application {
    public static double xOffset = 0;
    public static double yOffset = 0;
    public static Stage stage;


    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = StartLoader.loadFxml("/login.fxml").load();
        Scene scene = initStyle(primaryStage, root);

        scene.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });

        scene.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                primaryStage.setX(event.getScreenX() - xOffset);
                primaryStage.setY(event.getScreenY() - yOffset);
            }
        });
    }

    private Scene initStyle(Stage primaryStage, Parent root) throws IOException {
        Scene scene = new Scene(root);
        primaryStage.setTitle(Config.getInstance().getStringValue(Values.TITLE_CONFIG));
        primaryStage.setResizable(Config.getInstance().getBooleanValue(Values.RESIZE_CONFIG));
        primaryStage.setScene(scene);
        primaryStage.initStyle(StageStyle.UNDECORATED);
        primaryStage.show();
        stage = primaryStage;

        return scene;
    }

    public static FXMLLoader loadFxml(String path) throws IOException {
        FXMLLoader loader = new FXMLLoader(StartLoader.class.getResource(path));
        return loader;
    }

    private static void main(String[] args) {
        launch(args);
    }
}
