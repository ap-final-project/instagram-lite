package Controller;

import API.Messages;
import com.jfoenix.controls.JFXButton;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import models.User;

import java.io.IOException;
import java.util.concurrent.BrokenBarrierException;

import static Controller.LoadingFXMLs.loadOtherProfilePageFXML;
import static Controller.LoadingFXMLs.loadProfilePageFXML;
import static Controller.LoginCtrl.*;
import static Controller.MainPageCtrl.mainStackPane;
import static Controller.MainPageCtrl.showDialog;
import static Controller.ProfilePageCtrl.mainPageCtrl;
import static Controller.ProfilePageCtrl.sendRequest;

public class UserBoxCtrl {
    private Pane pane;
    private final String FOLLOW = "Follow";
    private final String UNFOLLOW = "UnFollow";

    @FXML
    private Label userIDLabel;

    @FXML
    private Pane mainPane;

    @FXML
    private Circle circleImage;

    @FXML
    private Label usernameLabel;

    @FXML
    private JFXButton followUnfollowBtn;


    public void setPaneForClass(Pane pane) {
        this.pane = pane;
    }

    public void setDataForBox(String id, String name, Boolean follow, Image image) {
        userIDLabel.setText(id);
        usernameLabel.setText(name);

        if (follow == null) {
            mainUserStyle();
        } else if (!follow) {
            followStyle();
        } else {
            unfollowStyle();
        }

        if (image == null) {
            circleImage.setVisible(false);
            FontAwesomeIconView avatar = new FontAwesomeIconView();
            avatar.setGlyphName("USER_CIRCLE");
            avatar.setSize("60");
            avatar.setLayoutX(15);
            avatar.setLayoutY(56);
            avatar.setFill(Color.web("#a8a5a5"));
            mainPane.getChildren().add(avatar);
        } else {
            circleImage.setFill(new ImagePattern(image));
        }
    }


    @FXML
    private void followUnfollowAction(ActionEvent event) throws BrokenBarrierException, InterruptedException {
        if (followUnfollowBtn.getText().equals(FOLLOW)) {
            long id = mainConnection.followReq(userIDLabel.getText());
            cyclicBarrier.await();
            boolean result = responseHandler.handleFollowResponse(id);

            if (result) {
                unfollowStyle();
            } else {
                showDialog(mainStackPane, Messages.PROBLEM);
            }

        } else {
            long id = mainConnection.unFollowReq(userIDLabel.getText());
            cyclicBarrier.await();
            boolean result = responseHandler.handleUnfollowResponse(id);

            if (result) {
                followStyle();
            } else {
                showDialog(mainStackPane, Messages.PROBLEM);
            }
        }
        sendRequest();
    }

    @FXML
    private void boxAction(MouseEvent event) throws BrokenBarrierException, InterruptedException, IOException {
        Parent parent = null;
        if (userIDLabel.getText().equals(mainUser.getUserID())) {
            sendRequest();
            parent = loadProfilePageFXML(mainPageCtrl);

        } else {
            long id = mainConnection.getUserProfileReq(userIDLabel.getText());
            cyclicBarrier.await();
            User user = responseHandler.handleGetUserProfileResponse(id);

            if (user != null) {
                parent = loadOtherProfilePageFXML(user);
            }
        }

        pane.getChildren().setAll(parent);


    }

    private void followStyle() {
        followUnfollowBtn.setText(FOLLOW);
        followUnfollowBtn.setStyle("-fx-background-color: #0095F6");
    }

    private void unfollowStyle() {
        followUnfollowBtn.setText(UNFOLLOW);
        followUnfollowBtn.setStyle("-fx-background-color: #A4C9EBFF");
    }

    private void mainUserStyle() {
        followUnfollowBtn.setVisible(false);
    }
}
