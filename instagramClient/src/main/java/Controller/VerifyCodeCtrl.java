package Controller;

import API.Messages;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.BrokenBarrierException;

import static Controller.LoadingFXMLs.loadSignUpPageFXML;
import static Controller.LoginCtrl.*;
import static Controller.SignUpCtrl.code;
import static Controller.StartLoader.*;

public class VerifyCodeCtrl implements Initializable {
    private String email;

    @FXML
    private Pane pane;

    @FXML
    private Label errorLbl;

    @FXML
    public Label description;

    @FXML
    private StackPane stackPane;

    @FXML
    private TextField codeField;


    public void setEmail(String email) {
        this.email = email;
    }

    @FXML
    private void enterCodeBtnHandler(ActionEvent event) throws InterruptedException, BrokenBarrierException {
        errorLbl.setText("");
        if (codeField.getText().equals(code)) {

            long reqId = mainConnection.verifyReq(true, email);
            cyclicBarrier.await();
            boolean result = responseHandler.handleVerifyResponse(reqId);

            Text text = new Text();
            if (result) {
                text.setText(Messages.SIGNUP_SUCCESSFUL);
            } else {
                text.setText(Messages.PROBLEM);
            }

            JFXDialogLayout layout = new JFXDialogLayout();
            text.setFont(Font.font(14));
            layout.setBody(text);
            layout.setStyle("-fx-background-color: #94ACD9");

            JFXDialog dialog = new JFXDialog(stackPane, layout, JFXDialog.DialogTransition.CENTER);

            JFXButton ok = new JFXButton("Ok");
            ok.setButtonType(JFXButton.ButtonType.RAISED);
            ok.setStyle("-fx-background-color: #221f3b");
            ok.setTextFill(Color.rgb(255, 226, 255));


            ok.setOnAction(e -> {
                dialog.close();
                stage.close();
            });

            JFXButton back = new JFXButton("Back To Login Page");
            back.setButtonType(JFXButton.ButtonType.RAISED);
            back.setStyle("-fx-background-color: #221f3b");
            back.setTextFill(Color.rgb(255, 226, 255));

            layout.setActions(ok, back);

            back.setOnAction(e -> {
                Parent root = null;
                try {
                    root = loadFxml("/login.fxml").load();
                    pane.setDisable(false);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                dialog.close();
                pane.getChildren().setAll(root);
                System.out.println("EnterCode page loaded");
            });

            dialog.show();
            pane.setDisable(true);
        } else {
            errorLbl.setText(Messages.INCORRECT_CODE);
        }


    }

    @FXML
    private void changeEmailHandler(MouseEvent event) throws IOException {
        pane.getChildren().setAll(loadSignUpPageFXML());
    }

    @FXML
    private void closeBtnHandler(MouseEvent event) {
        stage.close();
    }

    @FXML
    private void minimizeBtnHandler(MouseEvent event) {
        stage.setIconified(true);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        errorLbl.setFont(new Font(13));

        description.setText("We'll send a verification code to your Email address.\n" +
                "please enter it below.\n" +
                "Your Email address is :\n");

        codeField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable,
                                String oldValue, String newValue) {

                if (newValue == null) {
                    return;
                }
                if (newValue.length() > 5) {
                    codeField.setText(oldValue);
                } else {
                    codeField.setText(newValue);
                }
            }
        });
    }
}
