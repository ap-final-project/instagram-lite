package models;

import javafx.scene.image.Image;

import java.io.File;

import static Controller.LoginCtrl.mainConnection;

public class Activity {
    private String date;
    private String media;
    private String userID;
    private String postID;
    private String comment;
    private int filesNumber;
    private String repliedTo;
    private String commentID;
    private String profilePic;
    private String activityID;
    private String otherUserID;
    private String notification;
    private ActivityType type;

    public Image getProfImage() {
        if (profilePic != null) {
            return new Image(new File(mainConnection.dataPath + profilePic).toURI().toString());
        } else {
            return null;
        }
    }

    public Image getPostImage() {
        if (profilePic != null) {
            return new Image(new File(mainConnection.dataPath + media).toURI().toString());
        } else {
            return null;
        }
    }

    public String getDate() {
        return date;
    }

    public String getUserID() {
        return userID;
    }

    public String getPostID() {
        return postID;
    }

    public String getComment() {
        return comment;
    }

    public ActivityType getType() {
        return type;
    }

    public String getOtherUserID() {
        return otherUserID;
    }

    public String getNotification() {
        return notification;
    }


    public void setDate(String date) {
        this.date = date;
    }

    public void setType(ActivityType type) {
        this.type = type;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
