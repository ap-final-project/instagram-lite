package models;

public enum ActivityType {
    MENTION,
    COMMENT,
    DIRECT,
    FOLLOW,
    LIKE,
}
