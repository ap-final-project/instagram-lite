package models;

import javafx.scene.image.Image;

import java.io.File;

import static Controller.LoginCtrl.mainConnection;

public class Comment {
    private String date;
    private String userID;
    private String postID;
    private String comment;
    private String commentID;
    private String repliedTo;
    private String otherUserID;
    private String profilePic;

    public Comment(String userID, String replyID, String postID, String content, String otherUserID, String profPic) {
        this.postID = postID;
        this.userID = userID;
        this.comment = content;
        this.repliedTo = replyID;
        this.profilePic = profPic;
        this.otherUserID = otherUserID;
    }

    public Image getImage() {
        if (profilePic != null) {
            return new Image(new File(mainConnection.dataPath + profilePic).toURI().toString());
        } else {
            return null;
        }
    }

    public String getDate() {
        return date;
    }

    public String getUserID() {
        return userID;
    }

    public String getPostID() {
        return postID;
    }

    public String getComment() {
        return comment;
    }

    public String getRepliedTo() {
        return repliedTo;
    }

    public String getOtherUserID() {
        return otherUserID;
    }


    public void setDate(String date) {
        this.date = date;
    }

    public void setCommentID(String commentID) {
        this.commentID = commentID;
    }
}

