package models;

import javafx.scene.image.Image;

import java.io.File;
import java.util.ArrayList;

import static Controller.LoginCtrl.mainConnection;
import static Controller.LoginCtrl.mainUser;

public class DirectMessage {
    private String chatID;
    private String userID;
    private String profilePic;
    private String lastMessage;
    private ArrayList<Message> messages;

    public DirectMessage(String userID) {
        this.userID = userID;
        this.messages = new ArrayList<>();
        setChatID();
    }

    public void setChatID() {
        if (mainUser.getUserID().compareTo(userID) > 0) {
            chatID = userID + "#" + userID;
        } else {
            chatID = mainUser.getUserID() + "#" + userID;
        }
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getChatID() {
        return chatID;
    }

    public Image getProfileImage() {
        if (profilePic != null) {
            return new Image(new File(mainConnection.dataPath + profilePic).toURI().toString());
        } else {
            return null;
        }
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public ArrayList<Message> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<Message> messages) {
        this.messages = messages;
    }
}
