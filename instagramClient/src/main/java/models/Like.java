package models;

import javafx.scene.image.Image;

import java.io.File;

import static Controller.LoginCtrl.mainConnection;

public class Like {
    private String name;
    private String userID;
    private boolean follow;
    private String profilePicture;

    public Like(String name, String userID, boolean follow, String profilePicture) {
        this.name = name;
        this.userID = userID;
        this.follow = follow;
        this.profilePicture = profilePicture;
    }

    public Image getProfileImage() {
        return new Image(new File(mainConnection.dataPath + profilePicture).toURI().toString());
    }

    public String getName() {
        return name;
    }

    public boolean isFollow() {
        return follow;
    }

    public String getUserID() {
        return userID;
    }

}
