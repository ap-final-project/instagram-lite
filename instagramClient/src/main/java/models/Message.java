package models;

public class Message {
    private String date;
    private String sender;
    private String receiver;
    private String content;


    public Message(String receiver, String sender, String content) {
        this.sender = sender;
        this.receiver = receiver;
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public String getSender() {
        return sender;
    }

    public String getContent() {
        return content;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
