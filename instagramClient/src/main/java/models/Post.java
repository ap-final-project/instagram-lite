package models;

import javafx.scene.image.Image;

import java.io.File;
import java.util.ArrayList;

import static Controller.LoginCtrl.mainConnection;


public class Post {
    private int likes;
    private String date;
    private String media;
    private String postID;
    private boolean liked;
    private String userID;
    private String caption;
    private String profilePic;
    private ArrayList<Like> likesArray;
    private ArrayList<Comment> comments;

    public Post(String caption, String senderUserID) {
        this.caption = caption;
        this.userID = senderUserID;
        this.comments = new ArrayList<>();
        this.likesArray = new ArrayList<>();
    }

    public Image getProfileImage() {
        if (profilePic != null) {
            return new Image(new File(mainConnection.dataPath + profilePic).toURI().toString());
        } else {
            return null;
        }
    }

    public int getLikes() {
        return likes;
    }

    public String getDate() {
        return date;
    }

    public boolean isLiked() {
        return liked;
    }

    public String getMedia() {
        return media;
    }

    public String getUserID() {
        return userID;
    }

    public String getPostID() {
        return postID;
    }

    public String getCaption() {
        return caption;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public Image getPostImage() {
        return new Image(new File(mainConnection.dataPath + media).toURI().toString());
    }


    public void setLikes(int likes) {
        this.likes = likes;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public void setPostID(String postID) {
        this.postID = postID;
    }

    public void addNewComment(Comment comment) {
        this.comments.add(comment);
    }

    public void setLikesArray(ArrayList<Like> likesArray) {
        this.likesArray = likesArray;
    }

}
