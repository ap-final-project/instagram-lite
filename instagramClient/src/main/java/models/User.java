package models;


import javafx.scene.image.Image;

import java.io.File;
import java.util.ArrayList;

import static Controller.LoginCtrl.mainConnection;

public class User {
    private String bio;
    private String userName;
    private String email;
    private String userID;
    private String password;
    private boolean followed;
    private int numberOfPost;
    private String profilePic;
    private int numberOfFollower;
    private int numberOfFollowing;
    private ArrayList<Post> posts;


    public User(String userID, String userName, String email, String password) {
        this.userName = userName;
        this.email = email;
        this.userID = userID;
        this.password = password;
        this.posts = new ArrayList<>();
    }

    public User(String userID, String userName) {
        this.userName = userName;
        this.userID = userID;
        this.posts = new ArrayList<>();
    }

    public Image getAx() {
        if (profilePic != null) {
            return new Image(new File(mainConnection.dataPath + profilePic).toURI().toString());
        } else {
            return null;
        }
    }

    public String getBio() {
        return bio;
    }

    public String getUserName() {
        return userName;
    }

    public String getEmail() {
        return email;
    }

    public boolean isFollowed() {
        return followed;
    }

    public String getUserID() {
        return userID;
    }

    public ArrayList<Post> getPosts() {
        return posts;
    }

    public int getNumberOfPost() {
        return numberOfPost;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public int getNumberOfFollower() {
        return numberOfFollower;
    }

    public int getNumberOfFollowing() {
        return numberOfFollowing;
    }


    public void setFollowed(boolean followed) {
        this.followed = followed;
    }


    public void setBio(String bio) {
        this.bio = bio;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public void setPosts(ArrayList<Post> posts) {
        this.posts = posts;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setNumberOfPost(int numberOfPost) {
        this.numberOfPost = numberOfPost;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public void setNumberOfFollower(int numberOfFollower) {
        this.numberOfFollower = numberOfFollower;
    }

    public void setNumberOfFollowing(int numberOfFollowing) {
        this.numberOfFollowing = numberOfFollowing;
    }


}
