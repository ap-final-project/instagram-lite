package socket;

import API.Requests;
import API.Values;
import Configuration.Config;
import fileHandling.FileManager;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Requests are created from here( using Requests class).
 * Each request is created and sent to server in a new thread
 */

public class Connection {
    private final SocketClass socketClass;
    public static FileManager fileManager;
    private final Requests requestsClass;
    private final ExecutorService pool;
    public static long RequestID = 0;
    public final Listener listener;
    private final SSSocket socket;
    public String dataPath;


    public Connection() throws IOException {
        String host = Config.getInstance().getStringValue(Values.HOST_CONFIG);
        int port = Config.getInstance().getIntValue(Values.PORT_CONFIG);

        this.socket = new SSSocket(host, port);
        this.pool = Executors.newCachedThreadPool();
        this.requestsClass = new Requests();
        this.socketClass = new SocketClass(host, 8888);
        this.listener = new Listener(socket);

        this.dataPath = "instagramClient/src/ImagesData/";
        fileManager = new FileManager(socketClass, dataPath);
    }


    synchronized public static long getRequestID() {
        return RequestID++;
    }


    public long loginReq(String id, String pass) {
        long req = getRequestID();
        String json = requestsClass.creatLoginRequest(req, id, pass);
        pool.submit(new RequestSender(socket, json));

        return req;
    }

    public long signUpReq(String email, String name, String id, String pass) {
        long req = getRequestID();
        String json = requestsClass.creatSignUpRequest(req, id, name, email, pass);
        pool.submit(new RequestSender(socket, json));

        return req;
    }

    public long forgotPassReq(String email) {
        long req = getRequestID();
        String json = requestsClass.createForgotPassRequest(req, email);
        pool.submit(new RequestSender(socket, json));

        return req;
    }

    public long changeEmailReq(String email) {
        long req = getRequestID();
        String json = requestsClass.createChangeEmailRequest(req, email);
        pool.submit(new RequestSender(socket, json));

        return req;
    }

    public long editBioReq(String bio) {
        long req = getRequestID();
        String json = requestsClass.createEditBioRequest(req, bio);
        pool.submit(new RequestSender(socket, json));

        return req;
    }

    public long editUserNameReq(String userName) {
        long req = getRequestID();
        String json = requestsClass.createEditUserNameRequest(req, userName);
        pool.submit(new RequestSender(socket, json));

        return req;
    }

    public long verifyReq(boolean result, String email) {
        long req = getRequestID();
        String json = requestsClass.createVerifyRequest(req, email, result);
        pool.submit(new RequestSender(socket, json));

        return req;
    }

    public long editProfilePicReq(String fileName) {
        long req = getRequestID();
        String json = requestsClass.createEditProfilePicRequest(req, fileName);
        pool.submit(new RequestSender(socket, json));

        fileManager.sendOne("Temp/" + fileName);

        return req;
    }

    public long removeProfilePicReq() {
        long req = getRequestID();
        String json = requestsClass.createRemoveProfilePicRequest(req);
        pool.submit(new RequestSender(socket, json));

        return req;
    }

    public long addPostReq(String fileName, String caption) {
        long req = getRequestID();
        String json = requestsClass.createAddPostRequest(req, fileName, caption);
        pool.submit(new RequestSender(socket, json));

        fileManager.sendOne("Temp/" + fileName);

        return req;
    }

    public long getTimeLineReq(String last) {
        long req = getRequestID();
        String json = requestsClass.createGetTimeLineRequest(req, last);
        pool.submit(new RequestSender(socket, json));

        return req;
    }

    public long getMainUserProfile() {
        long req = getRequestID();
        String json = requestsClass.createGetMainUserProfileRequest(req);
        pool.submit(new RequestSender(socket, json));

        return req;
    }

    public long changePassReq(String oldPassword, String newPassword) {
        long req = getRequestID();
        String json = requestsClass.createChangePassRequest(req, oldPassword, newPassword);
        pool.submit(new RequestSender(socket, json));

        return req;
    }

    public long likePostReq(String otherUser, String postID) {
        long req = getRequestID();
        String json = requestsClass.createLikeRequest(req, otherUser, postID);
        pool.submit(new RequestSender(socket, json));

        return req;
    }

    public long unLikePostReq(String otherUser, String postID) {
        long req = getRequestID();
        String json = requestsClass.createUnLikeRequest(req, otherUser, postID);
        pool.submit(new RequestSender(socket, json));

        return req;
    }

    public long addCommentReq(String repliedTo, String postID,
                              String comment, String otherUserID) {
        long req = getRequestID();
        String json = requestsClass.createAddCommentRequest(req, repliedTo,
                postID, comment, otherUserID);
        pool.submit(new RequestSender(socket, json));

        return req;
    }

    public long getCommentsListReq(String postID, String last) {
        long req = getRequestID();
        String json = requestsClass.createGetCommentsListRequest(req, postID, last);
        pool.submit(new RequestSender(socket, json));

        return req;
    }

    public long getLikesListReq(String postID, String last) {
        long req = getRequestID();
        String json = requestsClass.createGetLikesListRequest(req, postID, last);
        pool.submit(new RequestSender(socket, json));

        return req;
    }

    public long getFollowersListReq(String userID, String last) {
        long req = getRequestID();
        String json = requestsClass.createGetFollowersListRequest(req, userID, last);
        pool.submit(new RequestSender(socket, json));

        return req;
    }

    public long getFollowingListReq(String userID, String last) {
        long req = getRequestID();
        String json = requestsClass.createGetFollowingListRequest(req, userID, last);
        pool.submit(new RequestSender(socket, json));

        return req;
    }

    public long followReq(String otherUser) {
        long req = getRequestID();
        String json = requestsClass.createFollowRequest(req, otherUser);
        pool.submit(new RequestSender(socket, json));

        return req;
    }

    public long unFollowReq(String otherUser) {
        long req = getRequestID();
        String json = requestsClass.createUnfollowRequest(req, otherUser);
        pool.submit(new RequestSender(socket, json));

        return req;
    }

    public long getUserProfileReq(String otherUser) {
        long req = getRequestID();
        String json = requestsClass.createGetUserProfileRequest(req, otherUser);
        pool.submit(new RequestSender(socket, json));

        return req;
    }

    public long deletePostReq(String postID) {
        long req = getRequestID();
        String json = requestsClass.createDeletePostRequest(req, postID);
        pool.submit(new RequestSender(socket, json));

        return req;
    }

    public long deleteAccountReq(String password) {
        long req = getRequestID();
        String json = requestsClass.createDeleteAccountRequest(req, password);
        pool.submit(new RequestSender(socket, json));

        return req;
    }

    public long getMorePostReq(String userID, String last) {
        long req = getRequestID();
        String json = requestsClass.createGerMorePostRequest(req, userID, last);
        pool.submit(new RequestSender(socket, json));

        return req;
    }

    public long logoutReq() {
        long req = getRequestID();
        String json = requestsClass.createLogoutRequest(req);
        pool.submit(new RequestSender(socket, json));

        return req;
    }

    public long getDirectMessagesReq(String chatID, String length) {
        long req = getRequestID();
        String json = requestsClass.createGetDirectMessagesRequest(req, chatID, length);
        pool.submit(new RequestSender(socket, json));

        return req;
    }

    public long getActivitiesReq(String last) {
        long req = getRequestID();
        String json = requestsClass.createGetActivitiesRequest(req, last);
        pool.submit(new RequestSender(socket, json));

        return req;
    }

    public long getChatListReq() {
        long req = getRequestID();
        String json = requestsClass.createGetChatListRequest(req);
        pool.submit(new RequestSender(socket, json));

        return req;
    }

    public long sendDirectMessageReq(String receiver, String content) {
        long req = getRequestID();
        String json = requestsClass.createSendDirectMessageRequest(req, receiver, content);
        pool.submit(new RequestSender(socket, json));

        return req;
    }


}