package socket;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.ConcurrentHashMap;

import static Controller.LoginCtrl.cyclicBarrier;

/**
 * Gets responses and notifications in its very own thread and converts them to proper objects
 */

public class Listener implements Runnable {
    public static ConcurrentHashMap<Long, JsonObject> responses;
    public static ArrayBlockingQueue<JsonObject> notifications;
    private SSSocket socket;
    private Gson gson;

    public Listener(SSSocket socket) {
        this.socket = socket;
        this.gson = new Gson();
        responses = new ConcurrentHashMap<>();
        new Thread(this).start();
    }

    @Override
    public void run() {
        while (true) {
            try {
                byte[] mess = socket.readMessage();

                if (mess != null) {
                    String str = new String(mess);

                    JsonObject reaction = gson.fromJson(str, JsonObject.class);

                    if (reaction.has("response")) {

                        responses.put(reaction.get("requestID").getAsLong(), reaction);

                        System.out.println("reaction : " + reaction);

                        cyclicBarrier.await();
                        //todo:mitonam await ro bokonam toye if, yani age folan id k mn mikham umadesh await ro seda bezan ta azad bashe
                        //todo: hata mitonam y counter ham bzarm k masalan ta 3 ta resp sabr kone
                    } else if (reaction.has("notification")) {
                        notifications.put(reaction);
                    }
                }
            } catch (BrokenBarrierException | IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
