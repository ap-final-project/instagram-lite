package socket;

import java.io.IOException;

/**
 * send request using thread
 */

public class RequestSender implements Runnable {
    private SSSocket socket;
    private String request;


    public RequestSender(SSSocket socket, String request) {
        this.socket = socket;
        this.request = request;
    }

    @Override
    public void run() {
        try {
            //todo: use cryptoClass
            System.out.println("Thread ID: " + Thread.currentThread().getId());
            socket.sendMessage(request.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
