package socket;

import API.CommonValues;
import API.Responses;
import Controller.SignUpCtrl;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import models.*;

import java.util.ArrayList;

import static Controller.LoginCtrl.mainUser;
import static socket.Connection.fileManager;


public class ResponseHandler {
    private JsonObject jsonString;
    private JsonObject jsonData;
    private final Gson gson;

    public ResponseHandler() {
        this.jsonString = new JsonObject();
        this.gson = new Gson();
    }

    private void setJsonData() {
        jsonData = jsonString.getAsJsonObject(CommonValues.DATA);
    }

    public Object handleLoginResponse(long reqID) {
        jsonString = Listener.responses.get(reqID);
        Listener.responses.remove(reqID);
        setJsonData();

        Responses.Login loginInfo = gson.fromJson(jsonString, Responses.Login.class);
        Responses.Login.data data = gson.fromJson(jsonData, Responses.Login.data.class);


        if (loginInfo.result) {
            fileManager.receiveMany(data.filesNumber);

            User user = new User(data.userID, data.userName, data.email, data.password);
            user.setNumberOfFollower(data.followersNumber);
            user.setNumberOfFollowing(data.followingNumber);
            user.setProfilePic(data.profilePic);
            user.setNumberOfPost(data.postNumber);
            user.setBio(data.bio);

            ArrayList<Post> arrayList = new ArrayList<>();
            if (!jsonData.get("posts").getAsJsonArray().isEmpty()) {
                for (String a : data.posts) {
                    Post post = gson.fromJson(a, Post.class);
                    arrayList.add(post);
                }
            }

            user.setPosts(arrayList);

            return user;
        } else {
            return data.message;
        }
    }

    public Object handleSignupResponse(long reqID) {
        jsonString = Listener.responses.get(reqID);
        Listener.responses.remove(reqID);
        setJsonData();

        Responses.SignUp info = gson.fromJson(jsonString, Responses.SignUp.class);
        Responses.SignUp.data data = gson.fromJson(jsonData, Responses.SignUp.data.class);


        if (info.result) {
            SignUpCtrl.code = data.code;
            return true;
        } else {
            return data.message;
        }
    }

    public boolean handleVerifyResponse(long reqID) {
        jsonString = Listener.responses.get(reqID);
        Listener.responses.remove(reqID);
        setJsonData();

        Responses.Verify info = gson.fromJson(jsonString, Responses.Verify.class);

        if (info.result) {
            return true;
        } else {
            return false;
        }
    }

    public boolean handleEditProfilePicResponse(long reqID) {
        jsonString = Listener.responses.get(reqID);
        Listener.responses.remove(reqID);
        setJsonData();

        Responses.EditProfilePic info = gson.fromJson(jsonString, Responses.EditProfilePic.class);

        return info.result;
    }

    public boolean handleRemoveProfilePicResponse(long reqID) {
        jsonString = Listener.responses.get(reqID);
        Listener.responses.remove(reqID);
        setJsonData();

        Responses.RemoveProfilePic info = gson.fromJson(jsonString, Responses.RemoveProfilePic.class);

        return info.result;
    }

    public boolean handleEditUserNameResponse(long reqID) {
        jsonString = Listener.responses.get(reqID);
        Listener.responses.remove(reqID);
        setJsonData();

        Responses.EditUserName info = gson.fromJson(jsonString, Responses.EditUserName.class);

        return info.result;
    }

    public boolean handleEditBioResponse(long reqID) {
        jsonString = Listener.responses.get(reqID);
        Listener.responses.remove(reqID);
        setJsonData();

        Responses.EditBio info = gson.fromJson(jsonString, Responses.EditBio.class);

        return info.result;
    }

    public boolean handleChangeEmailResponse(long reqID) {
        jsonString = Listener.responses.get(reqID);
        Listener.responses.remove(reqID);
        setJsonData();

        Responses.ChangeEmail info = gson.fromJson(jsonString, Responses.ChangeEmail.class);

        return info.result;
    }

    public Object handleAddPostResponse(long reqID) {
        jsonString = Listener.responses.get(reqID);
        Listener.responses.remove(reqID);
        setJsonData();

        Responses.AddPost info = gson.fromJson(jsonString, Responses.AddPost.class);
        Responses.AddPost.data data = gson.fromJson(jsonData, Responses.AddPost.data.class);


        if (info.result) {
            return data.postID;
        }

        return false;
    }

    public ArrayList<Post> handleGetTimeLineResponse(long reqID) {
        jsonString = Listener.responses.get(reqID);
        Listener.responses.remove(reqID);
        setJsonData();
        int filesNumber;

        Responses.GetTimeline info = gson.fromJson(jsonString, Responses.GetTimeline.class);
        Responses.GetTimeline.data data = gson.fromJson(jsonData, Responses.GetTimeline.data.class);

        ArrayList<Post> arrayList = new ArrayList<>();

        if (info.result) {
            filesNumber = data.filesNumber;
            fileManager.receiveMany(filesNumber);

            if (!jsonData.get("posts").getAsJsonArray().isEmpty()) {
                for (String a : data.posts) {
                    Post post = gson.fromJson(a, Post.class);
                    arrayList.add(post);
                }

                return arrayList;
            }
        }

        return arrayList;
    }

    public User handleGetMainUserProfileResponse(long reqID) {
        jsonString = Listener.responses.get(reqID);
        Listener.responses.remove(reqID);
        setJsonData();

        Responses.GetMainUserProfile loginInfo = gson.fromJson(jsonString, Responses.GetMainUserProfile.class);
        Responses.GetMainUserProfile.data data = gson.fromJson(jsonData, Responses.GetMainUserProfile.data.class);

        if (loginInfo.result) {
            fileManager.receiveMany(data.filesNumber);

            User user = new User(data.userID, data.userName, data.email, data.password);
            user.setNumberOfFollower(data.followersNumber);
            user.setNumberOfFollowing(data.followingNumber);
            user.setProfilePic(data.profilePic);
            user.setNumberOfPost(data.postNumber);
            user.setBio(data.bio);


            ArrayList<Post> arrayList = new ArrayList<>();
            if (!jsonData.get("posts").getAsJsonArray().isEmpty()) {
                for (String a : data.posts) {
                    Post post = gson.fromJson(a, Post.class);
                    arrayList.add(post);
                }
            }

            user.setPosts(arrayList);

            return user;
        }
        return null;
    }

    public boolean handleLikeResponse(long reqID) {
        jsonString = Listener.responses.get(reqID);
        Listener.responses.remove(reqID);
        setJsonData();

        Responses.Like info = gson.fromJson(jsonString, Responses.Like.class);

        return info.result;
    }

    public boolean handleUnLikeResponse(long reqID) {
        jsonString = Listener.responses.get(reqID);
        Listener.responses.remove(reqID);
        setJsonData();

        Responses.Unlike info = gson.fromJson(jsonString, Responses.Unlike.class);

        return info.result;
    }

    public Object handleAddCommentResponse(long reqID) {
        jsonString = Listener.responses.get(reqID);
        Listener.responses.remove(reqID);
        setJsonData();

        Responses.AddComment info = gson.fromJson(jsonString, Responses.AddComment.class);
        Responses.AddComment.data data = gson.fromJson(jsonData, Responses.AddComment.data.class);

        if (info.result) {
            return data.commentID;
        }
        return false;
    }

    public ArrayList<Comment> handleGetCommentsListResponse(long reqID) {
        jsonString = Listener.responses.get(reqID);
        Listener.responses.remove(reqID);
        setJsonData();

        Responses.GetCommentList info = gson.fromJson(jsonString, Responses.GetCommentList.class);
        Responses.GetCommentList.data data = gson.fromJson(jsonData, Responses.GetCommentList.data.class);


        ArrayList<Comment> arrayList = new ArrayList<>();
        if (info.result) {
            fileManager.receiveMany(data.filesNumber);

            if (!jsonData.get("comments").getAsJsonArray().isEmpty()) {
                for (String a : data.comments) {
                    Comment comment = gson.fromJson(a, Comment.class);
                    arrayList.add(comment);
                }

                return arrayList;
            }
        }

        return arrayList;
    }

    public ArrayList<Like> handleGetLikesListResponse(long reqID) {
        jsonString = Listener.responses.get(reqID);
        Listener.responses.remove(reqID);
        setJsonData();

        Responses.GetLikeList info = gson.fromJson(jsonString, Responses.GetLikeList.class);
        Responses.GetLikeList.data data = gson.fromJson(jsonData, Responses.GetLikeList.data.class);


        ArrayList<Like> arrayList = new ArrayList<>();

        if (info.result) {
            fileManager.receiveMany(data.filesNumber);

            if (!jsonData.get("likes").getAsJsonArray().isEmpty()) {
                for (String a : data.likes) {
                    Like like = gson.fromJson(a, Like.class);
                    arrayList.add(like);
                }

                return arrayList;
            }
        }

        return arrayList;
    }

    public ArrayList<User> handleGetFollowersListResponse(long reqID) {
        jsonString = Listener.responses.get(reqID);
        Listener.responses.remove(reqID);
        setJsonData();

        Responses.GetFollowersList info = gson.fromJson(jsonString, Responses.GetFollowersList.class);
        Responses.GetFollowersList.data data = gson.fromJson(jsonData, Responses.GetFollowersList.data.class);


        ArrayList<User> arrayList = new ArrayList<>();
        if (info.result) {
            fileManager.receiveMany(data.filesNumber);

            if (!jsonData.get("followers").getAsJsonArray().isEmpty()) {
                for (String a : data.followers) {
                    User user = gson.fromJson(a, User.class);
                    arrayList.add(user);
                }

                return arrayList;
            }
        }

        return arrayList;
    }

    public ArrayList<User> handleGetFollowingListResponse(long reqID) {
        jsonString = Listener.responses.get(reqID);
        Listener.responses.remove(reqID);
        setJsonData();

        Responses.GetFollowingList info = gson.fromJson(jsonString, Responses.GetFollowingList.class);
        Responses.GetFollowingList.data data = gson.fromJson(jsonData, Responses.GetFollowingList.data.class);


        ArrayList<User> arrayList = new ArrayList<>();
        if (info.result) {
            fileManager.receiveMany(data.filesNumber);

            if (!jsonData.get("following").getAsJsonArray().isEmpty()) {
                for (String a : data.following) {
                    User user = gson.fromJson(a, User.class);
                    arrayList.add(user);
                }

                return arrayList;
            }
        }

        return arrayList;
    }

    public boolean handleFollowResponse(long reqID) {
        jsonString = Listener.responses.get(reqID);
        Listener.responses.remove(reqID);
        setJsonData();

        Responses.Follow info = gson.fromJson(jsonString, Responses.Follow.class);

        return info.result;
    }

    public boolean handleUnfollowResponse(long reqID) {
        jsonString = Listener.responses.get(reqID);
        Listener.responses.remove(reqID);
        setJsonData();

        Responses.Unfollow info = gson.fromJson(jsonString, Responses.Unfollow.class);

        return info.result;
    }

    public boolean handleDeletePostResponse(long reqID) {
        jsonString = Listener.responses.get(reqID);
        Listener.responses.remove(reqID);
        setJsonData();

        Responses.DeletePost info = gson.fromJson(jsonString, Responses.DeletePost.class);

        return info.result;
    }

    public User handleGetUserProfileResponse(long reqID) {
        jsonString = Listener.responses.get(reqID);
        Listener.responses.remove(reqID);
        setJsonData();

        Responses.GetUserProfile loginInfo = gson.fromJson(jsonString, Responses.GetUserProfile.class);
        Responses.GetUserProfile.data data = gson.fromJson(jsonData, Responses.GetUserProfile.data.class);

        if (loginInfo.result) {
            fileManager.receiveMany(data.filesNumber);

            User user = new User(data.userID, data.userName);
            user.setNumberOfFollower(data.followersNumber);
            user.setNumberOfFollowing(data.followingNumber);
            user.setProfilePic(data.profilePic);
            user.setNumberOfPost(data.postNumber);
            user.setFollowed(data.followed);
            user.setBio(data.bio);


            ArrayList<Post> arrayList = new ArrayList<>();
            if (!jsonData.get("posts").getAsJsonArray().isEmpty()) {
                for (String a : data.posts) {
                    Post post = gson.fromJson(a, Post.class);
                    arrayList.add(post);
                }
            }

            user.setPosts(arrayList);

            return user;
        }
        return null;
    }

    public ArrayList<DirectMessage> handleGetChatListResponse(long reqID) {
        jsonString = Listener.responses.get(reqID);
        Listener.responses.remove(reqID);
        setJsonData();

        Responses.GetChatList loginInfo = gson.fromJson(jsonString, Responses.GetChatList.class);
        Responses.GetChatList.data data = gson.fromJson(jsonData, Responses.GetChatList.data.class);

        ArrayList<DirectMessage> arrayList = new ArrayList<>();
        if (loginInfo.result) {
            fileManager.receiveMany(data.filesNumber);

            if (!jsonData.get("chats").getAsJsonArray().isEmpty()) {
                for (String a : data.chats) {
                    DirectMessage directMessage = gson.fromJson(a, DirectMessage.class);
                    arrayList.add(directMessage);
                }
            }
            return arrayList;
        }

        return arrayList;
    }

    public ArrayList<Message> handleGetDirectMessagesResponse(long reqID) {
        jsonString = Listener.responses.get(reqID);
        Listener.responses.remove(reqID);
        setJsonData();

        Responses.GetDirectMessages loginInfo = gson.fromJson(jsonString, Responses.GetDirectMessages.class);
        Responses.GetDirectMessages.data data = gson.fromJson(jsonData, Responses.GetDirectMessages.data.class);

        ArrayList<Message> arrayList = new ArrayList<>();
        if (loginInfo.result) {

            if (!jsonData.get("directMessages").getAsJsonArray().isEmpty()) {
                for (String a : data.directMessages) {
                    Message message = gson.fromJson(a, Message.class);
                    arrayList.add(message);
                }
            }
            return arrayList;
        }

        return arrayList;
    }

    public ArrayList<Activity> handleGetActivitiesResponse(long reqID) {
        jsonString = Listener.responses.get(reqID);
        Listener.responses.remove(reqID);
        setJsonData();

        Responses.GetActivities loginInfo = gson.fromJson(jsonString, Responses.GetActivities.class);
        Responses.GetActivities.data data = gson.fromJson(jsonData, Responses.GetActivities.data.class);

        ArrayList<Activity> arrayList = new ArrayList<>();
        if (loginInfo.result) {
            fileManager.receiveMany(data.filesNumber);

            if (!jsonData.get("activities").getAsJsonArray().isEmpty()) {
                for (String a : data.activities) {
                    Activity activity = gson.fromJson(a, Activity.class);
                    switch (activity.getNotification()) {
                        case "comment" -> activity.setType(ActivityType.COMMENT);

                        case "directMessage" -> activity.setType(ActivityType.DIRECT);

                        case "follow" -> activity.setType(ActivityType.FOLLOW);

                        case "like" -> activity.setType(ActivityType.LIKE);

                        case "mentionOnComment" -> activity.setType(ActivityType.MENTION);
                    }
                    arrayList.add(activity);
                }
            }
            return arrayList;
        }

        return arrayList;
    }


    public boolean handleLogoutResponse(long reqID) {
        jsonString = Listener.responses.get(reqID);
        Listener.responses.remove(reqID);
        setJsonData();

        Responses.Logout info = gson.fromJson(jsonString, Responses.Logout.class);

        return info.result;
    }

    public boolean handleChangePassResponse(long reqID) {
        jsonString = Listener.responses.get(reqID);
        Listener.responses.remove(reqID);
        setJsonData();

        Responses.ChangePassword info = gson.fromJson(jsonString, Responses.ChangePassword.class);
        Responses.ChangePassword.data data = gson.fromJson(jsonData, Responses.ChangePassword.data.class);

        if (info.result) {
            mainUser.setPassword(data.password);
        }

        return info.result;
    }

    public boolean handleDeleteAccountResponse(long reqID) {
        jsonString = Listener.responses.get(reqID);
        Listener.responses.remove(reqID);
        setJsonData();

        Responses.DeleteAccount info = gson.fromJson(jsonString, Responses.DeleteAccount.class);

        return info.result;
    }

    public boolean handleSendMessageResponse(long reqID) {
        jsonString = Listener.responses.get(reqID);
        Listener.responses.remove(reqID);
        setJsonData();

        Responses.SendDirectMessage info = gson.fromJson(jsonString, Responses.SendDirectMessage.class);

        return info.result;
    }

    public boolean handleForgotPassResponse(long reqID) {
        jsonString = Listener.responses.get(reqID);
        Listener.responses.remove(reqID);
        setJsonData();

        Responses.ForgotPassword info = gson.fromJson(jsonString, Responses.ForgotPassword.class);

        return info.result;
    }
}
