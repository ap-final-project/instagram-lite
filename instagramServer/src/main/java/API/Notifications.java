package API;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;

import java.io.FileReader;
import java.io.IOException;

/**
 *
 * Includes methods to create Json String notification.
 *
 */
public class Notifications {

    public JsonObject getJsonObject(String path) {

        JsonObject jsonObject = new JsonObject();
        try (FileReader reader = new FileReader(path)) {
            Gson gson = new Gson();
            JsonReader jsonReader = new JsonReader(reader);
            jsonObject = gson.fromJson(jsonReader, JsonObject.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public String createLikeNotification(JsonObject data) {
        JsonObject jsonObject = getJsonObject(Paths.LIKE_NOTIFICATION_FILE_PATH);
        jsonObject.add(CommonValues.DATA, data);

        return jsonObject.toString();
    }

    public String createFollowNotification(JsonObject data) {
        JsonObject jsonObject = getJsonObject(Paths.FOLLOW_NOTIFICATION_FILE_PATH);
        jsonObject.add(CommonValues.DATA, data);

        return jsonObject.toString();
    }

    public String createCommentNotification(JsonObject data) {
        JsonObject jsonObject = getJsonObject(Paths.COMMENT_NOTIFICATION_FILE_PATH);
        jsonObject.add(CommonValues.DATA, data);

        return jsonObject.toString();
    }

    public String createMentionOnCommentNotification(JsonObject data) {
        JsonObject jsonObject = getJsonObject(Paths.MENTION_ON_COMMENT_NOTIFICATION_FILE_PATH);
        jsonObject.add(CommonValues.DATA, data);

        return jsonObject.toString();
    }

    public String createDirectMessageNotification(JsonObject data) {
        JsonObject jsonObject = getJsonObject(Paths.DIRECT_MESSAGE_NOTIFICATION_FILE_PATH);
        jsonObject.add(CommonValues.DATA, data);

        return jsonObject.toString();
    }
}
