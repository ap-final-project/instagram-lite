package API;

/**
 *
 * The value is used for request storage.
 * Json String, coming from client, will be converted to one of these objects.
 */
public class Requests {

    public class SignUp{
        public long requestID;
        public class data{
            public String userID;
            public String userName;
            public String email;
            public String password;
        }
    }

    public class Verify{
        public long requestID;
        public class data{
            public String email;
            public boolean result;
        }
    }

    public class Login{
        public long requestID;
        public class data{
            public String userID;
            public String password;
        }
    }

    public class ForgotPassword{
        public long requestID;
        public class data{
            public String email;
        }
    }

    public class ChangePassword{
        public long requestID;
        public class data{
            public String oldPassword;
            public String newPassword;
        }
    }

    public class ChangeEmail{
        public long requestID;
        public class data{
            public String email;
        }
    }

    public class EditProfilePic{
        public long requestID;
        public class data{
            public String fileName;
        }
    }

    public class RemoveProfilePic{
        public long requestID;
        public class data{}
    }

    public class EditUserName{
        public long requestID;
        public class data{
            public String userName;
        }
    }

    public class EditBio{
        public long requestID;
        public class data{
            public String bio;
        }
    }

    public class Follow{
        public long requestID;
        public class data{
            public String userID;
        }
    }

    public class GetFollowersList{
        public long requestID;
        public class data{
            public String userID;
            public String last;
        }
    }

    public class GetFollowingList{
        public long requestID;
        public class data{
            public String userID;
            public String last;
        }
    }

    public class Unfollow{
        public long requestID;
        public class data{
            public String userID;
        }
    }

    public class GetMainUserProfile{
        public long requestID;
        public class data{}
    }

    public class GetActivities{
        public long requestID;
        public class data{
            public String last;
        }
    }

    public class GetTimeline{
        public long requestID;
        public class data{
            public String last;
        }
    }

    public class GetUserProfile{
        public long requestID;
        public class data{
            public String userID;
        }
    }

    public class GetMorePost{
        public long requestID;
        public class data{
            public String userID;
            public String last;
        }
    }

    public class Post{
        public long requestID;
        public class data{
            public String fileName;
            public String caption;
        }
    }

    public class DeletePost{
        public long requestID;
        public class data{
            public String postID;
        }
    }

    public class Like{
        public long requestID;
        public class data{
            public String postID;
            public String otherUserID;
        }
    }

    public class Unlike{
        public long requestID;
        public class data{
            public String postID;
        }
    }

    public class GetLikeList{
        public long requestID;
        public class data{
            public String postID;
            public String last;
        }
    }

    public class Comment{
        public long requestID;
        public class data{
            public String postID;
            public String otherUserID;
            public String repliedTo;
            public String comment;
        }
    }

    public class DeleteComment{
        public long requestID;
        public class data{
            public String commentID;
        }
    }

    public class GetCommentList{
        public long requestID;
        public class data{
            public String postID;
            public String last;
        }
    }

    public class GetChatList{
        public long requestID;
        public class data{}
    }

    public class SendDirectMessage{
        public long requestID;
        public class data{
            public String receiver;
            public String type;
            public String content;
        }
    }

    public class GetDirectMessages{
        public long requestID;
        public class data{
            public String chatID;
            public int length;
        }
    }

    public class Logout{
        public long requestID;
        public class data{}
    }

    public class DeleteAccount{
        public long requestID;
        public class data{
            public String password;
        }
    }

}
