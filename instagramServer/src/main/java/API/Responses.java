package API;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;
import org.bson.Document;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;


/**
 * Includes methods to create Json String response.
 */

public class Responses {
    private final long requestID;
    private final boolean result;
    
    public Responses(long requestID, boolean result){
        this.requestID = requestID;
        this.result = result;
    }

    public JsonObject getJsonObject(String path) {

        JsonObject jsonObject = new JsonObject();
        try (FileReader reader = new FileReader(path)) {
            Gson gson = new Gson();
            JsonReader jsonReader = new JsonReader(reader);
            jsonObject = gson.fromJson(jsonReader, JsonObject.class);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    public String createSignUpResponse(String message, int code) {
        JsonObject jsonObject = getJsonObject(Paths.SIGNUP_RESPONSE_FILE_PATH);
        jsonObject.addProperty(CommonValues.REQUEST_ID, requestID);
        jsonObject.addProperty(CommonValues.RESULT, result);

        JsonObject data = new JsonObject();
        data.addProperty(CommonValues.MESSAGE, message);
        data.addProperty(CommonValues.CODE, code);
        jsonObject.add(CommonValues.DATA, data);

        return jsonObject.toString();
    }

    public String createVerifyResponse(String message) {
        JsonObject jsonObject = getJsonObject(Paths.VERIFY_RESPONSE_FILE_PATH);
        jsonObject.addProperty(CommonValues.REQUEST_ID, requestID);
        jsonObject.addProperty(CommonValues.RESULT, result);

        JsonObject data = new JsonObject();
        data.addProperty(CommonValues.MESSAGE, message);
        jsonObject.add(CommonValues.DATA, data);

        return jsonObject.toString();
    }

    public String createLoginResponse(JsonObject data) {
        JsonObject jsonObject = getJsonObject(Paths.LOGIN_RESPONSE_FILE_PATH);
        jsonObject.addProperty(CommonValues.REQUEST_ID, requestID);
        jsonObject.addProperty(CommonValues.RESULT, result);
        jsonObject.add(CommonValues.DATA, data);

        return jsonObject.toString();
    }

    public String createForgotPasswordResponse(String message) {
        JsonObject jsonObject = getJsonObject(Paths.FORGOT_PASSWORD_RESPONSE_FILE_PATH);
        jsonObject.addProperty(CommonValues.REQUEST_ID, requestID);
        jsonObject.addProperty(CommonValues.RESULT, result);

        JsonObject data = new JsonObject();
        data.addProperty(CommonValues.MESSAGE, message);
        jsonObject.add(CommonValues.DATA, data);

        return jsonObject.toString();
    }

    public String createChangePasswordResponse(String message, String newPassword) {
        JsonObject jsonObject = getJsonObject(Paths.CHANGE_PASSWORD_RESPONSE_FILE_PATH);
        jsonObject.addProperty(CommonValues.REQUEST_ID, requestID);
        jsonObject.addProperty(CommonValues.RESULT, result);

        JsonObject data = new JsonObject();
        data.addProperty(CommonValues.MESSAGE, message);
        data.addProperty(CommonValues.PASSWORD, newPassword);
        jsonObject.add(CommonValues.DATA, data);

        return jsonObject.toString();
    }

    public String createChangeEmailResponse(String message, String newEmail) {
        JsonObject jsonObject = getJsonObject(Paths.CHANGE_EMAIL_RESPONSE_FILE_PATH);
        jsonObject.addProperty(CommonValues.REQUEST_ID, requestID);
        jsonObject.addProperty(CommonValues.RESULT, result);

        JsonObject data = new JsonObject();
        data.addProperty(CommonValues.MESSAGE, message);
        data.addProperty(CommonValues.EMAIL, newEmail);
        jsonObject.add(CommonValues.DATA, data);

        return jsonObject.toString();
    }

    public String createEditProfilePicResponse(String message) {
        JsonObject jsonObject = getJsonObject(Paths.EDIT_PROFILE_PIC_RESPONSE_FILE_PATH);
        jsonObject.addProperty(CommonValues.REQUEST_ID, requestID);
        jsonObject.addProperty(CommonValues.RESULT, result);

        JsonObject data = new JsonObject();
        data.addProperty(CommonValues.MESSAGE, message);
        jsonObject.add(CommonValues.DATA, data);

        return jsonObject.toString();
    }

    public String createRemoveProfilePicResponse(String message) {
        JsonObject jsonObject = getJsonObject(Paths.REMOVE_PROFILE_PIC_RESPONSE_FILE_PATH);
        jsonObject.addProperty(CommonValues.REQUEST_ID, requestID);
        jsonObject.addProperty(CommonValues.RESULT, result);

        JsonObject data = new JsonObject();
        data.addProperty(CommonValues.MESSAGE, message);
        jsonObject.add(CommonValues.DATA, data);

        return jsonObject.toString();
    }

    public String createEditUserNameResponse(String message, String userName) {
        JsonObject jsonObject = getJsonObject(Paths.EDIT_USERNAME_RESPONSE_FILE_PATH);
        jsonObject.addProperty(CommonValues.REQUEST_ID, requestID);
        jsonObject.addProperty(CommonValues.RESULT, result);

        JsonObject data = new JsonObject();
        data.addProperty(CommonValues.MESSAGE, message);
        data.addProperty(CommonValues.USERNAME, userName);
        jsonObject.add(CommonValues.DATA, data);

        return jsonObject.toString();
    }

    public String createEditBioResponse(String message, String bio) {
        JsonObject jsonObject = getJsonObject(Paths.EDIT_BIO_RESPONSE_FILE_PATH);
        jsonObject.addProperty(CommonValues.REQUEST_ID, requestID);
        jsonObject.addProperty(CommonValues.RESULT, result);

        JsonObject data = new JsonObject();
        data.addProperty(CommonValues.MESSAGE, message);
        data.addProperty(CommonValues.BIO, bio);
        jsonObject.add(CommonValues.DATA, data);

        return jsonObject.toString();
    }

    public String createFollowResponse(String message) {
        JsonObject jsonObject = getJsonObject(Paths.FOLLOW_RESPONSE_FILE_PATH);
        jsonObject.addProperty(CommonValues.REQUEST_ID, requestID);
        jsonObject.addProperty(CommonValues.RESULT, result);

        JsonObject data = new JsonObject();
        data.addProperty(CommonValues.MESSAGE, message);
        jsonObject.add(CommonValues.DATA, data);

        return jsonObject.toString();
    }

    public String createUnfollowResponse(String message) {
        JsonObject jsonObject = getJsonObject(Paths.UNFOLLOW_RESPONSE_FILE_PATH);
        jsonObject.addProperty(CommonValues.REQUEST_ID, requestID);
        jsonObject.addProperty(CommonValues.RESULT, result);

        JsonObject data = new JsonObject();
        data.addProperty(CommonValues.MESSAGE, message);
        jsonObject.add(CommonValues.DATA, data);

        return jsonObject.toString();
    }

    public String createGetFollowersListResponse(JsonObject data) {
        JsonObject jsonObject = getJsonObject(Paths.GET_FOLLOWERS_LIST_RESPONSE_FILE_PATH);
        jsonObject.addProperty(CommonValues.REQUEST_ID, requestID);
        jsonObject.addProperty(CommonValues.RESULT, result);

        jsonObject.add(CommonValues.DATA, data);

        return jsonObject.toString();
    }

    public String createGetFollowingListResponse(JsonObject data) {
        JsonObject jsonObject = getJsonObject(Paths.GET_FOLLOWING_LIST_RESPONSE_FILE_PATH);
        jsonObject.addProperty(CommonValues.REQUEST_ID, requestID);
        jsonObject.addProperty(CommonValues.RESULT, result);

        jsonObject.add(CommonValues.DATA, data);

        return jsonObject.toString();
    }

    public String createGetMainUserProfileResponse(JsonObject data) {
        JsonObject jsonObject = getJsonObject(Paths.GET_MAIN_USER_PROFILE_RESPONSE_FILE_PATH);
        jsonObject.addProperty(CommonValues.REQUEST_ID, requestID);
        jsonObject.addProperty(CommonValues.RESULT, result);
        jsonObject.add(CommonValues.DATA, data);

        return jsonObject.toString();
    }

    public String createGetActivitiesResponse(JsonObject data) {
        JsonObject jsonObject = getJsonObject(Paths.GET_ACTIVITIES_RESPONSE_FILE_PATH);
        jsonObject.addProperty(CommonValues.REQUEST_ID, requestID);
        jsonObject.addProperty(CommonValues.RESULT, result);
        jsonObject.add(CommonValues.DATA, data);

        return jsonObject.toString();
    }

    public String createGetTimelineResponse(JsonObject data) {
        JsonObject jsonObject = getJsonObject(Paths.GET_TIMELINE_RESPONSE_FILE_PATH);
        jsonObject.addProperty(CommonValues.REQUEST_ID, requestID);
        jsonObject.addProperty(CommonValues.RESULT, result);
        jsonObject.add(CommonValues.DATA, data);

        return jsonObject.toString();
    }

    public String createGetUserProfileResponse(JsonObject data) {
        JsonObject jsonObject = getJsonObject(Paths.GET_USER_PROFILE_RESPONSE_FILE_PATH);
        jsonObject.addProperty(CommonValues.REQUEST_ID, requestID);
        jsonObject.addProperty(CommonValues.RESULT, result);
        jsonObject.add(CommonValues.DATA, data);

        return jsonObject.toString();
    }

    public String createGetMorePostResponse(JsonObject data){
        JsonObject jsonObject = getJsonObject(Paths.GET_MORE_POST_RESPONSE_FILE_PATH);
        jsonObject.addProperty(CommonValues.REQUEST_ID, requestID);
        jsonObject.addProperty(CommonValues.RESULT, result);
        jsonObject.add(CommonValues.DATA, data);

        return jsonObject.toString();
    }

    public String createPostResponse(JsonObject data) {

        JsonObject jsonObject = getJsonObject(Paths.POST_RESPONSE_FILE_PATH);
        jsonObject.addProperty(CommonValues.REQUEST_ID, requestID);
        jsonObject.addProperty(CommonValues.RESULT, result);

        jsonObject.add(CommonValues.DATA, data);

        return jsonObject.toString();
    }

    public String createDeletePostResponse(String message) {
        JsonObject jsonObject = getJsonObject(Paths.DELETE_POST_RESPONSE_FILE_PATH);
        jsonObject.addProperty(CommonValues.REQUEST_ID, requestID);
        jsonObject.addProperty(CommonValues.RESULT, result);

        JsonObject data = new JsonObject();
        data.addProperty(CommonValues.MESSAGE, message);
        jsonObject.add(CommonValues.DATA, data);

        return jsonObject.toString();
    }

    public String createLikeResponse(String message) {
        JsonObject jsonObject = getJsonObject(Paths.LIKE_RESPONSE_FILE_PATH);
        jsonObject.addProperty(CommonValues.REQUEST_ID, requestID);
        jsonObject.addProperty(CommonValues.RESULT, result);

        JsonObject data = new JsonObject();
        data.addProperty(CommonValues.MESSAGE, message);
        jsonObject.add(CommonValues.DATA, data);

        return jsonObject.toString();
    }

    public String createUnlikeResponse(String message) {
        JsonObject jsonObject = getJsonObject(Paths.UNLIKE_RESPONSE_FILE_PATH);
        jsonObject.addProperty(CommonValues.REQUEST_ID, requestID);
        jsonObject.addProperty(CommonValues.RESULT, result);

        JsonObject data = new JsonObject();
        data.addProperty(CommonValues.MESSAGE, message);
        jsonObject.add(CommonValues.DATA, data);

        return jsonObject.toString();
    }

    public String createGetLikeListResponse(JsonObject data) {
        JsonObject jsonObject = getJsonObject(Paths.GET_LIKE_LIST_RESPONSE_FILE_PATH);
        jsonObject.addProperty(CommonValues.REQUEST_ID, requestID);
        jsonObject.addProperty(CommonValues.RESULT, result);

        jsonObject.add(CommonValues.DATA, data);

        return jsonObject.toString();
    }

    public String createCommentResponse(JsonObject data) {
        JsonObject jsonObject = getJsonObject(Paths.COMMENT_RESPONSE_FILE_PATH);
        jsonObject.addProperty(CommonValues.REQUEST_ID, requestID);
        jsonObject.addProperty(CommonValues.RESULT, result);

        jsonObject.add(CommonValues.DATA, data);

        return jsonObject.toString();
    }

    public String createDeleteCommentResponse(String message) {
        JsonObject jsonObject = getJsonObject(Paths.DELETE_COMMENT_RESPONSE_FILE_PATH);
        jsonObject.addProperty(CommonValues.REQUEST_ID, requestID);
        jsonObject.addProperty(CommonValues.RESULT, result);

        JsonObject data =  new JsonObject();
        data.addProperty(CommonValues.MESSAGE, message);
        jsonObject.add(CommonValues.DATA, data);

        return jsonObject.toString();
    }

    public String createGetCommentListResponse(JsonObject data) {
        JsonObject jsonObject = getJsonObject(Paths.GET_COMMENT_LIST_RESPONSE_FILE_PATH);
        jsonObject.addProperty(CommonValues.REQUEST_ID, requestID);
        jsonObject.addProperty(CommonValues.RESULT, result);

        jsonObject.add(CommonValues.DATA, data);

        return jsonObject.toString();
    }

    public String createGetChatListResponse(JsonObject data) {
        JsonObject jsonObject = getJsonObject(Paths.GET_CHAT_LIST_RESPONSE_FILE_PATH);
        jsonObject.addProperty(CommonValues.REQUEST_ID, requestID);
        jsonObject.addProperty(CommonValues.RESULT, result);

        jsonObject.add(CommonValues.DATA, data);

        return jsonObject.toString();
    }

    public String createSendDirectMessageResponse(String message, JsonObject directMsg) {
        JsonObject jsonObject = getJsonObject(Paths.SEND_DIRECT_MESSAGE_RESPONSE_FILE_PATH);
        jsonObject.addProperty(CommonValues.REQUEST_ID, requestID);
        jsonObject.addProperty(CommonValues.RESULT, result);

        JsonObject data = new JsonObject();
        data.addProperty(CommonValues.MESSAGE, message);
        data.add(CommonValues.DIRECT_MESSAGE, directMsg);
        jsonObject.add(CommonValues.DATA, data);

        return jsonObject.toString();
    }

    public String createGetDirectMessagesResponse(JsonObject data) {
        JsonObject jsonObject = getJsonObject(Paths.GET_DIRECT_MESSAGES_RESPONSE_FILE_PATH);
        jsonObject.addProperty(CommonValues.REQUEST_ID, requestID);
        jsonObject.addProperty(CommonValues.RESULT, result);
        jsonObject.add(CommonValues.DATA, data);
        System.out.println(data);

        return jsonObject.toString();
    }

    public String createLogoutResponse(String message) {
        JsonObject jsonObject = getJsonObject(Paths.LOGOUT_RESPONSE_FILE_PATH);
        jsonObject.addProperty(CommonValues.REQUEST_ID, requestID);
        jsonObject.addProperty(CommonValues.RESULT, result);

        JsonObject data =  new JsonObject();
        data.addProperty(CommonValues.MESSAGE, message);
        jsonObject.add(CommonValues.DATA, data);

        return jsonObject.toString();
    }

    public String createDeleteAccountResponse(String message) {
        JsonObject jsonObject = getJsonObject(Paths.DELETE_ACCOUNT_RESPONSE_FILE_PATH);
        jsonObject.addProperty(CommonValues.REQUEST_ID, requestID);
        jsonObject.addProperty(CommonValues.RESULT, result);

        JsonObject data =  new JsonObject();
        data.addProperty(CommonValues.MESSAGE, message);
        jsonObject.add(CommonValues.DATA, data);

        return jsonObject.toString();
    }

    public String createInvalidRequestResponse() {
        JsonObject jsonObject = getJsonObject(Paths.INVALID_REQUEST_RESPONSE_FILE_PATH);
        jsonObject.addProperty(CommonValues.REQUEST_ID, requestID);

        return jsonObject.toString();
    }

}
