import API.CommonValues;

import socket.*;

import java.io.IOException;


public class App {
    public static void main(String[] args) {
        MainServer server = new MainServer(CommonValues.MAIN_PORT);
        server.execute();
    }
}
