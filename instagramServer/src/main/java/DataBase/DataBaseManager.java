package DataBase;

import API.CommonValues;
import API.Messages;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mongodb.client.*;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import fileHandling.FileManager;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;


public class DataBaseManager {

    public final ZoneId zone = ZoneId.of("Asia/Tehran");

    public MongoDatabase database;

    public MongoCollection<Document> inactiveUsersCollection;
    public MongoCollection<Document> activeUsersCollection;
    private final MongoCollection<Document> postsCollection;
    private final MongoCollection<Document> chatsCollection;
    private final MongoCollection<Document> commentsCollection;
    private final MongoCollection<Document> activitiesCollection;


    public DataBaseManager() {
        MongoClient client = MongoClients.create("mongodb://localhost:27017");
        database = client.getDatabase(CommonValues.DATABASE_NAME);
        inactiveUsersCollection = database.getCollection(CommonValues.INACTIVE_COLLECTION);
        activeUsersCollection = database.getCollection(CommonValues.USERS_COLLECTION);
        postsCollection = database.getCollection(CommonValues.POSTS_COLLECTION);
        chatsCollection = database.getCollection(CommonValues.CHATS_COLLECTION);
        commentsCollection = database.getCollection(CommonValues.COMMENTS_COLLECTION);
        activitiesCollection = database.getCollection(CommonValues.ACTIVITIES_COLLECTION);
    }

    public JsonObject followNotification(String fromUserID, String toUserID) {

        Document response = new Document()
                .append(CommonValues.DATE, ZonedDateTime.now(zone)
                        .format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")))
                .append(CommonValues.NOTIFICATION, CommonValues.FOLLOW)
                .append(CommonValues.USER_ID, toUserID)
                .append(CommonValues.OTHER_USER_ID, fromUserID);

        activitiesCollection.insertOne(response);
        response.remove(CommonValues.ID);

        String profilePic = getUserProfilePic(fromUserID);

        ArrayList<String> files = new ArrayList<>();
        if (profilePic != null) {
            files.add(profilePic);
        }
        response.append(CommonValues.PROFILE_PIC, profilePic)
                .append(CommonValues.FILE_NAMES, files)
                .append(CommonValues.FILES_NUM, files.size());

        return bsonDocToGsonJsonObj(response);
    }

    public JsonObject likeNotification(String fromUserID, String toUserID, String postID) {

        String postFile = getPostMedia(postID);

        Document response = new Document()
                .append(CommonValues.DATE, ZonedDateTime.now()
                        .format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")))
                .append(CommonValues.NOTIFICATION, CommonValues.LIKE)
                .append(CommonValues.POST_ID, postID)
                .append(CommonValues.USER_ID, toUserID)
                .append(CommonValues.OTHER_USER_ID, fromUserID)
                .append(CommonValues.MEDIA, postFile);

        activitiesCollection.insertOne(response);
        response.remove(CommonValues.ID);

        String profilePic = getUserProfilePic(fromUserID);

        ArrayList<String> files = new ArrayList<>();
        files.add(postFile);
        if (profilePic != null) {
            files.add(profilePic);
        }
        response.append(CommonValues.PROFILE_PIC, profilePic)
                .append(CommonValues.FILE_NAMES, files)
                .append(CommonValues.FILES_NUM, files.size());

        return bsonDocToGsonJsonObj(response);
    }

    public JsonObject commentNotification(String fromUserID, String toUserID, String postID
            , String repliedTo, String comment, String commentID) {

        String postFile = getPostMedia(postID);

        Document response = new Document()
                .append(CommonValues.DATE, ZonedDateTime.now(zone)
                        .format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")))
                .append(CommonValues.NOTIFICATION, CommonValues.COMMENT)
                .append(CommonValues.POST_ID, postID)
                .append(CommonValues.USER_ID, toUserID)
                .append(CommonValues.OTHER_USER_ID, fromUserID)
                .append(CommonValues.MEDIA, postFile)
                .append(CommonValues.COMMENT, comment)
                .append(CommonValues.COMMENT_ID, commentID)
                .append(CommonValues.REPLIED_TO, repliedTo);

        activitiesCollection.insertOne(response);
        response.remove(CommonValues.ID);

        String profilePic = getUserProfilePic(fromUserID);

        ArrayList<String> files = new ArrayList<>();
        files.add(postFile);
        if (profilePic != null) {
            files.add(profilePic);
        }

        response.append(CommonValues.PROFILE_PIC, profilePic)
                .append(CommonValues.FILE_NAMES, files)
                .append(CommonValues.FILES_NUM, files.size());

        return bsonDocToGsonJsonObj(response);
    }

    public JsonObject mentionOnCommentNotification(String fromUserID, String toUserID, String postID
            , String comment, String commentID) {

        String postFile = getPostMedia(postID);

        Document response = new Document()
                .append(CommonValues.DATE, ZonedDateTime.now(zone)
                        .format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")))
                .append(CommonValues.NOTIFICATION, CommonValues.MENTION_ON_COMMENT)
                .append(CommonValues.POST_ID, postID)
                .append(CommonValues.USER_ID, toUserID)
                .append(CommonValues.OTHER_USER_ID, fromUserID)
                .append(CommonValues.MEDIA, postFile)
                .append(CommonValues.COMMENT_ID, commentID)
                .append(CommonValues.COMMENT, comment);

        activitiesCollection.insertOne(response);
        response.remove(CommonValues.ID);

        String profilePic = getUserProfilePic(fromUserID);

        ArrayList<String> files = new ArrayList<>();
        files.add(postFile);
        if (profilePic != null) {
            files.add(profilePic);
        }

        response.append(CommonValues.PROFILE_PIC, profilePic)
                .append(CommonValues.FILE_NAMES, files)
                .append(CommonValues.FILES_NUM, files.size());

        return bsonDocToGsonJsonObj(response);
    }

    public JsonObject directMessageNotification(String sender, String receiver, JsonObject message) {
        Document doc = getUserDocument(receiver);
        if (doc != null) {
            String profilePic = getUserProfilePic(sender);

            Document response = new Document()
                    .append(CommonValues.SENDER, sender)
                    .append(CommonValues.MESSAGE, message.toString())
                    .append(CommonValues.PROFILE_PIC, profilePic);

            return bsonDocToGsonJsonObj(response);
        } else return null;
    }


    public String updateProfilePic(String userID, String fileName) {
        try {
            Document doc = getUserDocument(userID);
            String newFileName = FileManager.rename(CommonValues.SERVER_SIDE_FILE_DIR, fileName
                    , doc.get(CommonValues.ID).toString());
            activeUsersCollection.updateOne(Filters.eq(CommonValues.USER_ID, userID)
                    , Updates.set(CommonValues.PROFILE_PIC, newFileName));

            return Messages.EDIT_PROFILE_PIC_SUCCESSFUL;
        } catch (Exception e) {
            FileManager.deleteFile(CommonValues.SERVER_SIDE_FILE_DIR, fileName);
            System.err.println(e.getMessage());
            return Messages.SOMETHING_WENT_WRONG;
        }
    }

    public String removeProfilePic(String userID) {
        try {
            String profilePic = getUserProfilePic(userID);
            activeUsersCollection.updateOne(Filters.eq(CommonValues.USER_ID, userID)
                    , Updates.set(CommonValues.PROFILE_PIC, null));
            FileManager.deleteFile(CommonValues.SERVER_SIDE_FILE_DIR, profilePic);

            return Messages.REMOVE_PROFILE_PIC_SUCCESSFUL;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return Messages.SOMETHING_WENT_WRONG;
        }
    }

    public String updateUserName(String userID, String userName) {
        try {
            activeUsersCollection.updateOne(Filters.eq(CommonValues.USER_ID, userID)
                    , Updates.set(CommonValues.USERNAME, userName));
            return Messages.EDIT_USERNAME_SUCCESSFUL;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return Messages.SOMETHING_WENT_WRONG;
        }
    }

    public String updateBio(String userID, String bio) {
        try {
            activeUsersCollection.updateOne(Filters.eq(CommonValues.USER_ID, userID)
                    , Updates.set(CommonValues.BIO, bio));
            return Messages.EDIT_BIO_SUCCESSFUL;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return Messages.SOMETHING_WENT_WRONG;
        }
    }

    public String follow(String userID, String otherUserID) {
        try {
            ArrayList<String> following = getFollowingIDs(userID);

            if (!following.contains(otherUserID)) {
                following.add(otherUserID);
                activeUsersCollection.updateOne(Filters.eq(CommonValues.USER_ID, userID)
                        , Updates.set(CommonValues.FOLLOWING, following));

                ArrayList<String> followers = getFollowersIDs(otherUserID);
                followers.add(userID);
                activeUsersCollection.updateOne(Filters.eq(CommonValues.USER_ID, otherUserID)
                        , Updates.set(CommonValues.FOLLOWERS, followers));

            }
            return Messages.FOLLOWED_SUCCESSFULLY;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return Messages.SOMETHING_WENT_WRONG;
        }
    }

    public String unfollow(String userID, String otherUserID) {
        try {
            ArrayList<String> following = getFollowingIDs(userID);
            following.remove(otherUserID);
            activeUsersCollection.updateOne(Filters.eq(CommonValues.USER_ID, userID)
                    , Updates.set(CommonValues.FOLLOWING, following));

            ArrayList<String> followers = getFollowersIDs(otherUserID);
            followers.remove(userID);
            activeUsersCollection.updateOne(Filters.eq(CommonValues.USER_ID, otherUserID)
                    , Updates.set(CommonValues.FOLLOWERS, followers));

            activitiesCollection.deleteOne(Filters.and(Filters.eq(CommonValues.USER_ID, otherUserID)
                    , Filters.eq(CommonValues.OTHER_USER_ID, userID)));
            return Messages.UNFOLLOWED_SUCCESSFULLY;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return Messages.SOMETHING_WENT_WRONG;
        }
    }

    public JsonObject getFollowersList(String userID, String otherUserID, String last) {
        Document response = new Document();
        try {
            ArrayList<String> followerIDs = getFollowersIDs(otherUserID);
            ArrayList<String> followers = new ArrayList<>();
            ArrayList<String> files = new ArrayList<>();
            List<String> subList;
            int indexOfFirst = 0;
            if (followerIDs != null) {
                if (last != null && last.length() > 0) {
                    indexOfFirst = followerIDs.indexOf(last) + 1;
                }

                if (12 <= followerIDs.size() - indexOfFirst) {
                    subList = followerIDs.subList(indexOfFirst, indexOfFirst + 12);
                } else {
                    subList = followerIDs.subList(indexOfFirst, followerIDs.size());
                }
                boolean followed;
                ArrayList<String> mainUserFollowing = getFollowingIDs(userID);
                mainUserFollowing.add(userID);

                for (String id : subList) {
                    Document doc = getUserDocument(id);
                    if (doc != null) {
                        JsonObject user = new JsonObject();
                        followed = mainUserFollowing.contains(id);
                        user.addProperty(CommonValues.USER_ID, id);
                        String profilePic = doc.getString(CommonValues.PROFILE_PIC);
                        user.addProperty(CommonValues.PROFILE_PIC, profilePic);
                        user.addProperty(CommonValues.FOLLOWED, followed);
                        followers.add(user.toString());
                        if (profilePic != null && profilePic.length() > 0) {
                            files.add(profilePic);
                        }
                    }
                }
                response.append(CommonValues.MESSAGE, Messages.FOLLOWERS_LIST_GET_SUCCESSFULLY)
                        .append(CommonValues.FILE_NAMES, files)
                        .append(CommonValues.FOLLOWERS, followers);
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            response.append(CommonValues.MESSAGE, Messages.SOMETHING_WENT_WRONG);
        }
        return bsonDocToGsonJsonObj(response);
    }

    public JsonObject getFollowingList(String userID, String otherUserID, String last) {
        Document response = new Document();
        try {
            ArrayList<String> followingIDs = getFollowingIDs(otherUserID);
            ArrayList<String> following = new ArrayList<>();
            ArrayList<String> files = new ArrayList<>();
            List<String> subList;
            int indexOfFirst = 0;
            if (followingIDs != null) {
                if (last != null && last.length() > 0) {
                    indexOfFirst = followingIDs.indexOf(last) + 1;
                }

                if (12 <= followingIDs.size() - indexOfFirst) {
                    subList = followingIDs.subList(indexOfFirst, indexOfFirst + 12);
                } else {
                    subList = followingIDs.subList(indexOfFirst, followingIDs.size());
                }
                boolean followed;
                ArrayList<String> mainUserFollowing = getFollowingIDs(userID);
                mainUserFollowing.add(userID);

                for (String id : subList) {
                    Document doc = getUserDocument(id);
                    if (doc != null) {
                        JsonObject user = new JsonObject();
                        followed = mainUserFollowing.contains(id);
                        user.addProperty(CommonValues.USER_ID, id);
                        String profilePic = doc.getString(CommonValues.PROFILE_PIC);
                        user.addProperty(CommonValues.PROFILE_PIC, profilePic);
                        user.addProperty(CommonValues.FOLLOWED, followed);
                        following.add(user.toString());
                        if (profilePic != null && profilePic.length() > 0) {
                            files.add(profilePic);
                        }
                    }
                }
                response.append(CommonValues.MESSAGE, Messages.FOLLOWING_LIST_GET_SUCCESSFULLY)
                        .append(CommonValues.FILE_NAMES, files)
                        .append(CommonValues.FOLLOWING, following);
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            response.append(CommonValues.MESSAGE, Messages.SOMETHING_WENT_WRONG);
        }
        return bsonDocToGsonJsonObj(response);
    }

    public JsonObject getMainUserProfile(String userID) {
        Document userDoc = getUserDocument(userID);
        Document response = new Document();
        try {
            String profilePic = getUserProfilePic(userID);
            response.append(CommonValues.USER_ID, userID)
                    .append(CommonValues.EMAIL, userDoc.getString(CommonValues.EMAIL))
                    .append(CommonValues.USERNAME, userDoc.getString(CommonValues.USERNAME))
                    .append(CommonValues.BIO, userDoc.getString(CommonValues.BIO))
                    .append(CommonValues.PROFILE_PIC, profilePic)
                    .append(CommonValues.POST_NUM, getPostsIDs(userID).size())
                    .append(CommonValues.FOLLOWERS_NUM, getFollowersIDs(userID).size())
                    .append(CommonValues.FOLLOWING_NUM, getFollowingIDs(userID).size());

            ArrayList<String> postsIDs = getPostsIDs(userID);
            List<String> subList;
            int indexOfLast = postsIDs.size();
            if (0 < indexOfLast - 12) {
                subList = postsIDs.subList(indexOfLast - 12, indexOfLast);
            } else {
                subList = postsIDs.subList(0, indexOfLast);
            }
            Document doc = getPostListAsJson(subList, userID);
            ArrayList<String> files = (ArrayList<String>) doc.get(CommonValues.FILE_NAMES);
            if (profilePic != null && !files.contains(profilePic)) {
                files.add(profilePic);
                response.append(CommonValues.FILE_NAMES, files);
            }
            for (String key : doc.keySet()) {
                response.append(key, doc.get(key));
            }

            response.append(CommonValues.MESSAGE, Messages.GET_SELF_PROFILE_SUCCESSFULLY);

        } catch (Exception e) {
            System.err.println(e.getMessage());
            response.append(CommonValues.MESSAGE, Messages.SOMETHING_WENT_WRONG);
        }
        return bsonDocToGsonJsonObj(response);
    }

    public JsonObject getActivities(String userID, String last) {
        Document response = new Document();
        try {
            ArrayList<String> activityIDs = getActivitiesIDs(userID);
            ArrayList<String> activities = new ArrayList<>();
            ArrayList<String> files = new ArrayList<>();
            List<String> subList;
            int indexOfLast = activityIDs.size();
            if (activityIDs != null) {
                if (last != null && last.length() > 0) {
                    indexOfLast = activityIDs.indexOf(last);
                }
                if (0 < indexOfLast - 12) {
                    subList = activityIDs.subList(indexOfLast - 12, indexOfLast);
                } else {
                    subList = activityIDs.subList(0, indexOfLast);
                }

                for (String id : subList) {
                    Document doc = getActivityDocument(id);
                    JsonObject activity = bsonDocToGsonJsonObj(doc);
                    String notif = activity.get(CommonValues.NOTIFICATION).getAsString();
                    activity.remove(CommonValues.ID);
                    activity.addProperty(CommonValues.ACTIVITY_ID, id);
                    activity.addProperty(CommonValues.USER_ID, doc.getString(CommonValues.OTHER_USER_ID));
                    activity.remove(CommonValues.OTHER_USER_ID);
                    String profilePic = getUserProfilePic(doc.getString(CommonValues.OTHER_USER_ID));
                    activity.addProperty(CommonValues.PROFILE_PIC, profilePic);
                    activities.add(activity.toString());
                    if (!notif.equals(CommonValues.FOLLOW)) {
                        String media = activity.get(CommonValues.MEDIA).getAsString();
                        if (!files.contains(media)) {
                            files.add(media);
                        }
                    }
                    if (profilePic != null && !files.contains(profilePic)) {
                        files.add(profilePic);
                    }
                }
                Collections.reverse(activities);
                response.append(CommonValues.MESSAGE, Messages.ACTIVITIES_GET_SUCCESSFULLY)
                        .append(CommonValues.FILE_NAMES, files)
                        .append(CommonValues.ACTIVITIES, activities);
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            response.append(CommonValues.MESSAGE, Messages.SOMETHING_WENT_WRONG);
        }
        return bsonDocToGsonJsonObj(response);

    }

    public JsonObject getTimeline(String userId, String last) {
        Document response = new Document();
        try {
            ArrayList<String> followingIDs = getFollowingIDs(userId);
            followingIDs.add(userId);

            FindIterable<Document> posts = postsCollection.find();
            List<String> postsIDs = new ArrayList<>();
            List<String> subList;
            int indexOfLast;

            for (Document doc : posts) {
                if (followingIDs.contains(doc.getString(CommonValues.USER_ID))) {
                    postsIDs.add(doc.get(CommonValues.ID).toString());
                }
            }
            if (postsIDs != null) {
                if (last != null && last.length() > 0) {
                    indexOfLast = postsIDs.indexOf(last);
                } else {
                    indexOfLast = postsIDs.size();
                }
                if (0 < indexOfLast - 3) {
                    subList = postsIDs.subList(indexOfLast - 3, indexOfLast);
                } else {
                    subList = postsIDs.subList(0, indexOfLast);
                }
                response = getPostListAsJson(subList, userId);
            } else {
                response.append(CommonValues.MESSAGE, Messages.POSTS_GET_SUCCESSFULLY)
                        .append(CommonValues.POSTS, Arrays.asList());
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            response.append(CommonValues.MESSAGE, Messages.SOMETHING_WENT_WRONG);
        }
        return bsonDocToGsonJsonObj(response);
    }

    public JsonObject getUserProfile(String userID, String otherUserID) {
        Document userDoc = getUserDocument(otherUserID);
        Document response = new Document();
        try {
            if (userDoc != null) {
                String profilePic = userDoc.getString(CommonValues.PROFILE_PIC);
                response.append(CommonValues.USER_ID, otherUserID)
                        .append(CommonValues.USERNAME, userDoc.getString(CommonValues.USERNAME))
                        .append(CommonValues.BIO, userDoc.getString(CommonValues.BIO))
                        .append(CommonValues.PROFILE_PIC, profilePic)
                        .append(CommonValues.POST_NUM, getPostsIDs(otherUserID).size())
                        .append(CommonValues.FOLLOWERS_NUM, getFollowersIDs(otherUserID).size())
                        .append(CommonValues.FOLLOWING_NUM, getFollowingIDs(otherUserID).size())
                        .append(CommonValues.FOLLOWED, getFollowingIDs(userID).contains(otherUserID));

                if (userID.equals(otherUserID)) {
                    response.append(CommonValues.FOLLOWED, null);
                }

                ArrayList<String> postsIDs = getPostsIDs(otherUserID);
                List<String> subList;
                int indexOfLast = postsIDs.size();
                if (0 < indexOfLast - 12) {
                    subList = postsIDs.subList(indexOfLast - 12, indexOfLast);
                } else {
                    subList = postsIDs.subList(0, indexOfLast);
                }
                Document js2 = getPostListAsJson(subList, userID);
                ArrayList<String> files = (ArrayList<String>) js2.get(CommonValues.FILE_NAMES);
                if (profilePic != null && !files.contains(profilePic)) {
                    files.add(profilePic);
                    response.append(CommonValues.FILE_NAMES, files);
                }
                for (String key : js2.keySet()) {
                    response.append(key, js2.get(key));
                }
                response.append(CommonValues.MESSAGE, Messages.GET_USER_PROFILE_SUCCESSFULLY);
            } else {
                response.append(CommonValues.MESSAGE, Messages.GET_USER_PROFILE_UNSUCCESSFUL);
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            response.append(CommonValues.MESSAGE, Messages.SOMETHING_WENT_WRONG);
        }
        return bsonDocToGsonJsonObj(response);
    }

    public JsonObject getMorePost(String userID, String otherUserID, String last) {
        Document response = new Document();
        try {
            ArrayList<String> postsIDs = getPostsIDs(otherUserID);
            List<String> subList;
            int indexOfLast;
            if (postsIDs != null) {
                if (last != null && last.length() > 0) {
                    indexOfLast = postsIDs.indexOf(last);
                } else {
                    indexOfLast = postsIDs.size();
                }
                if (0 < indexOfLast - 12) {
                    subList = postsIDs.subList(indexOfLast - 12, indexOfLast);
                } else {
                    subList = postsIDs.subList(0, indexOfLast);
                }
                response = getPostListAsJson(subList, userID);
            } else {
                response.append(CommonValues.MESSAGE, Messages.POSTS_GET_SUCCESSFULLY)
                        .append(CommonValues.POSTS, Arrays.asList());
            }

        } catch (Exception e) {
            System.err.println(e.getMessage());
            response.append(CommonValues.MESSAGE, Messages.SOMETHING_WENT_WRONG);
        }
        return bsonDocToGsonJsonObj(response);
    }

    public JsonObject addPost(String userID, String caption, String fileName) {
        JsonObject response = new JsonObject();
        try {
            Document doc = new Document().append(CommonValues.DATE, ZonedDateTime.now(zone)
                    .format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")))
                    .append(CommonValues.USER_ID, userID)
                    .append(CommonValues.CAPTION, caption)
                    .append(CommonValues.MEDIA, null)
                    .append(CommonValues.LIKES, new ArrayList<>());
            postsCollection.insertOne(doc);

            String postID = doc.get(CommonValues.ID).toString();
            ArrayList<String> postIDs = getPostsIDs(userID);
            postIDs.add(postID);
            activeUsersCollection.updateOne(Filters.eq(CommonValues.USER_ID, userID)
                    , Updates.set(CommonValues.POSTS, postIDs));
            String newFileName = FileManager.rename(CommonValues.SERVER_SIDE_FILE_DIR, fileName, postID);
            postsCollection.updateOne(Filters.eq(CommonValues.ID, new ObjectId(postID))
                    , Updates.set(CommonValues.MEDIA, newFileName));

            response.addProperty(CommonValues.POST_ID, postID);
            response.addProperty(CommonValues.MESSAGE, Messages.POSTED_SUCCESSFULLY);
            return response;
        } catch (Exception e) {
            FileManager.deleteFile(CommonValues.SERVER_SIDE_FILE_DIR, fileName);
            System.err.println(e.getMessage());
            response.addProperty(CommonValues.MESSAGE, Messages.SOMETHING_WENT_WRONG);
            return response;
        }
    }

    public String deletePost(String postID) {
        try {
            Document doc = getPostDocument(postID);
            String fileName = getPostMedia(postID);
            String userID = doc.getString(CommonValues.USER_ID);

            ArrayList<String> postIDs = getPostsIDs(userID);
            postIDs.remove(postID);
            activeUsersCollection.updateOne(Filters.eq(CommonValues.USER_ID, userID)
                    , Updates.set(CommonValues.POSTS, postIDs));

            postsCollection.deleteOne(Filters.eq(CommonValues.ID, new ObjectId(postID)));
            commentsCollection.deleteMany(Filters.eq(CommonValues.POST_ID, postID));
            activitiesCollection.deleteMany(Filters.eq(CommonValues.POST_ID, postID));

            FileManager.deleteFile(CommonValues.SERVER_SIDE_FILE_DIR, fileName);
            return Messages.POST_DELETED_SUCCESSFULLY;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return Messages.SOMETHING_WENT_WRONG;
        }
    }

    public String like(String postID, String userID) {
        try {
            Document doc = getPostDocument(postID);
            ArrayList<String> likes = getLikeList(postID);
            if (!likes.contains(userID)) {
                likes.add(userID);
                postsCollection.updateOne(Filters.eq(CommonValues.ID, new ObjectId(postID))
                        , Updates.set(CommonValues.LIKES, likes));
            }
            return Messages.LIKED_SUCCESSFULLY;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return Messages.SOMETHING_WENT_WRONG;
        }
    }

    public String unlike(String postID, String userID) {
        try {
            Document doc = getPostDocument(postID);
            ArrayList<String> likes = getLikeList(postID);
            likes.remove(userID);
            postsCollection.updateOne(Filters.eq(CommonValues.ID, new ObjectId(postID)), Updates.set(CommonValues.LIKES, likes));
            activitiesCollection.deleteOne(Filters.and(Filters.eq(CommonValues.OTHER_USER_ID, userID)
                    , Filters.eq(CommonValues.POST_ID, postID)));
            return Messages.UNLIKED_SUCCESSFULLY;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return Messages.SOMETHING_WENT_WRONG;
        }
    }

    public JsonObject getLikeList(String userID, String postID, String last) {
        Document response = new Document();
        try {
            ArrayList<String> likerIDs = getLikeList(postID);
            ArrayList<String> likers = new ArrayList<>();
            ArrayList<String> files = new ArrayList<>();
            List<String> subList;
            int indexOfFirst = 0;
            if (likerIDs != null) {
                if (last != null && last.length() > 0) {
                    indexOfFirst = likerIDs.indexOf(last) + 1;
                }

                if (12 <= likerIDs.size() - indexOfFirst) {
                    subList = likerIDs.subList(indexOfFirst, indexOfFirst + 12);
                } else {
                    subList = likerIDs.subList(indexOfFirst, likerIDs.size());
                }
                boolean followed;
                ArrayList<String> mainUserFollowing = getFollowingIDs(userID);
                mainUserFollowing.add(userID);
                for (String id : subList) {
                    Document doc = getUserDocument(id);
                    if (doc != null) {
                        JsonObject user = new JsonObject();
                        followed = mainUserFollowing.contains(id);
                        user.addProperty(CommonValues.USER_ID, doc.getString(CommonValues.USER_ID));
                        String profilePic = doc.getString(CommonValues.PROFILE_PIC);
                        user.addProperty(CommonValues.PROFILE_PIC, profilePic);
                        user.addProperty(CommonValues.FOLLOWED, followed);
                        likers.add(user.toString());
                        if (profilePic != null && profilePic.length() > 0) {
                            files.add(profilePic);
                        }
                    }
                }
                response.append(CommonValues.MESSAGE, Messages.LIKE_LIST_GET_SUCCESSFULLY)
                        .append(CommonValues.FILE_NAMES, files)
                        .append(CommonValues.LIKES, likers);
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            response.append(CommonValues.MESSAGE, Messages.SOMETHING_WENT_WRONG);
        }
        return bsonDocToGsonJsonObj(response);
    }

    public JsonObject comment(String userID, String postID, String repliedTo, String comment) {
        JsonObject response = new JsonObject();

        try {
            Document doc = new Document()
                    .append(CommonValues.DATE, ZonedDateTime.now(zone)
                            .format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")))
                    .append(CommonValues.USER_ID, userID)
                    .append(CommonValues.POST_ID, postID)
                    .append(CommonValues.REPLIED_TO, repliedTo)
                    .append(CommonValues.COMMENT, comment);

            commentsCollection.insertOne(doc);

            response.addProperty(CommonValues.MESSAGE, Messages.COMMENTED_SUCCESSFULLY);
            response.addProperty(CommonValues.COMMENT_ID, doc.get(CommonValues.ID).toString());
            return response;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            response.addProperty(CommonValues.MESSAGE, Messages.SOMETHING_WENT_WRONG);
            return response;
        }
    }

    public String deleteComment(String commentID) {
        try {
            commentsCollection.deleteOne(Filters.eq(CommonValues.ID, new ObjectId((commentID))));
            activitiesCollection.deleteMany(Filters.eq(CommonValues.COMMENT_ID, commentID));

            return Messages.COMMENT_DELETED_SUCCESSFULLY;
        } catch (Exception e) {
            return Messages.SOMETHING_WENT_WRONG;
        }
    }

    public JsonObject getCommentList(String postID, String last) {
        Document response = new Document();
        try {
            ArrayList<String> commentIDS = getComments(postID);
            ArrayList<String> comments = new ArrayList<>();
            ArrayList<String> files = new ArrayList<>();
            List<String> subList;
            int indexOfFirst = 0;
            if (commentIDS != null) {
                if (last != null && last.length() > 0) {
                    indexOfFirst = commentIDS.indexOf(last) + 1;
                }

                if (12 <= commentIDS.size() - indexOfFirst) {
                    subList = commentIDS.subList(indexOfFirst, indexOfFirst + 12);
                } else {
                    subList = commentIDS.subList(indexOfFirst, commentIDS.size());
                }

                for (String id : subList) {
                    Document doc = getCommentDocument(id);
                    if (doc != null) {
                        JsonObject comment = bsonDocToGsonJsonObj(doc);
                        comment.remove(CommonValues.ID);
                        comment.addProperty(CommonValues.COMMENT_ID, id);
                        comment.addProperty(CommonValues.USER_ID, doc.getString(CommonValues.USER_ID));
                        String profilePic = getUserProfilePic(doc.getString(CommonValues.USER_ID));
                        comment.addProperty(CommonValues.PROFILE_PIC, profilePic);

                        comments.add(comment.toString());
                        if (profilePic != null && profilePic.length() > 0 && !files.contains(profilePic)) {
                            files.add(profilePic);
                        }
                    }
                }
                response.append(CommonValues.MESSAGE, Messages.GET_COMMENT_LIST_SUCCESSFULLY)
                        .append(CommonValues.FILE_NAMES, files)
                        .append(CommonValues.COMMENTS, comments);
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            response.append(CommonValues.MESSAGE, Messages.SOMETHING_WENT_WRONG);
        }
        return bsonDocToGsonJsonObj(response);
    }

    public JsonObject getChatList(String userID) {
        Document response = new Document();
        try {
            ArrayList<String> chatIDs = getChatsIDs(userID);
            ArrayList<String> chats = new ArrayList<>();
            ArrayList<String> files = new ArrayList<>();
            for (String chat : chatIDs) {
                JsonObject js = new JsonObject();
                String secondParticipant = getOtherParticipantInChat(userID, chat);
                String profilePic = getUserProfilePic(secondParticipant);
                js.addProperty(CommonValues.CHAT_ID, chat);
                js.addProperty(CommonValues.USER_ID, secondParticipant);
                js.addProperty(CommonValues.LAST_MESSAGE, getLastDirectMessage(chat));
                js.addProperty(CommonValues.PROFILE_PIC, profilePic);
                chats.add(js.toString());
                if (profilePic != null) {
                    files.add(profilePic);
                }
            }
            response.append(CommonValues.CHATS, chats)
                    .append(CommonValues.FILE_NAMES, files)
                    .append(CommonValues.MESSAGE, Messages.CHAT_LIST_GET_SUCCESSFULLY);
            return bsonDocToGsonJsonObj(response);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            response.append(CommonValues.MESSAGE, Messages.SOMETHING_WENT_WRONG);
            return bsonDocToGsonJsonObj(response);
        }
    }

    public JsonObject sendDirectMessage(String userID, String receiver, String directMsg) {
        JsonObject response = new JsonObject();
        try {
            String chatID;
            if (userID.compareTo(receiver) > 0) {
                chatID = receiver + "#" + userID;
            } else {
                chatID = userID + "#" + receiver;
            }
            ArrayList<String> chatsIDs = getChatsIDs(userID);
            chatsIDs.remove(chatID);
            chatsIDs.add(chatID);
            activeUsersCollection.updateOne(Filters.eq(CommonValues.USER_ID, userID)
                    , Updates.set(CommonValues.CHATS, chatsIDs));

            chatsIDs = getChatsIDs(receiver);
            chatsIDs.remove(chatID);
            chatsIDs.add(chatID);
            activeUsersCollection.updateOne(Filters.eq(CommonValues.USER_ID, receiver)
                    , Updates.set(CommonValues.CHATS, chatsIDs));

            ArrayList<String> directMsgs = getDirectMessages(chatID, userID, receiver);
            directMsgs.add(directMsg);

            chatsCollection.updateOne(Filters.eq(CommonValues.CHAT_ID, chatID)
                    , Updates.set(CommonValues.DIRECT_MESSAGES, directMsgs));

            response.addProperty(CommonValues.CHAT_ID, chatID);
            response.addProperty(CommonValues.MESSAGE, Messages.DIRECT_SENT_SUCCESSFULLY);
            return response;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            response.addProperty(CommonValues.MESSAGE, Messages.SOMETHING_WENT_WRONG);
            return response;
        }
    }

    public JsonObject getDirectMessages(String chatID, int length) {
        Document response = new Document();
        try {
            ArrayList<String> msgs = getDirectMessages(chatID);

            List<String> subList = new LinkedList<>();
            if (msgs != null) {
                int size = msgs.size();
                if (size - length >= 50) {
                    subList = msgs.subList(size - length - 50, size - length);
                } else {
                    subList = msgs.subList(0, size - length);
                }
            }
            response.append(CommonValues.MESSAGE, Messages.DIRECT_GET_SUCCESSFULLY + length)
                    .append(CommonValues.DIRECT_MESSAGES, subList);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            response.append(CommonValues.MESSAGE, Messages.SOMETHING_WENT_WRONG);
        }
        return bsonDocToGsonJsonObj(response);
    }

    public String deleteAccount(String userID, String password) {
        try {
            Document userDoc = getUserDocument(userID);
            if (Registry.get_SHA_512_SecurePassword(password).equals(userDoc.getString(CommonValues.PASSWORD))) {
                FindIterable<Document> docs = postsCollection.find(Filters.eq(CommonValues.USER_ID, userID));
                for (Document doc : docs) {
                    FileManager.deleteFile(CommonValues.SERVER_SIDE_FILE_DIR, doc.getString(CommonValues.MEDIA));
                }
                FileManager.deleteFile(CommonValues.SERVER_SIDE_FILE_DIR, userDoc.getString(CommonValues.PROFILE_PIC));

                ArrayList<String> followers = getFollowersIDs(userID);
                for (String user : followers) {
                    ArrayList<String> following = getFollowingIDs(user);
                    following.remove(userID);
                    activeUsersCollection.updateOne(Filters.eq(CommonValues.USER_ID, user)
                            , Updates.set(CommonValues.FOLLOWING, following));
                }

                ArrayList<String> following = getFollowingIDs(userID);
                for (String user : following) {
                    followers = getFollowersIDs(user);
                    followers.remove(userID);
                    activeUsersCollection.updateOne(Filters.eq(CommonValues.USER_ID, user)
                            , Updates.set(CommonValues.FOLLOWERS, followers));
                }

                ArrayList<String> chats = getChatsIDs(userID);
                for (String chat : chats) {
                    String secondParticipant = getOtherParticipantInChat(userID, chat);
                    ArrayList<String> secondParticipantChats = getChatsIDs(secondParticipant);
                    secondParticipantChats.remove(chat);
                    activeUsersCollection.updateOne(Filters.eq(CommonValues.USER_ID, secondParticipant)
                            , Updates.set(CommonValues.CHATS, secondParticipantChats));
                    chatsCollection.deleteOne(Filters.eq(CommonValues.CHAT_ID, chat));
                }
                ArrayList<String> postIDs = getPostsIDs(userID);
                for (String postID : postIDs) {
                    commentsCollection.deleteMany(Filters.eq(CommonValues.POST_ID, postID));
                }

                activeUsersCollection.deleteOne(Filters.eq(CommonValues.USER_ID, userID));
                postsCollection.deleteMany(Filters.eq(CommonValues.USER_ID, userID));
                commentsCollection.deleteMany(Filters.eq(CommonValues.USER_ID, userID));
                activitiesCollection.deleteMany(Filters.or(Filters.eq(CommonValues.USER_ID, userID)
                        , Filters.eq(CommonValues.OTHER_USER_ID, userID)));

                return Messages.DELETE_ACCOUNT_SUCCESSFUL;
            } else {
                return Messages.WRONG_PASSWORD;
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return Messages.SOMETHING_WENT_WRONG;
        }
    }


    public ArrayList<String> getFollowersIDs(String userID) {
        Document doc = getUserDocument(userID);
        return (ArrayList<String>) doc.get(CommonValues.FOLLOWERS);
    }

    public ArrayList<String> getFollowingIDs(String userID) {
        Document doc = getUserDocument(userID);
        return (ArrayList<String>) doc.get(CommonValues.FOLLOWING);
    }

    public ArrayList<String> getPostsIDs(String userID) {
        Document doc = getUserDocument(userID);
        return (ArrayList<String>) doc.get(CommonValues.POSTS);
    }

    private ArrayList<String> getDirectMessages(String chatID) {
        Document doc = getChatDocument(chatID);
        if(doc != null) {
            return (ArrayList<String>) doc.get(CommonValues.DIRECT_MESSAGES);
        }
        return new ArrayList<String>();
    }

    private ArrayList<String> getDirectMessages(String chatID, String user1, String user2) {
        Document doc = getChatDocument(chatID);
        if (doc == null) {
            chatsCollection.insertOne(new Document()
                    .append(CommonValues.CHAT_ID, chatID)
                    .append(CommonValues.PARTICIPANTS, Arrays.asList(user1, user2)));
            return new ArrayList<String>();
        }
        if (doc.get(CommonValues.DIRECT_MESSAGES) == null) {
            return new ArrayList<String>();
        }
        return (ArrayList<String>) doc.get(CommonValues.DIRECT_MESSAGES);
    }

    private ArrayList<String> getLikeList(String postID) {
        Document doc = getPostDocument(postID);
        ArrayList<String> likes = (ArrayList<String>) doc.get(CommonValues.LIKES);
        if (likes != null) {
            return likes;
        }
        return new ArrayList<String>();
    }

    private ArrayList<String> getComments(String postID) {
        FindIterable<Document> docs = commentsCollection.find(Filters.eq(CommonValues.POST_ID, postID));
        ArrayList<String> IDs = new ArrayList<>();
        for (Document doc : docs) {
            IDs.add(doc.get(CommonValues.ID).toString());
        }
        return IDs;
    }

    private ArrayList<String> getActivitiesIDs(String userID) {
        FindIterable<Document> docs = activitiesCollection.find(Filters.eq(CommonValues.USER_ID, userID));
        ArrayList<String> IDs = new ArrayList<>();
        for (Document doc : docs) {
            IDs.add(doc.get(CommonValues.ID).toString());
        }
        return IDs;
    }

    private ArrayList<String> getChatsIDs(String userID) {
        Document doc = getUserDocument(userID);
        if (doc == null || doc.get(CommonValues.CHATS) == null) {
            return new ArrayList<String>();
        }
        return (ArrayList<String>) doc.get(CommonValues.CHATS);
    }

    private ArrayList<String> getParticipants(String chatID) {
        return (ArrayList<String>) getChatDocument(chatID).get(CommonValues.PARTICIPANTS);
    }

    private String getUserProfilePic(String userID) {
        return getUserDocument(userID).getString(CommonValues.PROFILE_PIC);
    }

    private String getPostMedia(String postID) {
        return getPostDocument(postID).getString(CommonValues.MEDIA);
    }

    private String getOtherParticipantInChat(String userID, String chatID) {
        ArrayList<String> participants = getParticipants(chatID);
        participants.remove(userID);
        return participants.get(0);
    }

    private String getLastDirectMessage(String chatID) {
        ArrayList<String> msgs = getDirectMessages(chatID);
        JsonObject msg = new JsonParser().parse(msgs.get(msgs.size() - 1)).getAsJsonObject();
        return msg.get(CommonValues.CONTENT).getAsString();
    }

    private Document getUserDocument(String userID) {
        return activeUsersCollection.find(Filters.eq(CommonValues.USER_ID, userID)).first();
    }

    private Document getPostDocument(String postID) {
        return postsCollection.find(Filters.eq(CommonValues.ID, new ObjectId(postID))).first();
    }

    private Document getChatDocument(String chatID) {
        return chatsCollection.find(Filters.eq(CommonValues.CHAT_ID, chatID)).first();
    }

    private Document getActivityDocument(String activityID) {
        return activitiesCollection.find(Filters.eq(CommonValues.ID, new ObjectId(activityID))).first();
    }

    private Document getCommentDocument(String commentID) {
        return commentsCollection.find(Filters.eq(CommonValues.ID, new ObjectId(commentID))).first();
    }

    private Document getPostListAsJson(List<String> subList, String userID) {
        Document js = new Document();
        Collections.reverse(subList);
        ArrayList<String> posts = new ArrayList<>();
        ArrayList<String> files = new ArrayList<>();
        for (String id : subList) {
            Document doc = getPostDocument(id);
            String profile = getUserProfilePic(doc.getString(CommonValues.USER_ID));
            doc.put(CommonValues.POST_ID, id);
            doc.remove(CommonValues.ID);
            doc.put(CommonValues.LIKES, getLikeList(id).size());
            doc.put(CommonValues.LIKED, getLikeList(id).contains(userID));
            doc.put(CommonValues.PROFILE_PIC, profile);
            posts.add(doc.toJson());
            if (profile != null && profile.length() > 0 && !files.contains(profile)) {
                files.add(profile);
            }
            files.add(doc.getString(CommonValues.MEDIA));
        }
        js.append(CommonValues.MESSAGE, Messages.POSTS_GET_SUCCESSFULLY)
                .append(CommonValues.POSTS, posts)
                .append(CommonValues.FILE_NAMES, files);

        return js;
    }

    private JsonObject bsonDocToGsonJsonObj(Document doc) {
        String docStr = doc.toJson();
        JsonParser jsonParser = new JsonParser();
        JsonObject js = (JsonObject) jsonParser.parse(docStr);
        return js;
    }
}
