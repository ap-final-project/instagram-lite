package DataBase;

import API.CommonValues;
import API.Messages;
import com.google.gson.JsonObject;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import org.bson.Document;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

public class Registry {

    private DataBaseManager db;
    private String signUpMessages;

    public Registry() {
        db = new DataBaseManager();
    }

    /**
     *
     * saves user in inactive user collection until they verify their email address
     */
    public String signUp(String userID, String userName, String email, String password) {

        try {
            if (isUnique(userID, email)) {
                db.inactiveUsersCollection.insertOne(new Document()
                        .append(CommonValues.EMAIL, email)
                        .append(CommonValues.USERNAME, userName)
                        .append(CommonValues.USER_ID, userID)
                        .append(CommonValues.PASSWORD, get_SHA_512_SecurePassword(password)));

                return Messages.SIGNUP_SUCCESSFUL;
            }
            return signUpMessages; //comes from isUnique
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return Messages.SOMETHING_WENT_WRONG;
        }
    }

    public String verifyAccount(String email, boolean result) {
        try {
            Document doc = db.inactiveUsersCollection.find(Filters.eq(CommonValues.EMAIL, email)).first();
            db.inactiveUsersCollection.deleteOne(doc);

            if (result) {
                doc.append(CommonValues.POSTS, new ArrayList<Long>())
                        .append(CommonValues.FOLLOWERS, new ArrayList<String>())
                        .append(CommonValues.FOLLOWING, new ArrayList<String>())
                        .append(CommonValues.CHATS, new ArrayList<String>())
                        .append(CommonValues.PROFILE_PIC, null)
                        .append(CommonValues.BIO, null);
                db.activeUsersCollection.insertOne(doc);
                return Messages.VERIFY_SUCCESSFUL;
            }

            return Messages.VERIFY_UNSUCCESSFUL;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return Messages.SOMETHING_WENT_WRONG;
        }

    }

    public String login(String userID, String password)  {
        try {
            Document doc_u = db.database.getCollection(CommonValues.USERS_COLLECTION)
                    .find(Filters.eq(CommonValues.USER_ID, userID)).first();

            if (doc_u != null && checkPassword(doc_u, get_SHA_512_SecurePassword(password))) {
                return Messages.LOGIN_SUCCESSFUL;
            } else {
                return Messages.LOGIN_UNSUCCESSFUL;
            }
        }catch (Exception e){
            System.err.println(e.getMessage());
            return Messages.SOMETHING_WENT_WRONG;
        }
    }

    public String recoverPassword(String email, int newPassword) {
        try {
            Document doc = db.database.getCollection(CommonValues.USERS_COLLECTION)
                    .find(Filters.eq(CommonValues.EMAIL, email)).first();
            if (doc == null) {
                return Messages.FORGOT_PASS_ERROR;
            }
            db.activeUsersCollection.updateOne(doc
                    , Updates.set(CommonValues.PASSWORD, get_SHA_512_SecurePassword(String.valueOf(newPassword))));
            return Messages.FORGOT_PASS_SUCCESSFUL;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return Messages.SOMETHING_WENT_WRONG;
        }
    }

    public String changePassword(String userID, String oldPassword, String newPassword) {
        try {
            Document doc = db.database.getCollection(CommonValues.USERS_COLLECTION)
                    .find(Filters.eq(CommonValues.USER_ID, userID)).first();
            if (checkPassword(doc, get_SHA_512_SecurePassword(oldPassword))) {
                db.activeUsersCollection.updateOne(doc
                        , Updates.set(CommonValues.PASSWORD, get_SHA_512_SecurePassword(newPassword)));
                return Messages.CHANGE_PASS_SUCCESSFUL;
            }
            return Messages.CHANGE_PASS_INCORRECT;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return Messages.SOMETHING_WENT_WRONG;
        }
    }

    public String changeEmail(String userID, String email) {
        try {
            Document doc = db.database.getCollection(CommonValues.USERS_COLLECTION)
                    .find(Filters.eq(CommonValues.USER_ID, userID)).first();

            Document doc_e_active = db.database.getCollection(CommonValues.USERS_COLLECTION)
                    .find(Filters.eq(CommonValues.EMAIL, email)).first();
            Document doc_e_inactive = db.database.getCollection(CommonValues.INACTIVE_COLLECTION)
                    .find(Filters.eq(CommonValues.EMAIL, email)).first();
            if (doc_e_active == null && doc_e_inactive == null) {
                db.activeUsersCollection.updateOne(doc
                        , Updates.set(CommonValues.EMAIL, email));
                return Messages.CHANGE_EMAIL_SUCCESSFUL;
            }
            return Messages.CHANGE_EMAIL_UNSUCCESSFUL;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return Messages.SOMETHING_WENT_WRONG;
        }
    }

    static String get_SHA_512_SecurePassword(String passwordToHash){
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            byte[] bytes = md.digest(passwordToHash.getBytes());
            StringBuilder sb = new StringBuilder();
            for (byte aByte : bytes) {
                sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        return generatedPassword;
    }

    private boolean checkPassword(Document dc, String password) {
        return dc.get(CommonValues.PASSWORD).equals(password);
    }

    private boolean isUnique(String userID, String email) {
        Document doc_u = db.database.getCollection(CommonValues.USERS_COLLECTION)
                .find(Filters.eq(CommonValues.USER_ID, userID)).first();
        Document doc_e = db.database.getCollection(CommonValues.USERS_COLLECTION)
                .find(Filters.eq(CommonValues.EMAIL, email)).first();
        Document doc_u_inactive = db.database.getCollection(CommonValues.INACTIVE_COLLECTION)
                .find(Filters.eq(CommonValues.USER_ID, userID)).first();
        Document doc_e_inactive = db.database.getCollection(CommonValues.INACTIVE_COLLECTION)
                .find(Filters.eq(CommonValues.EMAIL, email)).first();

        if (doc_e == null && doc_u == null && doc_e_inactive == null && doc_u_inactive == null)
            return true;
        if (doc_u != null || doc_u_inactive != null) {
            signUpMessages = Messages.SIGNUP_ID_USED;
            return false;
        }
        signUpMessages = Messages.SIGNUP_EMAIL_USED;
        return false;

    }

}
