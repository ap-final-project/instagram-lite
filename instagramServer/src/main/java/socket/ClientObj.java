package socket;

import API.CommonValues;
import DataBase.DataBaseManager;
import DataBase.Registry;
import fileHandling.FileManager;

import java.io.IOException;
import java.net.Socket;

public class ClientObj implements Runnable {
    private SSSocket socket;
    private SocketClass fileSocket;
    private String id;
    private Thread thread;
    private DataBaseManager dataBaseManager;
    private Registry registry;
    private FileManager fileManager;
    private boolean loggedIn = false;

    /**
     *
     * Each client has its own obj and is running in its own thread
     * It gets message from client and create a RequestHandler for it
     */

    public ClientObj(Socket socket, Socket fileSocket, DataBaseManager manager, Registry registry) throws IOException {
        this.socket = new SSSocket(socket);
        this.fileSocket = new SocketClass(fileSocket);
        this.id = String.valueOf(0);
        this.dataBaseManager = manager;
        this.registry = registry;
        this.fileManager = new FileManager(this.fileSocket, CommonValues.SERVER_SIDE_FILE_DIR);
        thread = new Thread(this);
        thread.start();
    }

    public String getId() {
        return id;
    }

    public void setId(String id){
        this.id = id;
    }

    public void setLoggedIn(){
        loggedIn = true;
    }

    public void setLoggedOut(){
        loggedIn = false;
    }

    public boolean isLoggedIn(){
        return loggedIn;
    }

    public SSSocket getSocket() {
        return socket;
    }

    public FileManager getFileManager() {
        return fileManager;
    }



    @Override
    public void run() {
        while (true){
            try {
                String message = new String(socket.readMessage());
                new RequestHandler(this, socket, fileManager,
                        message, dataBaseManager, registry);
            } catch (Exception e) {
                System.err.println(getId() + " unexpectedly went offline.");;
                closeConnection();
                thread.interrupt();
                return;
            }
        }
    }

    public void closeConnection(){
        try {
            socket.close();
            fileSocket.close();
            MainServer.deleteClient(this);
        } catch (IOException ioException) {
            System.err.println(ioException.getMessage());
        }
    }

}
