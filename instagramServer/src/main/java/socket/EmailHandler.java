package socket;

import io.github.cdimascio.dotenv.Dotenv;

import API.CommonValues;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class EmailHandler {

    private Properties properties;
    private MimeMessage message;
    private final String EMAIL_HOST;
    private final String EMAIL_PORT;
    private Session session;

    public EmailHandler() {
        this.properties = System.getProperties();
        this.EMAIL_HOST = "smtp.gmail.com";
        this.EMAIL_PORT = "465";
    }

    public void sendRecoverPasswordEmail(String email, int newPass)  {
        setP();

        message = new MimeMessage(session);
        try {
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(email));
            message.setSubject("Instagram Account Password Recovery\n\n");
            message.setText("Dear " + email + "," +
                    " \nYour new password is: " + newPass +
                    "\nYou can login and change it.");

            Transport.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }

    }

    public void sendVerificationEmail(String email, String userName, int code) {
        setP();
        message = new MimeMessage(session);
        try {
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(email));
            message.setSubject("Instagram Account Verification\n\n");
            message.setText("Hello " + userName + "," +
                    " \nYour verification code is: " + code);
            Transport.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }

    }


    private void setP() {
        properties.setProperty("mail.smtp.host", EMAIL_HOST);
        properties.setProperty("mail.smtp.port", EMAIL_PORT);
        properties.setProperty("mail.smtp.auth", "true");
        properties.setProperty("mail.smtp.ssl.enable", "true");
        properties.setProperty("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");

        session = Session.getInstance(properties, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("notebookteam2020@gmail.com",
                        "1010msnt");
            }
        });
    }
}