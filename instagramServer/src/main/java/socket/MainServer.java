package socket;

import API.CommonValues;
import DataBase.DataBaseManager;
import DataBase.Registry;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;

public class MainServer {

    final int port;
    final int filePort = CommonValues.FILE_PORT;
    public ServerSocket server;
    public ServerSocket fileServer;
    private static ArrayList<ClientObj> clients = new ArrayList<>();

    private DataBaseManager dataBaseManager;
    private Registry registry;

    public MainServer(int port){
        this.port = port;
        dataBaseManager = new DataBaseManager();
        registry = new Registry();
        try {
            this.server = new ServerSocket(this.port);
            this.fileServer = new ServerSocket(this.filePort);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void addClient(ClientObj client) {
        clients.add(client);
    }

    public static void deleteClient(ClientObj client){
        clients.remove(client);
    }

    public static ClientObj getClientObj(String userID) {
        for(ClientObj client : clients){
            if(client.getId().equals(userID)){
                return client;
            }
        }
        return null;
    }

    public void execute(){
        try {
            System.out.println("building SocketServer...");
            System.out.printf("waiting on port %d for clients to connect...\n", port);

            while (true) {

                ClientObj newClient = new ClientObj(server.accept(), fileServer.accept()
                        , dataBaseManager, registry);
                addClient(newClient);

                System.out.println("new client connected");
            }

        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }


}
