package socket;

import API.CommonValues;
import API.Notifications;
import API.Responses;
import DataBase.DataBaseManager;
import com.google.gson.Gson;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import fileHandling.FileManager;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class NotificationHandler {
    private ClientObj clientObj;
    private FileManager fileManager;
    private SSSocket ssSocket;
    private DataBaseManager dataBaseManager;

    public NotificationHandler(String otherUserID) {
        clientObj = MainServer.getClientObj(otherUserID);
        dataBaseManager = new DataBaseManager();
        if(clientObj != null) {
            ssSocket = clientObj.getSocket();
            fileManager = clientObj.getFileManager();
        }
    }


    public void like(String fromUserID, String toUserID, String postID) {
        JsonObject jsNotif = dataBaseManager.likeNotification(fromUserID, toUserID, postID);
        if(clientObj != null) {
            String notification = new Notifications().createLikeNotification(jsNotif);
            System.out.println("Notification to " + clientObj.getId() + "\n" + notification);
            //ssSocket.sendMessage(notification.getBytes());
            ArrayList<String> files = (ArrayList<String>) new Gson().fromJson(jsNotif.get(CommonValues.FILE_NAMES), ArrayList.class);
            //fileManager.sendMany(files);
        }
    }

    public void follow(String fromUserID, String toUserID) {
        JsonObject jsNotif = dataBaseManager.followNotification(fromUserID, toUserID);
        if(clientObj != null) {
            String notification = new Notifications().createFollowNotification(jsNotif);
            System.out.println("Notification to " + clientObj.getId() + "\n" + notification);
            //ssSocket.sendMessage(notification.getBytes());
            ArrayList<String> files = (ArrayList<String>) new Gson().fromJson(jsNotif.get(CommonValues.FILE_NAMES), ArrayList.class);
            //fileManager.sendMany(files);
        }
    }

    public void comment(String fromUserID, String toUserID
            , String postID, String repliedTo, String comment, String commentID) {
        JsonObject jsNotif = dataBaseManager
                .commentNotification(fromUserID, toUserID, postID, repliedTo, comment, commentID);
        if(clientObj != null) {
            String notification = new Notifications().createCommentNotification(jsNotif);
            System.out.println("Notification to " + clientObj.getId() + "\n" + notification);
            //ssSocket.sendMessage(notification.getBytes());
            ArrayList<String> files = (ArrayList<String>) new Gson().fromJson(jsNotif.get(CommonValues.FILE_NAMES), ArrayList.class);
            //fileManager.sendMany(files);
        }
    }

    public void mentionOnComment(String fromUserID, String toUserID
            , String postID, String comment, String commentID) {
        JsonObject jsNotif = dataBaseManager
                .mentionOnCommentNotification(fromUserID, toUserID, postID, comment, commentID);
        if(clientObj != null) {
            String notification = new Notifications().createMentionOnCommentNotification(jsNotif);
            System.out.println("Notification to " + clientObj.getId() + "\n" + notification);
            //ssSocket.sendMessage(notification.getBytes());
            ArrayList<String> files = (ArrayList<String>) new Gson().fromJson(jsNotif.get(CommonValues.FILE_NAMES), ArrayList.class);
            //fileManager.sendMany(files);
        }
    }

    public void directMessage(String sender, String receiver, JsonObject message, String chatID) {
        if(clientObj != null){
            JsonObject jsNotif = dataBaseManager.directMessageNotification(sender, receiver, message);
            if (jsNotif != null) {
                jsNotif.addProperty(CommonValues.CHAT_ID, chatID);
                Object profilePic = jsNotif.get(CommonValues.PROFILE_PIC);
                ArrayList<String> files = new ArrayList<>();
                if(!(profilePic instanceof JsonNull)) {
                    String pic = jsNotif.get(CommonValues.PROFILE_PIC).getAsString();
                    jsNotif.addProperty(CommonValues.FILES_NUM, 1);
                    files.add(pic);
                } else{jsNotif.addProperty(CommonValues.FILES_NUM, 0);}
                String notification = new Notifications().createDirectMessageNotification(jsNotif);
                System.out.println("Notification to " + clientObj.getId() + "\n" + notification);
                //ssSocket.sendMessage(notification.getBytes());
                //fileManager.sendMany(files);
            }
        }
    }
}
