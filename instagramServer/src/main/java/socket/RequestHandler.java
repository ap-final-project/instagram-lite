package socket;


import API.CommonValues;
import API.Messages;
import API.Requests;
import API.Responses;
import DataBase.DataBaseManager;
import DataBase.Registry;
import com.google.gson.*;
import fileHandling.FileManager;

import java.security.SecureRandom;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;


/**
 *
 * Json String coming from client, will be passed to this class
 * Each request is handled in a new thread
 * proper response will be created here( using Response class) and sent to client
 */
public class RequestHandler implements Runnable {

    private final String message;
    private final SSSocket ssSocket;
    private final DataBaseManager dataBaseManager;
    private final Registry registry;
    private final ClientObj client;
    private final FileManager fileManager;
    private final JsonParser jsonParser = new JsonParser();
    private final Gson gson = new Gson();
    private JsonObject json;
    private JsonObject jsonData;

    public RequestHandler(ClientObj clientObj, SSSocket ssSocket, FileManager fileManager,
                          String message, DataBaseManager manager, Registry registry) {
        this.client = clientObj;
        this.message = message;
        this.ssSocket = ssSocket;
        this.fileManager = fileManager;
        this.dataBaseManager = manager;
        this.registry = registry;
        new Thread(this).start();
    }

    @Override
    public void run() {
        json = (JsonObject) jsonParser.parse(message);
        jsonData = json.getAsJsonObject(CommonValues.DATA);
        String request = json.get(CommonValues.REQUEST).getAsString();
        if(request.equals(CommonValues.SIGNUP)) {signUp();}
        else if(request.equals(CommonValues.VERIFY)) {verify();}
        else if(request.equals(CommonValues.LOGIN)) {login();}
        else if(client.isLoggedIn()){
            switch (request){
                case CommonValues.FORGOT_PASSWORD -> forgotPassword();
                case CommonValues.CHANGE_PASSWORD -> changePassword();
                case CommonValues.CHANGE_EMAIL -> changeEmail();
                case CommonValues.EDIT_PROFILE_PIC -> editProfilePic();
                case CommonValues.REMOVE_PROFILE_PIC -> removeProfilePic();
                case CommonValues.EDIT_USERNAME -> editUserName();
                case CommonValues.EDIT_BIO -> editBio();
                case CommonValues.FOLLOW -> follow();
                case CommonValues.UNFOLLOW -> unfollow();
                case CommonValues.GET_FOLLOWERS_LIST -> getFollowersList();
                case CommonValues.GET_FOLLOWING_LIST -> getFollowingList();
                case CommonValues.GET_MAIN_USER_PROFILE -> getMainUserProfile();
                case CommonValues.GET_ACTIVITIES -> getActivities();
                case CommonValues.GET_TIMELINE -> getTimeline();
                case CommonValues.GET_USER_PROFILE -> getUserProfile();
                case CommonValues.GET_MORE_POST -> getMorePost();
                case CommonValues.POST -> post();
                case CommonValues.DELETE_POST -> deletePost();
                case CommonValues.LIKE -> like();
                case CommonValues.UNLIKE -> unlike();
                case CommonValues.GET_LIKE_LIST -> getLikeList();
                case CommonValues.COMMENT -> comment();
                case CommonValues.DELETE_COMMENT -> deleteComment();
                case CommonValues.GET_COMMENT_LIST -> getCommentList();
                case CommonValues.GET_CHAT_LIST -> getChatList();
                case CommonValues.SEND_DIRECT_MESSAGE -> sendDirectMessage();
                case CommonValues.GET_DIRECT_MESSAGES -> getDirectMessages();
                case CommonValues.LOGOUT -> logout();
                case CommonValues.DELETE_ACCOUNT -> deleteAccount();
                default -> invalidRequest();
            }
        }else {invalidRequest();}
    }

    /**
     *
     * a 5 digits random code will be created
     * if userID and email are unique, user info and code will be saved in database as an inactive user
     * the code will be sent to user email address
     * proper response will be created (using "API.Responses) and sent to user
     */
    private void signUp() {
        Requests.SignUp signUpInfo = gson.fromJson(json, Requests.SignUp.class);
        Requests.SignUp.data data = gson.fromJson(jsonData, Requests.SignUp.data.class);
        String message = registry.signUp(data.userID, data.userName, data.email, data.password);
        boolean result;
        int code = 0;
        if (message.equals(Messages.SIGNUP_SUCCESSFUL)) {
            code = create5DigitRandomCode();
            result = true;
            new EmailHandler().sendVerificationEmail(data.email, data.userName, code);
        } else {
            result = false;
        }

        String response = new Responses(signUpInfo.requestID, result).createSignUpResponse(message, code);
        sendResponse(response);
    }

    /**
     *
     * if userID and code match, user will be saved as an active one
     * proper response will be created (using "API.Responses) and sent to user
     */
    private void verify() {
        Requests.Verify verifyInfo = gson.fromJson(json, Requests.Verify.class);
        Requests.Verify.data data = gson.fromJson(jsonData, Requests.Verify.data.class);
        String message = registry.verifyAccount(data.email, data.result);

        boolean result = message.equals(Messages.VERIFY_SUCCESSFUL);
        String response = new Responses(verifyInfo.requestID, result).createVerifyResponse(message);
        sendResponse(response);
    }

    /**
     *
     * check if userID and password match
     * proper response will be created (using "API.Responses) and sent to user
     */
    private void login() {
        Requests.Login loginInfo = gson.fromJson(json, Requests.Login.class);
        Requests.Login.data data = gson.fromJson(jsonData, Requests.Login.data.class);
        String message = registry.login(data.userID, data.password);
        boolean result = message.equals(Messages.LOGIN_SUCCESSFUL);

        JsonObject jsResponse = new JsonObject();
        jsResponse.addProperty(CommonValues.MESSAGE, message);
        if (result) {
            client.setId(data.userID);
            client.setLoggedIn();
            jsResponse = dataBaseManager.getMainUserProfile(data.userID);
            ArrayList<String> files = getFileNames(jsResponse);
            jsResponse.addProperty(CommonValues.FILES_NUM, files.size());
            jsResponse.addProperty(CommonValues.MESSAGE, message);
            String response = new Responses(loginInfo.requestID, result).createLoginResponse(jsResponse);
            sendResponseIncludingFiles(response, files);
        }else{
            String response = new Responses(loginInfo.requestID, result).createLoginResponse(jsResponse);
            sendResponse(response);
        }
    }

    /**
     *
     * a 5 digits random code will be created and sent to user email address
     * user password will be changed to the random code (user can change it later)
     * proper response will be created (using "API.Responses) and sent to user
     */
    private void forgotPassword(){
        Requests.ForgotPassword forgotPasswordInfo = gson.fromJson(json, Requests.ForgotPassword.class);
        Requests.ForgotPassword.data data = gson.fromJson(jsonData, Requests.ForgotPassword.data.class);

        int newPassword = create5DigitRandomCode();
        String message = registry.recoverPassword(data.email, newPassword);
        boolean result = message.equals(Messages.FORGOT_PASS_SUCCESSFUL);
        String response = new Responses(forgotPasswordInfo.requestID, result).createForgotPasswordResponse(message);

        if(result){
            new EmailHandler().sendRecoverPasswordEmail(data.email, newPassword);
        }
        sendResponse(response);
    }

    private void changePassword(){
        Requests.ChangePassword changePasswordInfo = gson.fromJson(json, Requests.ChangePassword.class);
        Requests.ChangePassword.data data = gson.fromJson(jsonData, Requests.ChangePassword.data.class);

        String message = registry.changePassword(client.getId(), data.oldPassword, data.newPassword);

        boolean result = message.equals(Messages.CHANGE_PASS_SUCCESSFUL);
        String newPassword = null;
        if(result){
            newPassword = data.newPassword;
        }
        String response = new Responses(changePasswordInfo.requestID, result).createChangePasswordResponse(message, newPassword);

        sendResponse(response);
    }

    private void changeEmail(){
        Requests.ChangeEmail changeEmailInfo = gson.fromJson(json, Requests.ChangeEmail.class);
        Requests.ChangeEmail.data data = gson.fromJson(jsonData, Requests.ChangeEmail.data.class);

        String message = registry.changeEmail(client.getId(), data.email);
        boolean result = message.equals(Messages.CHANGE_EMAIL_SUCCESSFUL);
        String newEmail = null;
        if(result){
            newEmail = data.email;
        }
        String response = new Responses(changeEmailInfo.requestID, result).createChangeEmailResponse(message, newEmail);

        sendResponse(response);
    }

    private void editProfilePic(){
        Requests.EditProfilePic editProfilePicInfo = gson.fromJson(json, Requests.EditProfilePic.class);
        Requests.EditProfilePic.data data = gson.fromJson(jsonData, Requests.EditProfilePic.data.class);
        fileManager.receiveOne();
        String message = dataBaseManager.updateProfilePic(client.getId(), data.fileName);
        boolean result = message.equals(Messages.EDIT_PROFILE_PIC_SUCCESSFUL);
        String response = new Responses(editProfilePicInfo.requestID, result).createEditProfilePicResponse(message);

        sendResponse(response);
    }

    private void removeProfilePic(){
        Requests.RemoveProfilePic removeProfilePicInfo = gson.fromJson(json, Requests.RemoveProfilePic.class);
        Requests.RemoveProfilePic.data data = gson.fromJson(jsonData, Requests.RemoveProfilePic.data.class);
        String message = dataBaseManager.removeProfilePic(client.getId());
        boolean result = message.equals(Messages.REMOVE_PROFILE_PIC_SUCCESSFUL);
        String response = new Responses(removeProfilePicInfo.requestID, result).createRemoveProfilePicResponse(message);
        sendResponse(response);
    }

    private void editUserName(){
        Requests.EditUserName editUserNameInfo = gson.fromJson(json, Requests.EditUserName.class);
        Requests.EditUserName.data data = gson.fromJson(jsonData, Requests.EditUserName.data.class);

        String message = dataBaseManager.updateUserName(client.getId(), data.userName);

        boolean result = message.equals(Messages.EDIT_USERNAME_SUCCESSFUL);
        String response = new Responses(editUserNameInfo.requestID, result).createEditUserNameResponse(message, data.userName);

        sendResponse(response);
    }

    private void editBio(){
        Requests.EditBio editBioInfo = gson.fromJson(json, Requests.EditBio.class);
        Requests.EditBio.data data = gson.fromJson(jsonData, Requests.EditBio.data.class);

        String message = dataBaseManager.updateBio(client.getId(), data.bio);

        boolean result = message.equals(Messages.EDIT_BIO_SUCCESSFUL);
        String response = new Responses(editBioInfo.requestID, result).createEditBioResponse(message, data.bio);

        sendResponse(response);
    }

    private void follow(){
        Requests.Follow followInfo = gson.fromJson(json, Requests.Follow.class);
        Requests.Follow.data data = gson.fromJson(jsonData, Requests.Follow.data.class);
        String message = dataBaseManager.follow(client.getId(), data.userID);

        boolean result = message.equals(Messages.FOLLOWED_SUCCESSFULLY);

        String response = new Responses(followInfo.requestID, result).createFollowResponse(message);
        sendResponse(response);
        if (result) {
            new NotificationHandler(data.userID).follow(client.getId(), data.userID);
        }
    }

    private void unfollow(){
        Requests.Unfollow unfollowInfo = gson.fromJson(json, Requests.Unfollow.class);
        Requests.Unfollow.data data = gson.fromJson(jsonData, Requests.Unfollow.data.class);
        String message = dataBaseManager.unfollow(client.getId(), data.userID);

        boolean result = message.equals(Messages.UNFOLLOWED_SUCCESSFULLY);

        String response = new Responses(unfollowInfo.requestID, result).createUnfollowResponse(message);
        sendResponse(response);
    }

    private void getFollowersList(){
        Requests.GetFollowersList getFollowersListInfo = gson.fromJson(json, Requests.GetFollowersList.class);
        Requests.GetFollowersList.data data = gson.fromJson(jsonData, Requests.GetFollowersList.data.class);
        JsonObject jsResponse = dataBaseManager.getFollowersList(client.getId(), data.userID, data.last);

        boolean result = jsResponse.get(CommonValues.MESSAGE).getAsString().equals(Messages.FOLLOWERS_LIST_GET_SUCCESSFULLY);
        if(result){
            ArrayList<String> files = getFileNames(jsResponse);
            jsResponse.addProperty(CommonValues.FILES_NUM, files.size());
            String response = new Responses(getFollowersListInfo.requestID, result).createGetFollowersListResponse(jsResponse);
            sendResponseIncludingFiles(response, files);
        }else {
            String response = new Responses(getFollowersListInfo.requestID, result).createGetFollowersListResponse(jsResponse);
            sendResponse(response);
        }
    }

    private void getFollowingList(){
        Requests.GetFollowingList getFollowingListInfo = gson.fromJson(json, Requests.GetFollowingList.class);
        Requests.GetFollowingList.data data = gson.fromJson(jsonData, Requests.GetFollowingList.data.class);
        JsonObject jsResponse = dataBaseManager.getFollowingList(client.getId(), data.userID, data.last);

        boolean result = jsResponse.get(CommonValues.MESSAGE).getAsString().equals(Messages.FOLLOWING_LIST_GET_SUCCESSFULLY);
        if(result){
            ArrayList<String> files = getFileNames(jsResponse);
            jsResponse.addProperty(CommonValues.FILES_NUM, files.size());
            String response = new Responses(getFollowingListInfo.requestID, result).createGetFollowingListResponse(jsResponse);
            sendResponseIncludingFiles(response, files);
        }else {
            String response = new Responses(getFollowingListInfo.requestID, result).createGetFollowingListResponse(jsResponse);
            sendResponse(response);
        }
    }

    private void getMainUserProfile(){
        Requests.GetMainUserProfile getMainUserProfileInfo = gson.fromJson(json, Requests.GetMainUserProfile.class);
        Requests.GetMainUserProfile.data data = gson.fromJson(jsonData, Requests.GetMainUserProfile.data.class);
        JsonObject jsResponse = dataBaseManager.getMainUserProfile(client.getId());

        boolean result = jsResponse.get(CommonValues.MESSAGE).getAsString().equals(Messages.GET_SELF_PROFILE_SUCCESSFULLY);
        if (result) {
            ArrayList<String> files = getFileNames(jsResponse);
            jsResponse.addProperty(CommonValues.FILES_NUM, files.size());
            String response = new Responses(getMainUserProfileInfo.requestID, result)
                    .createGetMainUserProfileResponse(jsResponse);
            sendResponseIncludingFiles(response, files);
        }else {
            String response = new Responses(getMainUserProfileInfo.requestID, result)
                    .createGetMainUserProfileResponse(jsResponse);
            sendResponse(response);
        }
    }

    private void getActivities(){
        Requests.GetActivities getActivitiesInfo = gson.fromJson(json, Requests.GetActivities.class);
        Requests.GetActivities.data data = gson.fromJson(jsonData, Requests.GetActivities.data.class);
        JsonObject jsResponse = dataBaseManager.getActivities(client.getId(), data.last);

        boolean result = jsResponse.get(CommonValues.MESSAGE).getAsString().equals(Messages.ACTIVITIES_GET_SUCCESSFULLY);
        if (result) {
            ArrayList<String> files = getFileNames(jsResponse);
            jsResponse.addProperty(CommonValues.FILES_NUM, files.size());
            String response = new Responses(getActivitiesInfo.requestID, result).createGetActivitiesResponse(jsResponse);
            sendResponseIncludingFiles(response, files);
        }else {
            String response = new Responses(getActivitiesInfo.requestID, result).createGetActivitiesResponse(jsResponse);
            sendResponse(response);
        }
    }

    private void getTimeline(){
        Requests.GetTimeline getTimelineInfo = gson.fromJson(json, Requests.GetTimeline.class);
        Requests.GetTimeline.data data = gson.fromJson(jsonData, Requests.GetTimeline.data.class);
        JsonObject jsResponse = dataBaseManager.getTimeline(client.getId(), data.last);
        boolean result = jsResponse.get(CommonValues.MESSAGE).getAsString().equals(Messages.POSTS_GET_SUCCESSFULLY);

        if (result) {
            ArrayList<String> files = getFileNames(jsResponse);
            jsResponse.addProperty(CommonValues.FILES_NUM, files.size());
            String response = new Responses(getTimelineInfo.requestID, result).createGetTimelineResponse(jsResponse);
            sendResponseIncludingFiles(response, files);
        }else {
            String response = new Responses(getTimelineInfo.requestID, result).createGetTimelineResponse(jsResponse);
            sendResponse(response);
        }

    }

    private void getUserProfile(){
        Requests.GetUserProfile getUserProfileInfo = gson.fromJson(json, Requests.GetUserProfile.class);
        Requests.GetUserProfile.data data = gson.fromJson(jsonData, Requests.GetUserProfile.data.class);
        JsonObject jsResponse = dataBaseManager.getUserProfile(client.getId(), data.userID);

        boolean result = jsResponse.get(CommonValues.MESSAGE).getAsString().equals(Messages.GET_USER_PROFILE_SUCCESSFULLY);
        if (result) {
            ArrayList<String> files = getFileNames(jsResponse);
            jsResponse.addProperty(CommonValues.FILES_NUM, files.size());
            String response = new Responses(getUserProfileInfo.requestID, result).createGetUserProfileResponse(jsResponse);
            sendResponseIncludingFiles(response, files);
        }else {
            String response = new Responses(getUserProfileInfo.requestID, result).createGetUserProfileResponse(jsResponse);
            sendResponse(response);
        }

    }

    private void getMorePost(){
        Requests.GetMorePost getMorePostInfo = gson.fromJson(json, Requests.GetMorePost.class);
        Requests.GetMorePost.data data = gson.fromJson(jsonData, Requests.GetMorePost.data.class);
        JsonObject jsResponse = dataBaseManager.getMorePost(client.getId(), data.userID, data.last);

        boolean result = jsResponse.get(CommonValues.MESSAGE).getAsString().equals(Messages.POSTS_GET_SUCCESSFULLY);
        if (result) {
            ArrayList<String> files = getFileNames(jsResponse);
            jsResponse.addProperty(CommonValues.FILES_NUM, files.size());
            String response = new Responses(getMorePostInfo.requestID, result).createGetMorePostResponse(jsResponse);
            sendResponseIncludingFiles(response, files);
        }else {
            String response = new Responses(getMorePostInfo.requestID, result).createGetMorePostResponse(jsResponse);
            sendResponse(response);
        }

    }

    private void post(){
        Requests.Post postInfo = gson.fromJson(json, Requests.Post.class);
        Requests.Post.data data = gson.fromJson(jsonData, Requests.Post.data.class);
        fileManager.receiveOne();
        JsonObject jsResponse = dataBaseManager.addPost(client.getId(), data.caption, data.fileName);
        boolean result = jsResponse.get(CommonValues.MESSAGE).getAsString().equals(Messages.POSTED_SUCCESSFULLY);

        String response = new Responses(postInfo.requestID, result).createPostResponse(jsResponse);

        sendResponse(response);
    }

    private void deletePost(){
        Requests.DeletePost deletePostInfo = gson.fromJson(json, Requests.DeletePost.class);
        Requests.DeletePost.data data = gson.fromJson(jsonData, Requests.DeletePost.data.class);

        String message = dataBaseManager.deletePost(data.postID);
        boolean result = message.equals(Messages.POST_DELETED_SUCCESSFULLY);
        String response = new Responses(deletePostInfo.requestID, result).createDeletePostResponse(message);

        sendResponse(response);
    }

    private void like(){
        Requests.Like likeInfo = gson.fromJson(json, Requests.Like.class);
        Requests.Like.data data = gson.fromJson(jsonData, Requests.Like.data.class);
        String message = dataBaseManager.like(data.postID, client.getId());

        boolean result = message.equals(Messages.LIKED_SUCCESSFULLY);

        String response = new Responses(likeInfo.requestID, result).createLikeResponse(message);
        sendResponse(response);

        if (result && !client.getId().equals(data.otherUserID)) {
            new NotificationHandler(data.otherUserID).like(client.getId(), data.otherUserID, data.postID);
        }

    }

    private void unlike(){
        Requests.Unlike unlikeInfo = gson.fromJson(json, Requests.Unlike.class);
        Requests.Unlike.data data = gson.fromJson(jsonData, Requests.Unlike.data.class);
        String message = dataBaseManager.unlike(data.postID, client.getId());

        boolean result = message.equals(Messages.UNLIKED_SUCCESSFULLY);

        String response = new Responses(unlikeInfo.requestID, result).createUnlikeResponse(message);
        sendResponse(response);
    }

    private void getLikeList(){
        Requests.GetLikeList getLikeListInfo = gson.fromJson(json, Requests.GetLikeList.class);
        Requests.GetLikeList.data data = gson.fromJson(jsonData, Requests.GetLikeList.data.class);
        JsonObject jsResponse = dataBaseManager.getLikeList(client.getId(), data.postID, data.last);

        boolean result = jsResponse.get(CommonValues.MESSAGE).getAsString().equals(Messages.LIKE_LIST_GET_SUCCESSFULLY);
        if (result) {
            ArrayList<String> files = getFileNames(jsResponse);
            jsResponse.addProperty(CommonValues.FILES_NUM, files.size());
            String response = new Responses(getLikeListInfo.requestID, result).createGetLikeListResponse(jsResponse);
            sendResponseIncludingFiles(response, files);
        } else{
            String response = new Responses(getLikeListInfo.requestID, result).createGetLikeListResponse(jsResponse);
            sendResponse(response);
        }
    }

    private void comment(){
        Requests.Comment commentInfo = gson.fromJson(json, Requests.Comment.class);
        Requests.Comment.data data = gson.fromJson(jsonData, Requests.Comment.data.class);
        JsonObject jsResponse = dataBaseManager.comment(client.getId() , data.postID, data.repliedTo, data.comment);

        boolean result = jsResponse.get(CommonValues.MESSAGE).getAsString().equals(Messages.COMMENTED_SUCCESSFULLY);

        String response = new Responses(commentInfo.requestID, result).createCommentResponse(jsResponse);
        sendResponse(response);
        if(result) {
            if(!client.getId().equals(data.otherUserID)) {
                new NotificationHandler(data.otherUserID)
                        .comment(client.getId(), data.otherUserID
                                , data.postID, data.repliedTo, data.comment
                                , jsResponse.get(CommonValues.COMMENT_ID).getAsString());
            }
            if(data.repliedTo != null && data.repliedTo.length() > 0
                    && !client.getId().equals(data.repliedTo) && !data.otherUserID.equals(data.repliedTo) ) {
                new NotificationHandler(data.repliedTo)
                        .mentionOnComment(client.getId(), data.repliedTo
                                , data.postID, data.comment
                                , jsResponse.get(CommonValues.COMMENT_ID).getAsString());
            }
        }
    }

    private void deleteComment() {
        Requests.DeleteComment deleteCommentInfo = gson.fromJson(json, Requests.DeleteComment.class);
        Requests.DeleteComment.data data = gson.fromJson(jsonData, Requests.DeleteComment.data.class);
        String message = dataBaseManager.deleteComment(data.commentID);

        boolean result = message.equals(Messages.COMMENT_DELETED_SUCCESSFULLY);

        String response = new Responses(deleteCommentInfo.requestID, result).createDeleteCommentResponse(message);
        sendResponse(response);
    }

    private void getCommentList() {
        Requests.GetCommentList getCommentListInfo = gson.fromJson(json, Requests.GetCommentList.class);
        Requests.GetCommentList.data data = gson.fromJson(jsonData, Requests.GetCommentList.data.class);

        JsonObject jsResponse = dataBaseManager.getCommentList(data.postID, data.last);
        boolean result = jsResponse.get(CommonValues.MESSAGE).getAsString().equals(Messages.GET_COMMENT_LIST_SUCCESSFULLY);
        if(result){
            ArrayList<String> files = getFileNames(jsResponse);
            jsResponse.addProperty(CommonValues.FILES_NUM, files.size());
            String response = new Responses(getCommentListInfo.requestID, result).createGetCommentListResponse(jsResponse);
            sendResponseIncludingFiles(response, files);
        } else {
            String response = new Responses(getCommentListInfo.requestID, result).createGetCommentListResponse(jsResponse);
            sendResponse(response);
        }
    }

    private void getChatList() {
        Requests.GetChatList getChatListInfo = gson.fromJson(json, Requests.GetChatList.class);
        JsonObject jsResponse = dataBaseManager.getChatList(client.getId());
        boolean result = jsResponse.get(CommonValues.MESSAGE).getAsString().equals(Messages.CHAT_LIST_GET_SUCCESSFULLY);

        if (result){
            ArrayList<String> files = getFileNames(jsResponse);
            jsResponse.addProperty(CommonValues.FILES_NUM, files.size());
            String response = new Responses(getChatListInfo.requestID, result).createGetChatListResponse(jsResponse);
            sendResponseIncludingFiles(response, files);
        } else {
            String response = new Responses(getChatListInfo.requestID, result).createGetChatListResponse(jsResponse);
            sendResponse(response);
        }

    }

    private void sendDirectMessage(){
        Requests.SendDirectMessage sendMessageInfo = gson.fromJson(json, Requests.SendDirectMessage.class);
        Requests.SendDirectMessage.data data = gson.fromJson(jsonData, Requests.SendDirectMessage.data.class);
        JsonObject directMsg = new JsonObject();
        String date = ZonedDateTime.now(ZoneId.of("Asia/Tehran"))
                .format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        directMsg.addProperty(CommonValues.CONTENT, data.content);
        directMsg.addProperty(CommonValues.SENDER, client.getId());
        directMsg.addProperty(CommonValues.DATE, date);

        JsonObject jsResponse = dataBaseManager.sendDirectMessage(client.getId(), data.receiver, directMsg.toString());

        boolean result = jsResponse.get(CommonValues.MESSAGE).getAsString().equals(Messages.DIRECT_SENT_SUCCESSFULLY);

        String response = new Responses(sendMessageInfo.requestID, result)
                .createSendDirectMessageResponse(message, directMsg);
        sendResponse(response);

        if (result) {
            new NotificationHandler(data.receiver).directMessage(client.getId(), data.receiver, directMsg
                    , jsResponse.get(CommonValues.CHAT_ID).getAsString());
        }

    }

    private void getDirectMessages(){
        Requests.GetDirectMessages getMessagesInfo = gson.fromJson(json, Requests.GetDirectMessages.class);
        Requests.GetDirectMessages.data data = gson.fromJson(jsonData, Requests.GetDirectMessages.data.class);

        JsonObject jsResponse = dataBaseManager.getDirectMessages(data.chatID, data.length);
        boolean result = jsResponse.get(CommonValues.MESSAGE).getAsString().equals(Messages.DIRECT_GET_SUCCESSFULLY + data.length);

        String response = new Responses(getMessagesInfo.requestID, result).createGetDirectMessagesResponse(jsResponse);

        sendResponse(response);
    }

    private void logout(){
        Requests.Logout logoutInfo = gson.fromJson(json, Requests.Logout.class);
        String response = new Responses(logoutInfo.requestID, true).createLogoutResponse(Messages.LOGOUT_SUCCESSFUL);
        sendResponse(response);

        client.setId("0");
        client.setLoggedOut();
    }

    private void deleteAccount(){
        Requests.Logout deleteAccountInfo = gson.fromJson(json, Requests.Logout.class);
        Requests.DeleteAccount.data data = gson.fromJson(jsonData, Requests.DeleteAccount.data.class);
        String message = dataBaseManager.deleteAccount(client.getId(), data.password);
        boolean result = message.equals(Messages.DELETE_ACCOUNT_SUCCESSFUL);

        String response = new Responses(deleteAccountInfo.requestID, result).createDeleteAccountResponse(message);
        sendResponse(response);

        if (result){
            client.setId("0");
            client.setLoggedOut();
        }
    }

    private void invalidRequest(){
        long requestID = json.get(CommonValues.REQUEST_ID).getAsLong();
        String response = new Responses(requestID, false).createInvalidRequestResponse();
        sendResponse(response);
    }



    private void sendResponseIncludingFiles(String response, ArrayList<String> files) {
        try {
            printLog(response);
            ssSocket.sendMessage(response.getBytes());
            fileManager.sendMany(files);
        } catch (Exception e) {
            System.err.println("Something went wrong!\nServer can not send response to " + client.getId());
        }
    }

    private void sendResponse(String response) {
        try {
            printLog(response);
            ssSocket.sendMessage(response.getBytes());
        } catch (Exception e) {
            System.err.println("Something went wrong!\nServer can not send response to " + client.getId());
        }
    }

    private void printLog(String response) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(response);
        String prettyResponse = gson.toJson(je);
        System.out.println("Request from " + client.getId() + "\n" + this.message + "\n" +
                "Response :\n" + prettyResponse + "\n\n");
    }

    private ArrayList<String> getFileNames(JsonObject js){
        return (ArrayList<String>) gson.fromJson(js.get(CommonValues.FILE_NAMES), ArrayList.class);
    }

    private int create5DigitRandomCode() {
        SecureRandom random = new SecureRandom();
        int num = random.nextInt(100000);
        if (num > 9999) {
            return num;
        }
        return num + 10000;
    }

}
