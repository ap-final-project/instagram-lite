package API;

import io.github.cdimascio.dotenv.Dotenv;

import java.time.ZoneId;

public class CommonValues {
    public static final int FILE_PORT = 8888;
    public static final int MAIN_PORT = 5103;

    //public static final ZoneId zone = ZoneId.of(Dotenv.load().get("ZONE"));


    public static final String SERVER_SIDE_FILE_DIR = "instagramServer/database/";


    public static final String ACTIVITIES_COLLECTION = "Activities";
    public static final String INACTIVE_COLLECTION = "Inactive";
    public static final String COMMENTS_COLLECTION = "Comments";
    public static final String DATABASE_NAME = "InstagramDB";
    public static final String POSTS_COLLECTION = "Posts";
    public static final String CHATS_COLLECTION = "Chats";
    public static final String USERS_COLLECTION = "Users";

    public static final String GET_MAIN_USER_PROFILE = "getMainUserProfile";
    public static final String GET_FOLLOWERS_LIST = "getFollowersList";
    public static final String GET_FOLLOWING_LIST = "getFollowingList";
    public static final String GET_DIRECT_MESSAGES = "getDirectMessages";
    public static final String GET_USER_PROFILE = "getUserProfile";
    public static final String GET_COMMENT_LIST = "getCommentList";
    public static final String GET_ACTIVITIES = "getActivities";
    public static final String GET_CHAT_LIST = "getChatList";
    public static final String GET_MORE_POST = "getMorePost";
    public static final String GET_LIKE_LIST = "getLikeList";
    public static final String GET_TIMELINE = "getTimeline";

    public static final String PROFILE_PIC = "profilePic";
    public static final String ACTIVITIES = "activities";
    public static final String FOLLOWERS = "followers";
    public static final String FOLLOWING = "following";
    public static final String USERNAME = "userName";
    public static final String PASSWORD = "password";
    public static final String POSTS = "posts";
    public static final String CHATS = "chats";
    public static final String EMAIL = "email";
    public static final String BIO = "bio";

    public static final String OTHER_USER_ID = "otherUserID";
    public static final String ACTIVITY_ID = "activityID";
    public static final String COMMENT_ID = "commentID";
    public static final String REQUEST_ID = "requestID";
    public static final String CHAT_ID = "chatID";
    public static final String POST_ID = "postID";
    public static final String USER_ID = "userID";
    public static final String ID = "_id";

    public static final String NOTIFICATION = "notification";
    public static final String FILE_NAMES = "fileNames";
    public static final String REQUEST = "request";
    public static final String RESULT = "result";
    public static final String CODE = "code";
    public static final String DATA = "data";
    public static final String DATE = "date";

    public static final String FORGOT_PASSWORD = "forgotPassword";
    public static final String CHANGE_PASSWORD = "changePassword";
    public static final String CHANGE_EMAIL = "changeEmail";
    public static final String FOLLOWED = "followed";
    public static final String UNFOLLOW = "unfollow";
    public static final String FOLLOW = "follow";

    public static final String SEND_DIRECT_MESSAGE = "sendDirectMessage";
    public static final String DIRECT_MESSAGES = "directMessages";
    public static final String DIRECT_MESSAGE = "directMessage";
    public static final String PARTICIPANTS = "participants";
    public static final String LAST_MESSAGE = "lastMessage";
    public static final String CHAT = "chat";

    public static final String MENTION_ON_COMMENT = "mentionOnComment";
    public static final String DELETE_COMMENT = "deleteComment";
    public static final String REPLIED_TO = "repliedTo";
    public static final String COMMENTS = "comments";
    public static final String COMMENT = "comment";

    public static final String REMOVE_PROFILE_PIC = "removeProfilePic";
    public static final String EDIT_PROFILE_PIC = "editProfilePic";
    public static final String EDIT_USERNAME = "editUserName";
    public static final String EDIT_BIO = "editBio";

    public static final String FOLLOWING_NUM = "followingNumber";
    public static final String FOLLOWERS_NUM = "followersNumber";
    public static final String FILES_NUM = "filesNumber";
    public static final String POST_NUM = "postNumber";

    public static final String DELETE_POST = "deletePost";
    public static final String CAPTION = "caption";
    public static final String MEDIA = "media";
    public static final String POST = "post";

    public static final String RECEIVER = "receiver";
    public static final String MESSAGE = "message";
    public static final String CONTENT = "content";
    public static final String SENDER = "sender";

    public static final String UNLIKE = "unlike";
    public static final String LIKES = "likes";
    public static final String LIKED = "liked";
    public static final String LIKE = "like";

    public static final String SIGNUP = "signUp";
    public static final String VERIFY = "verify";
    public static final String LOGIN = "login";

    public static final String DELETE_ACCOUNT = "deleteAccount";
    public static final String LOGOUT = "logout";
}
