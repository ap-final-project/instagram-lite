package API;

public class Messages {
    //public static final String EDIT_BIO_UNSUCCESSFUL = "Bio must contains less than 200 characters.";

    public static final String INCORRECT_CODE = "The entered code is incorrect";
    public static final String FILL_FIELDS = "Please fill all fields";
    public static final String PROBLEM = "Sorry!\n\nThere seems to be a problem.\n\nPlease try again later.";

    public static final String SOMETHING_WENT_WRONG = "Something went wrong!";
    public static final String WRONG_PASSWORD = "Wrong password!";

    public static final String SIGNUP_PASS_INCORRECT = "The entered password is incorrect.";
    public static final String SIGNUP_PASS_5_CHAR = "Password must contains 5 characters.";
    public static final String SIGNUP_EMAIL_USED = "This Email has already been used.";
    public static final String SIGNUP_SUCCESSFUL = "Account successfully created.";
    public static final String SIGNUP_ID_USED = "This ID has already been used.";

    public static final String VERIFY_UNSUCCESSFUL = "User data has been removed.";
    public static final String VERIFY_SUCCESSFUL = "Verified successfully.";

    public static final String LOGIN_SUCCESSFUL = "You are successfully logged in.";
    public static final String LOGIN_UNSUCCESSFUL = "ID or password is incorrect.";

    public static final String FORGOT_PASS_ERROR = "There's no account associated with this email address.";
    public static final String FORGOT_PASS_SUCCESSFUL = "We'll send the password to your email.";

    public static final String CHANGE_PASS_MISMATCH = "Your new password and confirmation password do not match.";
    public static final String CHANGE_PASS_INCORRECT = "The current password is not incorrect.";
    public static final String CHANGE_PASS_SUCCESSFUL = "Password successfully changed.";

    public static final String CHANGE_EMAIL_SUCCESSFUL = "Email address successfully changed.";
    public static final String CHANGE_EMAIL_UNSUCCESSFUL = "This Email has already been used.";
    public static final String CHANGE_EMAIL_INCORRECT = "Email address is incorrect.";

    public static final String REMOVE_PROFILE_PIC_SUCCESSFUL = "Profile picture removed successfully.";
    public static final String EDIT_PROFILE_PIC_SUCCESSFUL = "Profile picture successfully changed.";
    public static final String EDIT_USERNAME_UNSUCCESSFUL = "Username is incorrect.";
    public static final String EDIT_USERNAME_SUCCESSFUL = "Username successfully changed.";
    public static final String EDIT_BIO_SUCCESSFUL = "Bio successfully changed.";

    public static final String GET_USER_PROFILE_SUCCESSFULLY = "Got the user profile successfully.";
    public static final String GET_SELF_PROFILE_SUCCESSFULLY = "Got your profile successfully.";
    public static final String GET_COMMENT_LIST_SUCCESSFULLY = "Get comment list successfully.";
    public static final String GET_USER_PROFILE_UNSUCCESSFUL = "User not found.";

    public static final String FOLLOWERS_LIST_GET_SUCCESSFULLY = "Got followers list successfully.";
    public static final String FOLLOWING_LIST_GET_SUCCESSFULLY = "Got following list successfully.";
    public static final String ACTIVITIES_GET_SUCCESSFULLY = "Got activities successfully.";
    public static final String LIKE_LIST_GET_SUCCESSFULLY = "Got like list successfully.";
    public static final String CHAT_LIST_GET_SUCCESSFULLY = "Got chat list successfully.";
    public static final String POSTS_GET_SUCCESSFULLY = "Got post list successfully.";
    public static final String DIRECT_GET_SUCCESSFULLY = "Messages for length ";

    public static final String UNFOLLOWED_SUCCESSFULLY = "Unfollowed successfully.";
    public static final String FOLLOWED_SUCCESSFULLY = "Followed successfully.";
    public static final String UNLIKED_SUCCESSFULLY = "Unliked successfully.";
    public static final String LIKED_SUCCESSFULLY = "Liked successfully.";

    public static final String COMMENT_DELETED_SUCCESSFULLY = "Comment deleted successfully.";
    public static final String POST_DELETED_SUCCESSFULLY = "Post deleted successfully.";
    public static final String COMMENTED_SUCCESSFULLY = "Commented successfully.";
    public static final String POSTED_SUCCESSFULLY = "Posted successfully.";

    public static final String DELETE_ACCOUNT_SUCCESSFUL = "Account deleted successfully.";
    public static final String LOGOUT_SUCCESSFUL = "Logged out successfully.";

    public static final String DIRECT_SENT_SUCCESSFULLY = "Message sent successfully.";

}
