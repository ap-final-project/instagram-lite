package API;

import io.github.cdimascio.dotenv.Dotenv;

public class Paths {

    //public final static String RESPONSES_ROOT = dotenv.get("RESPONSES_ROOT");
    //public final static String REQUESTS_ROOT = dotenv.get("REQUESTS_ROOT");
    //public final static String NOTIFICATIONS_ROOT = dotenv.get("NOTIFICATIONS_ROOT");

    public final static String RESPONSES_ROOT = "src/docs/json/responses/";
    public final static String REQUESTS_ROOT = "src/docs/json/request/";
    public final static String NOTIFICATIONS_ROOT = "src/docs/json/notifications/";

    public final static String SIGNUP_RESPONSE_FILE_PATH = RESPONSES_ROOT + "signUp.json";
    public final static String VERIFY_RESPONSE_FILE_PATH = RESPONSES_ROOT + "verify.json";
    public final static String LOGIN_RESPONSE_FILE_PATH = RESPONSES_ROOT + "login.json";
    public final static String FORGOT_PASSWORD_RESPONSE_FILE_PATH = RESPONSES_ROOT + "forgotPass.json";
    public final static String CHANGE_PASSWORD_RESPONSE_FILE_PATH = RESPONSES_ROOT + "changePass.json";
    public final static String CHANGE_EMAIL_RESPONSE_FILE_PATH = RESPONSES_ROOT + "changeEmail.json";
    public final static String EDIT_PROFILE_PIC_RESPONSE_FILE_PATH = RESPONSES_ROOT + "editProfilePic.json";
    public final static String REMOVE_PROFILE_PIC_RESPONSE_FILE_PATH = RESPONSES_ROOT + "removeProfilePic.json";
    public final static String EDIT_USERNAME_RESPONSE_FILE_PATH = RESPONSES_ROOT + "editUserName.json";
    public final static String EDIT_BIO_RESPONSE_FILE_PATH = RESPONSES_ROOT + "editBio.json";
    public final static String FOLLOW_RESPONSE_FILE_PATH = RESPONSES_ROOT + "follow.json";
    public final static String UNFOLLOW_RESPONSE_FILE_PATH = RESPONSES_ROOT + "unfollow.json";
    public final static String GET_FOLLOWERS_LIST_RESPONSE_FILE_PATH = RESPONSES_ROOT + "getFollowersList.json";
    public final static String GET_FOLLOWING_LIST_RESPONSE_FILE_PATH = RESPONSES_ROOT + "getFollowingList.json";
    public final static String GET_MAIN_USER_PROFILE_RESPONSE_FILE_PATH = RESPONSES_ROOT + "getMainUserProfile.json";
    public final static String GET_ACTIVITIES_RESPONSE_FILE_PATH = RESPONSES_ROOT + "getActivities.json";
    public final static String GET_TIMELINE_RESPONSE_FILE_PATH = RESPONSES_ROOT + "getTimeline.json";
    public final static String GET_USER_PROFILE_RESPONSE_FILE_PATH = RESPONSES_ROOT + "getUserProfile.json";
    public final static String GET_MORE_POST_RESPONSE_FILE_PATH = RESPONSES_ROOT + "getMorePost.json";
    public final static String POST_RESPONSE_FILE_PATH = RESPONSES_ROOT + "post.json";
    public static final String DELETE_POST_RESPONSE_FILE_PATH = RESPONSES_ROOT + "deletePost.json";
    public final static String LIKE_RESPONSE_FILE_PATH = RESPONSES_ROOT + "like.json";
    public final static String UNLIKE_RESPONSE_FILE_PATH = RESPONSES_ROOT + "unlike.json";
    public final static String GET_LIKE_LIST_RESPONSE_FILE_PATH = RESPONSES_ROOT + "getLikeList.json";
    public final static String COMMENT_RESPONSE_FILE_PATH = RESPONSES_ROOT + "comment.json";
    public static final String DELETE_COMMENT_RESPONSE_FILE_PATH = RESPONSES_ROOT + "deleteComment.json";
    public final static String GET_COMMENT_LIST_RESPONSE_FILE_PATH = RESPONSES_ROOT + "getCommentList.json";
    public final static String GET_CHAT_LIST_RESPONSE_FILE_PATH = RESPONSES_ROOT + "getChatList.json";
    public final static String SEND_DIRECT_MESSAGE_RESPONSE_FILE_PATH = RESPONSES_ROOT + "sendDirectMessage.json";
    public final static String GET_DIRECT_MESSAGES_RESPONSE_FILE_PATH = RESPONSES_ROOT + "getDirectMessages.json";
    public final static String LOGOUT_RESPONSE_FILE_PATH = RESPONSES_ROOT + "logout.json";
    public final static String DELETE_ACCOUNT_RESPONSE_FILE_PATH = RESPONSES_ROOT + "deleteAccount.json";
    public final static String INVALID_REQUEST_RESPONSE_FILE_PATH = RESPONSES_ROOT + "invalidRequest.json";


    public final static String SIGNUP_REQUEST_FILE_PATH = REQUESTS_ROOT + "signUp.json";
    public final static String VERIFY_REQUEST_FILE_PATH = REQUESTS_ROOT + "verify.json";
    public final static String LOGIN_REQUEST_FILE_PATH = REQUESTS_ROOT + "login.json";
    public final static String FORGOT_PASS_REQUEST_FILE_PATH = REQUESTS_ROOT + "forgotPass.json";
    public final static String CHANGE_PASSWORD_REQUEST_FILE_PATH = REQUESTS_ROOT + "changePass.json";
    public final static String CHANGE_EMAIL_REQUEST_FILE_PATH = REQUESTS_ROOT + "changeEmail.json";
    public final static String EDIT_PROFILE_PIC_REQUEST_FILE_PATH = REQUESTS_ROOT + "editProfilePic.json";
    public final static String REMOVE_PROFILE_PIC_REQUEST_FILE_PATH = REQUESTS_ROOT + "removeProfilePic.json";
    public final static String EDIT_USERNAME_REQUEST_FILE_PATH = REQUESTS_ROOT + "editUserName.json";
    public final static String EDIT_BIO_REQUEST_FILE_PATH = REQUESTS_ROOT + "editBio.json";
    public final static String FOLLOW_REQUEST_FILE_PATH = REQUESTS_ROOT + "follow.json";
    public final static String UNFOLLOW_REQUEST_FILE_PATH = REQUESTS_ROOT + "unfollow.json";
    public final static String GET_FOLLOWERS_LIST_REQUEST_FILE_PATH = REQUESTS_ROOT + "getFollowersList.json";
    public final static String GET_FOLLOWING_LIST_REQUEST_FILE_PATH = REQUESTS_ROOT + "getFollowingList.json";
    public final static String GET_MAIN_USER_PROFILE_REQUEST_FILE_PATH = REQUESTS_ROOT + "getMainUserProfile.json";
    public final static String GET_ACTIVITIES_REQUEST_FILE_PATH = REQUESTS_ROOT + "getActivities.json";
    public final static String GET_TIMELINE_REQUEST_FILE_PATH = REQUESTS_ROOT + "getTimeline.json";
    public final static String GET_USER_PROFILE_REQUEST_FILE_PATH = REQUESTS_ROOT + "getUserProfile.json";
    public final static String GET_MORE_POST_REQUEST_FILE_PATH = REQUESTS_ROOT + "getMorePost.json";
    public final static String POST_REQUEST_FILE_PATH = REQUESTS_ROOT + "post.json";
    public static final String DELETE_POST_REQUEST_FILE_PATH = REQUESTS_ROOT + "deletePost.json";
    public final static String LIKE_REQUEST_FILE_PATH = REQUESTS_ROOT + "like.json";
    public final static String UNLIKE_REQUEST_FILE_PATH = REQUESTS_ROOT + "unlike.json";
    public final static String GET_LIKE_LIST_REQUEST_FILE_PATH = REQUESTS_ROOT + "getLikeList.json";
    public final static String COMMENT_REQUEST_FILE_PATH = REQUESTS_ROOT + "comment.json";
    public static final String DELETE_COMMENT_REQUEST_FILE_PATH = REQUESTS_ROOT + "deleteComment.json";
    public final static String GET_COMMENT_LIST_REQUEST_FILE_PATH = REQUESTS_ROOT + "getCommentList.json";
    public final static String GET_CHAT_LIST_REQUEST_FILE_PATH = REQUESTS_ROOT + "getChatList.json";
    public final static String SEND_DIRECT_MESSAGE_REQUEST_FILE_PATH = REQUESTS_ROOT + "sendDirectMessage.json";
    public final static String GET_DIRECT_MESSAGES_REQUEST_FILE_PATH = REQUESTS_ROOT + "getDirectMessages.json";
    public final static String LOGOUT_REQUEST_FILE_PATH = REQUESTS_ROOT + "logout.json";
    public final static String DELETE_ACCOUNT_REQUEST_FILE_PATH = REQUESTS_ROOT + "deleteAccount.json";


    public static final String LIKE_NOTIFICATION_FILE_PATH = NOTIFICATIONS_ROOT + "like.json";
    public static final String FOLLOW_NOTIFICATION_FILE_PATH = NOTIFICATIONS_ROOT + "follow.json";
    public static final String COMMENT_NOTIFICATION_FILE_PATH = NOTIFICATIONS_ROOT + "comment.json";
    public static final String MENTION_ON_COMMENT_NOTIFICATION_FILE_PATH = NOTIFICATIONS_ROOT + "mentionOnComment.json";
    public static final String DIRECT_MESSAGE_NOTIFICATION_FILE_PATH = NOTIFICATIONS_ROOT + "directMessage.json";

}
