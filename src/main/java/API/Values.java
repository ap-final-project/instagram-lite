package API;

public class Values {
    public static final String TITLE_CONFIG = "stage.title";
    public static final String RESIZE_CONFIG = "stage.resizable";
    public static final String HOST_CONFIG = "instagram.socket.host";
    public static final String PORT_CONFIG = "instagram.socket.port";
    public static final String ERROR_COLOR_CONFIG = "errorLbl.color";

}
