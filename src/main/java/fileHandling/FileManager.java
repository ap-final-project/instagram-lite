package fileHandling;

import org.apache.commons.io.FilenameUtils;
import socket.SocketClass;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;

/**
 * This class is used to send and receive files in both client and server side
 */
public class FileManager {
    private SocketClass socket;
    private String dir;

    public FileManager(SocketClass socket, String dir) {
        this.socket = socket;
        this.dir = dir;
    }


    public void receiveOne() {
        byte[] partBytes = new byte[0];
        try {
            partBytes = socket.readMessage();
            ByteBuffer byteBuffer = ByteBuffer.allocate(8);

            byteBuffer.put(partBytes);
            byteBuffer.flip();
            long parts = byteBuffer.getLong();

            byte[] formatBytes = socket.readMessage();
            String fileNme = new String(formatBytes);

            getFile(socket, parts, fileNme, dir);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void receiveMany(int num) {
        for (int i = 0; i < num; i++) {
            receiveOne();
        }
    }

    public void sendOne(String filename) {
        File file = new File(dir + filename);
        FileMessage fileMsg = new FileMessage(file);
        try {
            byte[] numOfPart = longToBytes(fileMsg.getNumOfPart());
            socket.sendMessage(numOfPart);

            byte[] fileName = file.getName().getBytes();
            socket.sendMessage(fileName);

            sendFile(this.socket, fileMsg);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendMany(ArrayList<String> fileList) {
        for (String s : fileList) {
            sendOne(s);
        }
    }

    private void sendFile(SocketClass socket, FileMessage fileMsg) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(fileMsg.getFile());
        for (long i = 0; i < fileMsg.getNumOfPart(); i++) {
            if ((i == fileMsg.getNumOfPart() - 1) && (fileMsg.getExtraBytes() != 0)) {
                socket.sendMessage(fileInputStream.readNBytes(fileMsg.getExtraBytes()));
            } else {
                socket.sendMessage(fileInputStream.readNBytes(FileMessage.PART_SIZE));
            }
        }

        fileInputStream.close();
    }

    private void getFile(SocketClass socket, long parts, String fileName, String dir) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(dir + fileName);

        for (long i = 0; i < parts; i++) {
            byte[] byt = socket.readMessage();
            fileOutputStream.write(byt);
        }

        fileOutputStream.close();
    }

    private byte[] longToBytes(long x) {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.putLong(x);
        return buffer.array();
    }

    public static String rename(String dir, String from, String to) {
        File f1 = new File(dir + from);
        File f2 = new File(dir + to + "." + FilenameUtils.getExtension(from));
        f1.renameTo(f2);
        return f2.getName();
    }

    public static boolean deleteFile(String dir, String fileName) {
        return new File(dir + fileName).delete();
    }
}
