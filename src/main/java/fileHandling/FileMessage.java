package fileHandling;

import java.io.File;

public class FileMessage {
    private File file;
    private long numOfPart;
    private int extraBytes;
    public final static int PART_SIZE = 1024 * 1024;

    public FileMessage(File file) {
        this.file = file;

        this.extraBytes = (int) (file.length() % PART_SIZE);

        this.numOfPart = count();
    }

    private long count() {
        long parts = this.file.length() / PART_SIZE;
        if (this.extraBytes > 0) {
            parts++;
        }
        return parts;
    }

    public File getFile() {
        return file;
    }

    public long getNumOfPart() {
        return numOfPart;
    }

    public int getExtraBytes() {
        return extraBytes;
    }
}
