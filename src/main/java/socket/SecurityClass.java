package socket;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;

/**
 * Encrypt and Decrypt data using AES algorithm with GCM mode
 */

public class SecurityClass {
    private final String ENCRYPT_ALGO = "AES/GCM/NoPadding";
    private final Charset UTF_8 = StandardCharsets.UTF_8;
    private GCMParameterSpec gcmParameterSpec;
    private final int TAG_LENGTH_BIT = 128;
    private final int IV_LENGTH_BYTE = 16;
    private final int AES_KEY_BIT = 256;
    private SecretKey secret;
    private Cipher cipher;
    private byte[] iv;

    public SecurityClass() {
        CryptoClass cryptoClass = new CryptoClass();
        try {
            this.secret = cryptoClass.getAESKey(AES_KEY_BIT);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        this.iv = cryptoClass.getRandom(IV_LENGTH_BYTE);
        this.gcmParameterSpec = new GCMParameterSpec(TAG_LENGTH_BIT, iv);

    }

    public byte[] encrypt(byte[] message) throws Exception {
        cipher = Cipher.getInstance(ENCRYPT_ALGO);
        cipher.init(Cipher.ENCRYPT_MODE, secret, gcmParameterSpec);
        return cipher.doFinal(message);
    }


    public String decrypt(byte[] message) throws Exception {
        cipher = Cipher.getInstance(ENCRYPT_ALGO);
        cipher.init(Cipher.DECRYPT_MODE, secret, gcmParameterSpec);
        byte[] plainText = cipher.doFinal(message);
        return new String(plainText, UTF_8);
    }

}
